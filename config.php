<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->disableupdatenotifications = false;
//$CFG->simplereturnloginas = true; // Para no cerrar sesion ante cambio de roles

//$CFG->dbtype    = 'mysqli';
//$CFG->dblibrary = 'native';
//$CFG->dbhost    = 'https://unibe.edu.do';
//$CFG->dbname    = 'moodle';
//$CFG->dbuser    = 'moodle';
//$CFG->dbpass    = 'lcA7AOy6';
//$CFG->prefix    = 'mdl_';
//$CFG->dboptions = array (
//    'dbpersist' => 0,
//    'dbport' => '',
//    'dbsocket' => '',
//);

// local config database

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodle';
$CFG->dbuser    = 'root';
$CFG->dbpass    = '';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
    'dbpersist' => 0,
    'dbport' => '',
    'dbsocket' => '',
);

//$CFG->wwwroot   = 'https://aulavirtual.unibe.edu.do';
//$CFG->dataroot  = 'd:\\datos\\moodledata';
//$CFG->admin     = 'admin';

// config local server
//$ipLocal = $_SERVER['REMOTE_ADDR'];
$CFG->wwwroot   = 'http://172.26.0.159/AV/avunibe';
$CFG->dataroot  = 'C:\\moodledata';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

$CFG->opensslcnf = 'C:\Program Files (x86)\PHP\v5.6.27\extras\ssl\openssl.cnf';

$CFG->smtpauthtype = "LOGIN";//LOGIN,NTLM
$CFG->smtprealm = "unibe.edu.do";//"unibe.inet";//Domain or email domain
//$CFG->smtpworkstation = "ui12m001";//Allowed workstation to send mails
$CFG->smtpsecure = "STARTTLS";
$CFG->smtphosts = "smtp.office365.com:587";
$CFG->smtpuser = 'virtual@unibe.edu.do';
$CFG->smtppass = 'virtualunibe';
$CFG->debugsmtp = 1;
$CFG->debugdeveloper = true;
$CFG->debugdisplay = 1;
//$CFG->debug = (E_ALL | E_STRICT);

// Almacenamiento en caché de JS
$CFG->cachejs = true;

require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!