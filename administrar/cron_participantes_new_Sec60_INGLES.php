﻿<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////// [INICIO] - Inicializar ///////////////////////////////////////////////////////////////////////////////////////////////////////////

	define('FULLME', 'cron');

	set_time_limit(0);

	require_once(dirname(__FILE__) . '/../config.php');
	require_once($CFG->libdir.'/adminlib.php');

	error_reporting(E_ALL);

	global $CFG, $DB, $mssql_dbhandle;


		//////////// Coneccion MSSQL a Acad_Media ////
		//////////////////////////////////////////////
	$serverName = "ui11m011\uni_sql";
	$connectionInfo = array( "Database"=>"Acad_Media", "UID"=>"AVirtual", "PWD"=>'$haDV1rt');
	$mssql_dbhandle = sqlsrv_connect( $serverName, $connectionInfo);

	/*//// [PRESENTACIÓN] ////*/ echo "<div style='margin:6px auto;font-family:arial;background-color:lightgray;padding:5px;'>";
		
	if ($mssql_dbhandle) {
		/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:lightgreen;padding:5px;margin:2px;color:black;'>Conexión establecida</div>"; ////*
	} else {
		/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:black;padding:5px;margin:2px;color:red;'>Conexión no se pudo establecer</div>"; ////*
		 echo ".<br />";
		 die(print_r( sqlsrv_errors(), true));
	}

	function disconnect_mssql() {
		global $mssql_dbhandle;
		sqlsrv_close($mssql_dbhandle);
	}

	register_shutdown_function('disconnect_mssql');


		//////////// Seteo ////
		///////////////////////
	$param_cursos = optional_param('cursos', null, PARAM_RAW);
	$in_cursos = ''; //// $in_cursos = 'AI1291';

	if ($param_cursos) {
		$arr_cursos = unserialize($param_cursos);
		$in_cursos = implode("','", $arr_cursos);
	}


		//////////// Determino en que mes del periodo actual estoy(1er mes, 2do, 3ro, 4to) ////
		///////////////////////////////////////////////////////////////////////////////////////
	$periodo = getdate();

	$dia = $periodo["mday"];
	$mes = $periodo["mon"];
	$ano = $periodo["year"];

	if($mes >= 9 and $mes <= 12) { $ano = $ano + 1; $per_pref = 1;}
	if($mes >= 1 and $mes <= 4) { $per_pref = 2;}
	if($mes >= 5 and $mes <= 8) { $per_pref = 3;}

	$per_num = $ano."-".$per_pref; //// $per_num="2011-1";

	/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:gray;padding:5px;margin:2px;color:white;'>Script de Actualizacion de Usuarios de Acad_Media -> AulaVirtual</div>"; ////*
	/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:gray;padding:5px;margin:2px;color:white;'>Periodo Academico: <strong>".$per_num."</strong></div>"; ////*

	if($mes == 9 OR $mes == 1 OR $mes == 5) { $secuencia_mes = 1; }
	if($mes == 10 OR $mes == 2 OR $mes == 6) { $secuencia_mes = 2; }
	if($mes == 11 OR $mes == 3 OR $mes == 7) { $secuencia_mes = 3; }
	if($mes == 12 OR $mes == 4 OR $mes == 8) { $secuencia_mes = 4; }
	if($secuencia_mes == 1 and $dia > 12) { $secuencia_mes = 2; } //// Si han pasado 2 semanas del primer mes, tratalo como si fuera el 2do mes

	if($secuencia_mes == 1) { $accion_ejecutar = "Agregar y Remover usuarios"; }
	if($secuencia_mes == 2) { $accion_ejecutar = "Agregar usuarios"; }
	if($secuencia_mes == 3) { $accion_ejecutar = "Remover usuarios retirados"; }
	if($secuencia_mes == 4) { $accion_ejecutar = "Remover usuarios retirados"; }

	/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:pink;padding:5px;margin:2px;color:white;'>Secuencia mes: <strong>".$secuencia_mes."</strong></div>"; ////*

		//////////// Recorro todos los cursos visibles ////
		///////////////////////////////////////////////////
	$cursos = array(
'CCC100',
'CCC110',
'CGB110',
'CGC100',
'CGC110',
'CGC120',
'CGC210',
'CGC400',
'CGE111',
'CGE112',
'CGF101',
'CGM140',
'CGQ100',
'CGQ200',
'CGQ201',
'CGS110',
'CGS140',
'CGT210',
'ELE100',
'ELE103',
'ELE104',
'ELE503',
'ESP140',
'M11100',
'M12242',
'M12250',
'M12260',
'M12310',
'M12320',
'M12330',
'M12340',
'M12360',
'M12361',
//'M12362',
//'M12363',
'M12370',
'M12371',
'M12380',
'M12400',
//'M12410',
//'M12420',
//'M12425',
'M14200',
'M14210',
'M14220',
'M14230',
'M14240',
'M14241',
'M14242',
'M14245',
'M14250',
'M14260',
'M14270',
'M14280',
'M14290',
'M14310',
'M14320',
'M14330',
'M14340',
'M14350',
'M14355',
'M14360',
'M14361',
'M14370',
'M14371',
'M14380',
'M14390',
'M14395',
'M14400',
'M14700');

	if (!$cursos) {
		die ('no hay cursos seleccionados');//Si no encuentra nigun curso que cumpla las condiciones, termina
	}

	/*//// [PRESENTACIÓN] ////*/ echo "</div>";
	
	foreach ($cursos as $shortname1) {
		/*//// [PRESENTACIÓN] ////*/ echo "<div style='margin:6px auto;font-family:arial;background-color:lightgray;padding:5px;'>";
		$conteo = "";
		$query_cont = "SELECT COUNT(*) AS [Conteo] FROM Acad_Media.dbo.Asignatura WHERE AsignaturaID = '".$shortname1."'";
		/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:gray;padding:5px;margin:2px;color:white;'>Asignatura a procesar: <strong>".$shortname1."</strong></div>"; ////*
		
		//////////// Si el codigo del curso en AV no existe en AM saltar ////
		$ms_result = sqlsrv_query($mssql_dbhandle, $query_cont);
		
		if ($ms_result) {
			while($row = sqlsrv_fetch_array($ms_result, SQLSRV_FETCH_ASSOC)) {
				$conteo = $row['Conteo'];
			}
		} else {
			CONTINUE;
		}
		
		if ($conteo == "0") {
			/*//// [PRESENTACIÓN] ////*/ echo "</div><br />"; ////*
			CONTINUE;
		} else {
			/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:gray;padding:5px;margin:2px;color:white;'>Existente en Acad_Media: ".$row['Conteo']."</div>";
		}

		//////////// Vease que si ya paso la 2da semana del 1er mes se trata como si fuera el segundo mes ////
		/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:blue;padding:5px;margin:2px;color:white;'>Procesando curso: <strong>".$shortname1."</strong></div>"; ////*
		
		switch($accion_ejecutar) {
			case "Agregar y Remover usuarios":
				/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:yellow;padding:5px;margin:2px;color:black;'> --A-- <strong>".$accion_ejecutar."</strong></div>"; ////*
				agregar_remover($shortname1);
			break;
			case "Agregar usuarios":
				/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:yellow;padding:5px;margin:2px;color:black;'> --B-- <strong>".$accion_ejecutar."</strong></div>"; ////*
				agregar($shortname1);
			break;
			case "Remover usuarios retirados":
				/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:yellow;padding:5px;margin:2px;color:black;'> --C-- <strong>".$accion_ejecutar."</strong></div>"; ////*
				retirar($shortname1);
			break;
			case "Remover usuarios retirados":
				/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:yellow;padding:5px;margin:2px;color:black;'> --D-- <strong>".$accion_ejecutar."</strong></div>"; ////*
				retirar($shortname1);
			break;
			default:
				die('No se detecto el mes');
			break;
		}
		
		/*//// [PRESENTACIÓN] ////*/ echo "</div><br />";
	}

//////////// [FIN] - Inicializar //////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//////////// Busca todas las personas en el curso indicado del periodo actual que esten en A.M. ////
	//////////// y no esten en A.V. y las agrega a A.V. en el curso indicado ////
function agregar($shortname) {
	global $CFG, $DB, $mssql_dbhandle;
	
	//// Primero inscribe el mismo curso por defecto
	inscribir_participantes($shortname);

	//// Segundo busca si hay cursos equivalentes al curso seleccionado
	//// Pero solo inscribelos si el equivalente NO esta creado como curso en el AV.(se crearia duplicidad)
	$query = "
		SELECT 
			ae.AsEqEquivalencia 
		FROM 
			Asignatura_Equivalencia ae 
			inner join Asignatura a1 
				on ae.AsignaturaID = a1.AsignaturaID 
			inner join Asignatura a 
				on ae.AsEqEquivalencia = a.AsignaturaID 
		WHERE 
			ae.AsignaturaID = '".$shortname."' 
		";
		
	$result = sqlsrv_query($mssql_dbhandle,$query);
	
	if(($result) and (sqlsrv_num_rows($result) < 1)) { return; } //Si no hay ningun curso equivalente, termino

	while($row = sqlsrv_fetch_array($result)) {
		$curso_equivalente = $row["AsEqEquivalencia"];
		//Si el curos quivalente esta en av no lo inscribo
		$curso_eq_av = $DB->get_record_sql("SELECT * FROM  mdl_course WHERE shortname = '".$curso_equivalente."'");
		
		if ($curso_eq_av) {
			echo "<b>NO SE INSCRIBIO el curso $curso_equivalente dentro del curso $shortname ya que esta creado en el AV.</b><br />"; CONTINUE;
		} else {
			echo "<b>INSCRIBIENDO curso equivalente $curso_equivalente en curso: $shortname </b><br />";
			inscribir_participantes($curso_equivalente, $shortname); //Inscribo el equivalante en el origignal
		}
	}
}

	//////////// Busca todas las personas ESTUDIANTES en el curso indicado del periodo actual que esten en A.M. ////
	//////////// y no esten en A.V. y las agrega a A.V. en el curso indicado ////
	//////////// NOTA: Solo quita a estudiantes, pues los profesores es algo irregular ////
function agregar_remover($shortname) {
	global $CFG, $DB, $mssql_dbhandle;
	
	//// Agrega todos los usuarios que falten primero
	agregar($shortname);
	
	$periodo = determinar_periodo_academico();
	
	//// Buscar todos los estudiantes en A.V. que NO esten en A.M.
	$course = $DB->get_record('course', array('shortname'=>$shortname));
	
	//// get_course_users($course->id, '', '', 'u.id, u.firstname, u.lastname');
	$context = context_course::instance($course->id);
	$users = get_users_by_capability($context, 'moodle/legacy:student', 'u.id, u.username, u.firstname, u.lastname');//Solo estudiantes
	
	//// Busco en AV el grupo al que pertenece y veo si en AM el pertence a una seccion con ese nombre. Si no lo elimino DEL GRUPO
	//// Si no esta en AM en la materia que indica el name del grupo tambien lo elimino DEL CURSO
	
	echo "<br /><b>Buscando estudiantes con cambios de seccion o asignatura: ".$course->id."</b>";
	
	if ($users) foreach ($users as $user) {
		//// Determino el grupo al que pertenece
		$groups_user = user_group($course->id, $user->id);
		
		//// Aunque cada usuario se supone que tiene un solo grupo
		if ($groups_user) foreach($groups_user as $group) {
			list($group_asignatura, $group_seccion) = @spliti("-SEC", $group->name);
			
			if ($group_asignatura) {
				$query = "
					SELECT 
						* 
					FROM 
						SeleccionAsignatura_Estudiante 
					WHERE 
						PeriodoAcademicoID = '".$periodo."' 
						AND AsignaturaID = '".$group_asignatura."' 
						AND MatriculaID = '".$user->username."' 
					";
					
				$result = sqlsrv_query($mssql_dbhandle,$query);
				
				if (($result) and (sqlsrv_num_rows($result) < 1)) {
					//// Elimino usuario con rol de estudiante del curso
					echo "<br />Quitando de curso a usuario:".$user->username; role_unassign(5, $user->id, 0, $context->id);
				} else {
					$query = "
						SELECT 
							* 
						FROM 
							SeleccionAsignatura_Estudiante 
						WHERE 
							PeriodoAcademicoID = '".$periodo."' 
							AND AsignaturaID = '".$group_asignatura."' 
							AND MatriculaID = '".$user->username."' 
							AND SeccionID = '".$group_seccion."' 
						";
						
					$result = sqlsrv_query($mssql_dbhandle,$query);
					
					if (($result) and (sqlsrv_num_rows($result) < 1)) {
						//// Elimino del grupo solamente
						if(delete_records('groups_members', 'userid', $user->id, "groupid", $group->id)){
							echo "<br />Quitando de grupo a usuario: ".$user->username. " ".$group->name;
						}
					}
				}
			}
		}
	}
}

	//////////// Busca todas las personas ESTUDIANTES en el curso indicado del periodo actual que tengan estatus RETIRADO 
	//////////// en A.M. y quitalos del A.V. en el curso indicado
	//////////// NOTA: Solo quita a estudiantes, pues los profesores es algo irregular
function retirar($shortname) {
	global $CFG, $DB, $mssql_dbhandle;
	
	//// Obtengo el context del curso
	$course = $DB->get_record('course', 'shortname', $shortname);
	$context = $context->get_context_instance(CONTEXT_COURSE, $course->id);
	
	//// Si no se encuentra el context retorna
	if (!$context) { return; }

	//// Busca todas las asignaturas inscritas en este curso
	$groups = get_groups($course->id);
	
	echo "<br /><b>Eliminando del curso estudiantes Retirados:</b>";
	$group_asignaturas_array = array();
	
	foreach ($groups as $group) {
		list($group_asignatura, $group_seccion) = spliti("-SEC", $group->name);

		if($group_asignatura and !in_array($group_asignatura, $group_asignaturas_array)) {
			
			//// Para ir grabando las asignaturas para no repetir
			$group_asignaturas_array[] = $group_asignatura;
			
			//// Armo el store procedure para la consulta de Estudiantes retirados en A.M.
			$stmt = sqlsrv_prepare($mssql_dbhandle, 'call dbo.spBusca_EstudiantesRetirados_Asignatura(?, ?);', array($determinar_periodo_academico(), $group_asignatura));

			if ($result = sqlsrv_execute($stmt)) {
				echo "<br />Resultados de asignaturas retiradas para: $shortname  $group_asignatura"; 	
				
				while ($row = sqlsrv_fetch_array($result)) {
					//// Quito al usuario del curso
					$user = $DB->get_record('user', 'username', $row['Matricula']);
					//// print_r($user); echo $row['Matricula'];
					if($user) { echo "<br />Retirando ".$user->id."context".$context->id; }
					if($user) { role_unassign(0, $user->id, 0, $context->id); }
				}
			}
			
			sqlsrv_free_stmt($stmt);
			
		}
	}
}


	//////////// LIBRERIA ////
	//////////////////////////
	//////////// Libreria de todas las funciones administrativas del panel de AV
	//////////// Determina el periodo academico de UNIBE segun una fecha 
	//////////// @param string $fecha La fecha en formato unixtimestamp (optional).
	//////////// @return string
	

	//////////// Determinar periodo Academico ////
function determinar_periodo_academico($fecha = '') {
	global $CFG, $DB, $mssql_dbhandle;
	$periodo = (empty($fecha)) ? getdate() : getdate($fecha);
	
	$dia = $periodo["mday"];
	$mes = $periodo["mon"];
	$ano = $periodo["year"];
	
	if($mes >= 9 and $mes <= 12) { $ano = $ano + 1; $per_pref = 1;}
	if($mes >= 1 and $mes <= 4) { $per_pref = 2;}
	if($mes >= 5 and $mes <= 8) { $per_pref = 3;}
	
	$per_num = $ano."-".$per_pref;
	
	return $per_num;
}

	//////////// Inscribe los participantes de A.M. en A.V. ////
	////////////////////////////////////////////////////////////
	//////////// @param string $shortname La AsignaturaID en A.M.
	//////////// @param string $shortname2 El shortname en A.V. donde se va a inscribir a los participantes si es diferente de $shortname (opcional).
	//////////// @param string $tipo_participantes El tipo de participantes a inscribir, docente, estudiante, todos (optional).
	//////////// @param string $periodo El periodo academico (optional).
	//////////// @param mixed $secciones Las secciones a inscribir (ingles, espanol, todas, custom) (optional).
	//////////// @return boolean
function inscribir_participantes($shortname, $shortname2='', $tipo_participantes='',  $periodo='', $secciones='ingles') {
	global $CFG, $DB, $mssql_dbhandle;
	
	/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:blue;padding:5px;margin:2px;color:white;'> ---- inscribir_participantes: <strong>".$shortname."</strong></div>"; ////*
	
	//// Determino las secciones
	if(! empty($secciones)) { $add_query =  " AND s.SeccionId IN (".$secciones.")"; } //Si manda un csv de cursos (abajo sobre escribo si no es asi)
	if($secciones == 'espanol') { $add_query = " AND s.SeccionId < '60'"; }
	if($secciones == 'ingles') { $add_query = " AND s.SeccionId > '59'"; }
	if($secciones == 'todas') { $add_query = ""; } //// todas
	
	//// Determino el periodo
	$periodo = (empty($periodo)) ? determinar_periodo_academico() : $periodo;

	$query = "
		SELECT DISTINCT 
			s.ProfesorID, 
			escu.EscuDescripcion as department, 
			s.ProfesorID as matricula, 
			RTRIM(p.PersPrimerNombre) + ' ' + RTRIM(p.PersSegundoNombre) as nombres, 
			RTRIM(p.PersPrimerApellido) + ' ' + RTRIM(p.PersSegundoApellido) as apellidos, 
			s.AsignaturaID as asignatura, 
			s.SeccionId as seccion, 
			p.PersEmail1 as email, 
			p.PersEmail2 as email2, 
			pd.PeDiTelefono1 as telefono, 
			pd.PeDiCelular as celular, 
			p.persRutaFoto, 
			p.persFoto, 
			p.PersonaId, 
			(SELECT top 1 DoIdValor FROM PersonaDocumentoIdentificacion WHERE personaid = p.PersonaId AND DoIdValor <> '' AND NOT DoIdValor is null ORDER BY DocumentoIdentificacionId,DoIdValor DESC ) as documento 
		FROM 
			Academicos.SeccionHorarioAsignatura_Det s 
			Inner Join Profesor pr 
				on s.ProfesorID = pr.ProfesorID 
			Inner Join Persona p 
				on pr.PersonaID = p.PersonaID 
			INNER JOIN Asignatura a 
				on s.AsignaturaID = a.AsignaturaID 
			INNER JOIN PersonaDireccion pd 
				on p.PersonaID = pd.PersonaID 
			Left Outer Join Escuela escu 
				on pr.EscuelaID = escu.EscuelaID 
		WHERE 
			s.PeriodoAcademicoID = '".$periodo."' 
			AND s.AsignaturaID IN ('".$shortname."') 
			AND s.SeccionId NOT LIKE '%-%' 
			AND SUBSTRING(p.PersEmail1, 1, CHARINDEX('@', p.PersEmail1)) NOT LIKE '%colegiada%' 
			".$add_query." 
		ORDER BY asignatura 
	";
	
	//// --INNER JOIN Persona pe On e.PersonaID = pe.PersonaID 
	//// --esto, para que NO traiga secciones de laboratorio(practica) 
	//// --Para que no inscriba el usuario ese de Honoris cundo no hay profesor 
	//// echo "<br />Query profesor<br /><pre>".$query."</pre>";

	if ($tipo_participantes == 'docente' or $tipo_participantes == '') { 
		inscribir_participantes_execute($query, $shortname, $shortname2, 'docente',  $periodo, $secciones); 
	}
	
	$query = "
		SELECT DISTINCT 
			x.matricula, 
			x.nombres, 
			x.apellidos, 
			x.asignatura, 
			x.seccion, 
			x.AsigDescripcion, 
			x.email,
			x.carrera, 
			x.department,
			MAX(x.telefono) AS telefono, 
			MAX(x.peditelefono2) AS peditelefono2, 
			MAX(x.celular) AS celular,
			x.PersRutaFoto, 
			x.PersFoto, 
			x.PersonaID, 
			x.documento 
		FROM
			(
				SELECT DISTINCT 
					e.MatriculaID AS matricula, 
					RTRIM(e.PersPrimerNombre) + ' ' + RTRIM(e.PersSegundoNombre) AS nombres,
					RTRIM(e.PersPrimerApellido) + ' ' + RTRIM(e.PersSegundoApellido) AS apellidos,
					s.AsignaturaID AS asignatura, 
					s.SeccionId AS seccion, 
					a.AsigDescripcion,
					(SELECT top 1 peememail FROM persona_email WHERE personaid = e.personaid AND ISNULL(peememail, '') <> '' ORDER BY personaEmailDuenoID ASC) AS email,
					e.CarreraID AS carrera, 
					e.CarrDescripcion AS department,
					pd.PeDiTelefono1 AS telefono, 
					pd.PeDiTelefono2, 
					pd.PeDiCelular AS celular,
					pe.persRutaFoto, 
					pe.persFoto, 
					pe.PersonaId, 
					(SELECT top 1 DoIdValor FROM PersonaDocumentoIdentificacion WHERE personaid = pe.PersonaId AND DoIdValor <> '' AND NOT DoIdValor is null ORDER BY DocumentoIdentificacionId,DoIdValor DESC ) AS documento
				FROM 
					vEstudiantes e
					INNER JOIN SeleccionAsignatura_Estudiante s 
						ON e.MatriculaID = s.MatriculaID
					INNER JOIN Asignatura a 
						ON s.AsignaturaID = a.AsignaturaID
					INNER JOIN Persona pe 
						ON e.PersonaID = pe.PersonaID
					LEFT OUTER JOIN PersonaDireccion pd 
						ON pe.PersonaID = pd.PersonaID
					LEFT OUTER JOIN Persona_Email pee 
						ON e.PersonaID = pee.PersonaID
				WHERE 
					e.EstadoEstudianteID IN (1, 12, 17)
					AND s.AsignaturaID IN ('".$shortname."')
					AND s.PeriodoAcademicoID = '".$periodo."'
					AND s.SeccionId NOT LIKE '%-%' 
					".$add_query."
					AND dbo.fntestudiantetienepagomatricula('".$periodo."', e.MatriculaID) = 1
			) AS x		
		GROUP BY 
			x.matricula, 
			x.nombres, 
			x.apellidos, 
			x.asignatura, 
			x.seccion, 
			x.AsigDescripcion, 
			x.email,
			x.carrera, 
			x.department, 
			x.PersRutaFoto, 
			x.PersFoto, 
			x.PersonaID, 
			x.documento 
		ORDER BY 
			x.asignatura, 
			x.seccion
	";
	
	//// --pee.peemEmail AS email, 
	//// --INNER JOIN PagoMatriculacion p 
	//// --ON s.PeriodoAcademicoID = p.PeriodoAcademicoID 
	//// --And s.MatriculaID = p.MatriculaID
	//// --INNER JOIN Financieros.DocumentoContable_Enc de 
	//// --ON p.TipoDocumentoContableID = de.TipoDocumentoContableID 
	//// --And p.DocumentoContableID = de.DocumentoContableID
	//// --AND p.DocumentoContableID IS NOT NULL
	//// --AND de.EstatusID = 1
	//// --AND TipoDireccionID = 'A' //// --A, es simplemente para que la direccion no sea repetida(no se repita el rocrd)
	//// --And pee.PersonaEmailDuenoID = 1
	//// --esto, para que NO traiga secciones de laboratorio(practica)
	
	//// echo "<br />Query estudiante<br /><pre>".$query."</pre>";
	if ($tipo_participantes == 'estudiante' or $tipo_participantes == '') {
		echo "<br />Inscribiendo Estudiante:<br />"; 
		inscribir_participantes_execute($query, $shortname, $shortname2, 'estudiante',  $periodo, $secciones);
	}
	
	echo "<hr />";
}

	/*//////////// Ejecuta el query para Inscribir los participantes de A.M. en A.V. ////
	///////////////////////////////////////////////////////////////////////////////////
	//////////// @param string $query El query a ejecutar
	//////////// @param string $shortname La AsignaturaID en A.M.
	//////////// @param string $shortname2 El shortname en A.V. donde se va a inscribir a los participantes si es diferente de $shortname (opcional).
	//////////// @param string $tipo_participantes El tipo de participantes a inscribir, docente o estudiante.
	//////////// @param string $periodo El periodo academico (optional).
	//////////// @param mixed $secciones Las secciones a inscribir (ingles, espanol, todas, custom) (optional).
	//////////// @return boolean */
function inscribir_participantes_execute($query, $shortname, $shortname2, $tipo_participantes,  $periodo, $secciones) {
	global $CFG, $DB, $mssql_dbhandle;

	/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:blue;padding:5px;margin:2px;color:white;'> ---- inscribir_participantes_execute [".$tipo_participantes."]: <strong>".$shortname."</strong></div>"; ////*
	/*//// [PRESENTACIÓN] ////*/ echo "<div style='background-color:blue;padding:5px;margin:2px;color:white;max-height:120px;overflow-y:auto;'> ---- query: <br /><pre>".$query."</pre></div>"; ////*
	
	$pwd = md5('buenosdias');
	$time = time();
	
	$shortname = strtoupper($shortname);
	$shortname = str_replace('M12','M14',$shortname);
	$shortname = $shortname."B";
	
	/* if (strpos($shortname,'M12')) {
	} */
	
	$asignatura =  (! empty($shortname2)) ? $shortname2 : $shortname;
	$admin_usr = $DB->get_record_sql("SELECT * FROM  mdl_user WHERE username = ?", array('admin'));
	$courseid1 = $DB->get_record_sql("SELECT * FROM  mdl_course WHERE shortname = '".$asignatura."'");	
	$courseid = $courseid1->id;
	$contextid1 = $DB->get_record_sql("SELECT * FROM mdl_context WHERE contextlevel = '50' AND instanceid = '".$courseid."'"); 
	$contextid = $contextid1->id;

	echo "<br />CourseID: ".$courseid."<br />";
	echo "<br />Tipo Part: ".$tipo_participantes."<br />";
	echo "<br /><pre>Tipo Part: ".$query."</pre><br />";
	
	//////////// sqlsrv_query($mssql_dbhandle, "SET latin1 COLLATE latin1_general_ci");
	//////////// sqlsrv_query($mssql_dbhandle, "COLLATE SQL_Latin1_General_CP1_CI_AS") or die("Invalid MSSQL query: " . mssql_get_last_message()); 
	//////////// sqlsrv_query($mssql_dbhandle, "SET collation_connection = 'SQL_Latin1_General_CP1_CI_AS'") or die("Invalid MSSQL query: " . mssql_get_last_message()); 
	//////////// echo $query;
		
	//////////// proceseo los resultados del query de Luis
	$result = sqlsrv_query($mssql_dbhandle, $query);

	//////////// if (sqlsrv_num_rows($result) < 1) {
	//////////// CONTINUE;
	//////////// }
	
	$numRows = sqlsrv_num_rows($result); //// printeo el el numero de filas
	echo "<br /><b>" . $numRows . " resultado" . ($numRows == 1 ? "" : "s") . " en el sistema academico.</b><br />"; 
	$asig_sec = 3; $asig_sec_tmp = 4; //// simplemente para inicializar con valores diferentes cualquiera
	
	while($row = sqlsrv_fetch_array($result)) {
		$nombres = addslashes(ucwords(strtolower($row["nombres"])));
		$apellidos = addslashes(ucwords(strtolower($row["apellidos"])));
		
		//ya no es necesario, se le hace un utf8 encode al select entero
		//$nombres = utf8_encode($nombres);//Esta data viene de MS SQL y no esta codficada en UTF8 por lo cual hay que convertirla
		//$apellidos =  utf8_encode($apellidos);
		//if ($row["ProfesorID"]){ // si existe el campo entonces es el select de profesores
		//$row["matricula"] = str_ireplace("@", "", $row["matricula"]);//matricula prefijo del email profesor(vea res select)
		//}
		
		$asig_sec = $row["asignatura"] . "-SEC". $row["seccion"];
		
		echo "<li>SEC-".$row["seccion"]." NOMBRE REAL:(".$asig_sec.") ";
		echo ($row["matricula"]) ? $row["matricula"] : "**no hay nombre usuario**";//Si no hay, se obvia con un continue; mas abajo
		echo "-". $nombres . " " . $apellidos . "</li>";
		
		$row['email'] = ($row["email"]) ? $row["email"] : '';

		///////////////////////BEGIN para crear los grupos de las secciones//////////////////////////////
		/*
		use `moodle`; INSERT INTO mdl_groups ( COURSEID, NAME, DESCRIPTION, LANG, TIMECREATED ) VALUES ( 8, 'gp_gab', '', 'es_utf8', 1171303970 )
		use `moodle`; INSERT INTO mdl_groups_members ( GROUPID, USERID, TIMEADDED ) VALUES ( 15, 50, 1171305336 )
		use `moodle`; INSERT INTO mdl_groups_members ( GROUPID, USERID, TIMEADDED ) VALUES ( 15, 41, 1171305336 )
		*/

		if($asig_sec <> $asig_sec_tmp) { //sino esta repetido del fetch anterior no hay que buscar nada porque el fetch anterior ya hizo todo lo que tenia que hacer con respecto a esa asig y sec
			require_once("../group/lib.php");
			
			$query2 = "SELECT * FROM mdl_course WHERE shortname = '".$asignatura."'";
			$result2 = $DB->get_record_sql($query2); $courseid = $result2->id;//necesito el id del curso para hacer el INSERT
			$query2 = "SELECT * FROM mdl_groups WHERE name = '".$asig_sec."' AND courseid = ".$courseid."";//PUEDEN HABER VARIOS GRUPOS CON EL MISMO NOMBRE SEGUN FABRICIO
			$result2 = $DB->get_record_sql($query2);
			
			if(! $result2) { //si el grupo no esta creado ya, entonces lo creo
				$query2 = "SELECT * FROM mdl_course WHERE shortname = '".$asignatura."'";
				$result2 = $DB->get_record_sql($query2);
				
				if(! $result2 ) { die("ERRRORRR ..... NO EXISTE EL CURSO ".$asignatura.". Por favor cree el curso."); }

				$data = new stdClass();
				$data->name = $asig_sec;
				$data->courseid = $courseid;
				$data->description = '';
				$group_id = groups_create_group($data);
				
				//$query2 = "INSERT INTO mdl_groups ( COURSEID, NAME, DESCRIPTION, TIMECREATED ) VALUES ( $courseid, '$asig_sec', '', $time )";
				//$group_id****confuncion grupo = execute_sql($query2) or die("Error al intentar crear un grupo");
			}

			$query2 = "SELECT * FROM mdl_groupings WHERE name = '".$asig_sec."' AND courseid = ".$courseid."";
			$result2 = $DB->get_record_sql($query2);
			
			if(!$result2) { //si el gruping no esta creado ya, entonces lo creo
				$data = new stdClass();
				$data->name = $asig_sec;
				$data->courseid = $courseid;
				$grouping_id = groups_create_grouping($data);
				
				echo "<div style='background-color:black;color:white;'>grouping </div>";
				groups_assign_grouping($grouping_id, (int)$group_id); //// valida existencia antes de insertar
			}
		}
		
		$asig_sec_tmp = $asig_sec;
		////////////////////////END para crear los grupos de las secciones///////////////////////////////////

		//averiguo si el username ya esta en bd local(que lo puese en un array)
		if(!$DB->count_records_sql("SELECT COUNT(*) FROM mdl_user WHERE username = '".$row['matricula']."'")) {
			$query = "INSERT INTO mdl_user ( auth, confirmed, policyagreed, deleted, suspended, mnethostid, username, password, idnumber, firstname, lastname, email, emailstop, icq, skype, yahoo, aim, msn, phone1, phone2, institution, department, address, city, country, lang, calendartype, theme, timezone, firstaccess, lastaccess, lastlogin, currentlogin, lastip, secret, picture, url, description, descriptionformat, mailformat, maildigest, maildisplay, autosubscribe, trackforums, timecreated, timemodified, trustbitmask, imagealt, lastnamephonetic, firstnamephonetic, middlename, alternatename) VALUES ( 'manual', 1, 0, 0, 0, 1, '".$row['matricula']."', '".$pwd."', '".$row['documento']."', '".$nombres."', '".$apellidos."', '".$row['email']."', 0, '', '', '', '', '', '".$row['telefono']."', '".$row['celular']."', '', '".$row['department']."', '', '".$admin_usr->city."', '".$admin_usr->country."', '".$admin_usr->lang."', 'gregorian', '', '".$admin_usr->timezone."', ".$time.", ".$time.", ".$time.", ".$time.", '', '', 0, '', '', 1, 1, 0, 2, 1, 0, ".$time.", ".$time.", 0, '', '', '', '', '')";
			/*//// [mdl_role_assignments] ////*/ echo "<div style='border:1px solid red;'>[mdl_user] username: ".$row['matricula']."</div>";
			$DB->execute(utf8_encode($query)); //// or die("ERROR no se pudo insertar el nuevo usuario en AV: ".$DB->errorMsg().$query);
		}

		$userid1 = $DB->get_record_sql("SELECT * FROM mdl_user WHERE username = '".$row['matricula']."'"); 
		$userid = $userid1->id;
		
		$roleid = ($tipo_participantes == 'docente') ? 3 : 5; //3 profesor, 5 = estudiante
		
		$groupid1 = $DB->get_record_sql("SELECT * FROM mdl_groups WHERE name = '".$asig_sec."' AND courseid = ".$courseid."");
		$groupid = $groupid1->id;
		
		if(!$DB->count_records_sql("SELECT COUNT(*) FROM mdl_role_assignments WHERE roleid = ".$roleid." AND contextid = ".$contextid." AND userid = ".$userid."")) {
			$query = "INSERT INTO mdl_role_assignments (roleid, contextid, userid, timemodified, modifierid, component, itemid, sortorder) VALUES (".$roleid.", ".$contextid.", ".$userid.", ".$time.", 0, '', 0, 0)";
			/*//// [mdl_role_assignments] ////*/ echo "<div style='border:1px solid red;'>[mdl_role_assignments] ContextID: ".$contextid.", UserID: ".$userid."</div>";
			//// [ORIGINAL 1.9] //// $query = "INSERT INTO mdl_role_assignments ( ROLEID, CONTEXTID, USERID, HIDDEN, TIMESTART, TIMEEND, TIMEMODIFIED, MODIFIERID, ENROL ) VALUES ( ".$roleid.", ".$contextid.", ".$userid.", 0, ".$time.", 0, ".$time.", 0, 'manual' )";
			$DB->execute($query); //// or die("no se pudo asignar al usuario a su rol".$DB->errorMsg().$query);
		}

		if(!$DB->count_records_sql("SELECT COUNT(*) FROM mdl_groups_members WHERE GROUPID = ".$groupid." AND USERID = ".$userid."")){
			$query = "INSERT INTO mdl_groups_members ( groupid, userid, timeadded, component, itemid ) VALUES ( ".$groupid.", ".$userid.", ".$time.", '', 0 )";
			/*//// [mdl_role_assignments] ////*/ echo "<div style='border:1px solid red;'>[mdl_groups_members] groupid: ".$groupid.", userid: ".$userid."</div>";
			//$query = "INSERT INTO mdl_groups_members ( GROUPID, USERID, TIMEADDED ) VALUES ( ".$groupid.", ".$userid.", ".$time." )";
			$DB->execute($query); //// or die("No se pudo asignar al usuario a su grupo");
		}
		
		//// [MOODLE 3] //// Buscar enrolid ////
		$enrolid1 = $DB->get_record_sql("SELECT * FROM mdl_enrol WHERE courseid = ".$courseid." AND enrol = 'manual'"); 
		$enrolid = $enrolid1->id;
		
		if ($enrolid) {
			if(!$DB->count_records_sql("SELECT COUNT(*) FROM mdl_user_enrolments WHERE enrolid = ".$enrolid." AND userid = ".$userid."")) {
				//// [MOODLE 3] //// AGREGAR EN mdl_user_enrolments ////
				$query = "INSERT INTO mdl_user_enrolments (
					status,
					enrolid,
					userid,
					timestart,
					timeend,
					modifierid,
					timecreated,
					timemodified
				) VALUES (
					0,
					'".$enrolid."',
					".$userid.",
					UNIX_TIMESTAMP(),
					'0',
					2,
					UNIX_TIMESTAMP(),
					UNIX_TIMESTAMP())";
				//echo "<pre>".$query."</pre>";
				$DB->execute($query);
				echo "<div style='background-color:black;color:white;padding:3px;'>[mdl_user_enrolments] enrolid: ".$enrolid.", userid: ".$userid."</div>";
			}
		} else {
			echo "<div style='background-color:black;color:red;padding:3px;border:5px dotted red;'>[mdl_user_enrolments] NO ENROL ID, CourseId: ".$courseid."</div>";
		}
	}
}

	//////////// Obtiene un listado de cursos segun usuario ////
function listado_cursos_usuario($username) {
	global $CFG, $DB, $mssql_dbhandle;
	
	$user = get_record('user', 'username', $username);
	
	//////////// Obtengo la lista de cursos en AV ////
	if ($mycourses = get_my_courses($user->id)) {
		return $mycourses;
	}
	
	return false;
}
	
	//////////// Obtiene un listado de cursos segun usuario en Acadmedia ////
function listado_cursos_usuario_acad($username) {
	global $CFG, $DB, $mssql_dbhandle;
	
	$query = "
		SELECT DISTINCT
			s.AsignaturaID, 
			s.SeccionId, 
			a.AsigDescripcion
		FROM 
			SeleccionAsignatura_Estudiante s
			INNER JOIN Asignatura a 
				on s.AsignaturaID = a.AsignaturaID
			INNER JOIN Academicos.SeccionHorarioAsignatura_Det sec
				On s.PeriodoAcademicoID = sec.PeriodoAcademicoID
				and s.AsignaturaID = sec.AsignaturaID
				and s.SeccionID = sec.SeccionID
			INNER JOIN Aula al 
				on sec.AulaID = al.AulaID
		WHERE 
			s.PeriodoAcademicoID = '".$periodo."' 
			AND s.MatriculaId = '".$username."' 
	";
	
	//// --AND s.SeccionId NOT LIKE '%-%'
	
	$result = sqlsrv_query($mssql_dbhandle, $query);
	if(! sqlsrv_num_rows($result)>0) { return false; }
	$arr_cursos = array();
	
	while($row = sqlsrv_fetch_object($result)) {
		$arr_cursos[] = $row;	
		//// $courselisting2 .= "<tr><td>{$row['AsignaturaID']}</td><td>{$row['SeccionId']}</td><td>{$row['AsigDescripcion']}</td></tr>";
	}
	
	return $arr_cursos;
}

	//////////// Copia las imagenes de uno o varios usuarios del sistema academico hacia el AV ////
function ProcesarImg($usuario, $asignatura = null) {
	global $CFG, $DB, $mssql_dbhandle;
	
	require_once($CFG->libdir.'/gdlib.php');
	
	$query = "
		SELECT 
			* 
		FROM 
			mdl_user 
		WHERE 
			username = '".$usuario."'";
	
	if($usuario == "Actualizar") {
		$query = "
			SELECT 
				* 
			FROM 
				mdl_user 
			WHERE 
				picture = 0 
				AND username LIKE '%-%'"; //// solo los usuarios que no tienen imagen en moodle el LIKE '-' es para MATRICULAS(no incluya a los profesores)(luego tengo que verificar antes de subir las imagenes que el usuario en Honoris tenga su foto para no hacer un POST de valde)
	}
	
	if($usuario == "Actualizar Asignatura") {
		$query = "
			SELECT 
				t1.id AS id, 
				t1.username AS username, 
				t1.picture AS picture 
			FROM 
				mdl_user t1 
				INNER JOIN mdl_role_assignments t2 
					ON t1.id = t2.userid 
				INNER JOIN mdl_context t3 
					ON t2.contextid = t3.id 
				INNER JOIN mdl_course t4 
					ON t3.instanceid = t4.id 
			WHERE 
				t4.shortname = '".$asignatura."' 
				AND username LIKE '%-%' 
				AND picture = 0 
		";
	}
	
	echo $query;
	$result = $DB->get_records_sql($query);
	$numRows = count($result); //printeo el el numero de filas
	echo "<h3>" . $numRows . " Usuarios sin foto en UNIBE Virtual que se van a actualizar con la foto de HONORIS si tenen fotos en HONORIS. </h3>";

	$incActualizados = 0; $incOmitidosNOE =0; $incOmitidosNOR = 0;
	foreach($result as $row) {
		$matricula = $row->username; 
		$id = $row->id;

		//hago el query en Honoris con la matricula actual
		$query2 = "
			SELECT DISTINCT 
				e.MatriculaID as matricula, 
				RTRIM(e.PersPrimerNombre) + ' ' + RTRIM(e.PersSegundoNombre) as nombres,
				RTRIM(e.PersPrimerApellido) + ' ' + RTRIM(e.PersSegundoApellido) As apellidos,
				e.PersEmail1 as email, 
				e.CarreraID as carrera, 
				e.CarrDescripcion,
				pd.PeDiTelefono1, 
				pd.PeDiTelefono2, 
				pd.PeDiCelular, 
				pe.persRutaFoto, 
				pe.persFoto
			FROM 
				vEstudiantes e
				INNER JOIN Persona pe 
					On e.PersonaID = pe.PersonaID
				INNER JOIN PersonaDireccion pd 
					on pe.PersonaID = pd.PersonaID
			WHERE 
				pd.TipoDireccionID = 'A' 
				And e.MatriculaID = '".$matricula."'
				AND e.EstadoEstudianteID = 1
		";
		
		$result2 = sqlsrv_query($mssql_dbhandle, $query2);
		$numRows2 = sqlsrv_num_rows($result2); //printeo el el numero de filas


		while($row2 = sqlsrv_fetch_array($result2)) {
			$entro++;
			$archivo = $row2["persRutaFoto"] . $row2["persFoto"];
			
			if(! empty($archivo)) { //// si no esta vacio significa que si tiene foto por lo tanto hago el POST
				echo "<li>Subiendo el archivo: " . $archivo." .....";
				if (!file_exists($archivo)) {
					echo " El archivo no existe</li>"; $incOmitidosNOE++;
				} else {
					$destination = create_profile_image_destination($id,'user');
					//// Postear($archivo, $id, $matricula, 1);// id , matricula $soy_admin
					if(process_profile_image($archivo, $destination)) {
						set_field('user', 'picture', 1, 'id', $id);
						echo $archivo." archivo subido.</li>"; $incActualizados++;
					} else {
						echo $archivo." archivo NO subido.</li>";
					}
					
				}
			} else {
				$incOmitidosNOR++; 
			}
		}
		
		$archivo = '';
		
		//$i++; if ($i == 500) break; //nota hay que buscar una solucion para este break porque en el svr hay usuarios q n tienen fotos y por lo tanto eso cuenta un counter y va ocupando los que si tienen
	}
	
	echo "<br />".$entro." Total de fetch ".$numRows." , Se actualizaron ".$incActualizados." en UNIBE Virtual, se omitieron ".$incOmitidosNOR." no registrados con foto en HONORIS y ".$incOmitidosNOE." omitidos por tener imagenes inexistentes a la fecha. <br />";
	
}

?>