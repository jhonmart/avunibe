﻿<?php

@ini_set("output_buffering", "Off");
@ini_set('implicit_flush', 1);
@ini_set('zlib.output_compression', 0);
@ini_set('max_execution_time',1200);

ignore_user_abort(true);

error_reporting(E_ALL);

//set_time_limit(0);

require('../config.php');
require_once('../course/reset_form.php');

reset_bulk(0, 500);

function reset_bulk($start, $final){

    global $DB;

    $sql = 'SELECT id FROM {course} WHERE category NOT IN (5, 65, 67, 73, 74, 156, 30, 31, 32, 56, 57, 58, 59, 60, 61, 63, 71, 89, 91, 96, 103) AND (id >= '. $start .' AND id <= '. $final .') ;';

//        $sql = 'SELECT id FROM {course} WHERE category NOT IN (5, 65, 67, 73, 74, 156, 30, 31, 32, 56, 57, 58, 59, 60, 61, 63, 71, 89, 91, 96, 103) AND (id >= '.$start.' AND id <= '.$final.');';

    $courseids = $DB->get_records_sql($sql);

    echo "<p>Corriendo ejecución:</p>";

    foreach ($courseids as &$value) {

        ob_start();

        $startTime = microtime(true);

        $data = new stdClass;
        //$data->reset_start_date = 1504645132;
        $data->reset_assignment_submissions = 1;
        $data->reset_assign_submissions = 1;
        $data->reset_attendance_log = 1;
        $data->reset_attendance_statuses = 1;
        $data->reset_attendance_sessions = 1;
        $data->reset_chat = 1;
        $data->reset_choice = 1;
        $data->reset_data = 0;
        $data->reset_data_ratings = 0;
        $data->reset_data_comments = 0;
        $data->reset_data_notenrolled = 0;
        $data->reset_forum_all = 1;
        $data->reset_glossary_all = 0;
        $data->reset_glossary_ratings = 0;
        $data->reset_glossary_comments = 0;
        $data->reset_glossary_notenrolled = 0;
        $data->reset_jabberchat = 1;
        $data->reset_lesson = 0;
        $data->reset_peerassessment_all = 1;
        $data->reset_quiz_attempts = 0;
        $data->reset_scorm = 0;
        $data->reset_survey_answers = 1;
        $data->reset_survey_analysis = 1;
        $data->reset_turnitintool = 2; // This means "do nothing"
        $data->reset_workshop_submissions = 1;
        $data->reset_workshop_assessments = 1;
        $data->reset_workshop_phase = 0;

        // Course specific
        $data->reset_completion = 1;
        $data->reset_events = 1;
        $data->reset_logs = 1;
        $data->reset_roles_local = 1;
        $data->reset_gradebook_grades = 1;
        $data->reset_notes = 1;
        $data->reset_roles_overrides = 1;
        $data->reset_groups_members = 1;
        $data->reset_groups_remove = 1;
        $data->reset_groupings_members = 1;
        $data->reset_groupings_remove = 1;
        $data->reset_gradebook_items = 0;
        $data->reset_comments = 1;
        //$data->reset_roles = Array(5);
        $data->unenrol_users = Array(5);
        $data->id = $value->id;

        require_login($value->id);
        require_capability('moodle/course:update', context_course::instance($value->id));
        //		require_capability('moodle/course:update', get_context_instance(CONTEXT_COURSE, $value->id));

        reset_course_userdata($data);
        $time_elapsed_secs = microtime(true) - $startTime;

        print_r('Reiniciado: ' . $data->id . ' ('. round($time_elapsed_secs, 2) . 's)<br>');

        flush();
        ob_flush();
        ob_end_flush();
        ob_end_clean();

    }

    exit('<p>Log terminado.</p>');

}


//global $DB;
//
//$total_courses = $DB->count_records('course');
//$increase = 10;
//
//$log = array(
//    'start' => [0, 11],
//    'final' => [10]
//);
//
//for($i = 0; $i < $total_courses; $i++){
//    $new_value_start = end($log['start']) + $increase;
//    $new_value_final = end($log['final']) + $increase;
//
//    array_push($log['start'], $new_value_start);
//    array_push($log['final'], $new_value_final);
//}
//
//header( 'Content-type: text/html; charset=utf-8' );
//
//// revisar tiempo de este bucle
//for($i = 0; $i < count($log['final']); $i++){
//
//// CAMBIAR CONDICION AL TERMINAR LOG
//    if($log['final'][$i] < 1000){
//        ob_start();
//        reset_bulk($log['start'][$i], $log['final'][$i]);
//        flush();
//        ob_flush();
//        ob_end_flush();
//        ob_end_clean();
//    }else{
//        exit('<p>Log terminado.</p>');
//    }
//
////    unset($log['start'][$i]);
////    unset($log['final'][$i]);
//    //    echo ob_get_contents();
//
//
//}

?>