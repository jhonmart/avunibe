define(['jquery', 'core/chosen.jquery'], function ($, chosen) {

    return {
        init: function () {
            chosen.init();
            $('#id_overall_aggregation').chosen();
        }
    }

});