define(['jquery'], function($) {
    // DOCUMENTACION
    // https://docs.moodle.org/dev/Javascript_Modules

    //CALLS:
    // course/view.php

    return {
        init: function() {
            // SU UTILIDAD ES OCULTAR LOS ITEMS DE CATEGORIAS EN EL LISTADO DE CURSOS EN EL ASIDE
            $('.canexpand').attr('aria-expanded', false);
            $('.canexpand').attr('aria-selected', false);
            $('.canexpand').next('ul').attr('aria-hidden', true);

            var activity_status = $('.activityinstance a');

            if(activity_status.hasClass('dimmed')){
                $('.dimmed .instancename').css('color', '#969696');
                $('.dimmed .instancename').each(function (i, v) {
                    var actual_value = $(v).html();
                    $(v).html(actual_value + ' ' + '<span class="msg_field_hide">(Oculto)</span>');
                });

            }

        }
    };
});