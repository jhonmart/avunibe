-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mdl_lesson_timer`
--

DROP TABLE IF EXISTS `mdl_lesson_timer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_lesson_timer` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `lessonid` bigint(10) NOT NULL DEFAULT '0',
  `userid` bigint(10) NOT NULL DEFAULT '0',
  `starttime` bigint(10) NOT NULL DEFAULT '0',
  `lessontime` bigint(10) NOT NULL DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mdl_lesstime_use_ix` (`userid`),
  KEY `mdl_lesstime_les_ix` (`lessonid`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8 COMMENT='lesson timer for each lesson';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_lesson_timer`
--

LOCK TABLES `mdl_lesson_timer` WRITE;
/*!40000 ALTER TABLE `mdl_lesson_timer` DISABLE KEYS */;
INSERT INTO `mdl_lesson_timer` VALUES (108,21,355,1493730824,1493730833,1),(109,21,355,1493730847,1493730869,0),(187,1,4821,1506559286,1506559287,0),(188,1,5421,1506710977,1506712206,1),(189,1,6522,1507080240,1507083541,0),(190,1,6526,1506560042,1506560042,0),(191,1,2117,1506888087,1506891081,1),(192,1,6519,1507051822,1507055266,1),(193,1,4816,1507060459,1507064167,1),(194,1,6517,1506603000,1506605913,1),(195,1,6524,1506904823,1506906256,1),(196,1,6525,1506972389,1506976173,1),(197,1,2823,1507075638,1507077995,1),(198,1,4300,1506560059,1506560289,0),(199,1,6528,1506830817,1506836521,1),(200,1,6531,1506560094,1506560137,0),(201,1,6660,1507082112,1507086403,1),(202,1,6076,1506864802,1506864957,0),(203,1,6269,1506972601,1506975981,1),(204,1,6486,1506560232,1506560232,0),(205,1,6335,1506985047,1506987799,1),(206,1,2790,1506612063,1506614283,1),(207,1,6526,1506616179,1506633072,1),(208,1,6517,1506614621,1506614694,0),(209,1,4468,1506627016,1506628369,0),(210,1,6526,1506633158,1506633158,0),(211,1,4821,1506646410,1506646410,0),(212,1,4468,1506952071,1506957762,1),(213,1,6528,1506836650,1506836650,0),(214,1,6659,1506879774,1506880261,1),(215,1,6659,1506880289,1506880289,0),(216,1,6532,1506886076,1506887459,1),(217,1,6532,1506889682,1506889682,0),(218,1,2117,1506893245,1506893245,0),(219,1,6528,1506897919,1506897919,0),(220,1,6523,1506904274,1506907685,1),(221,1,6523,1506907808,1506907818,0),(222,1,6486,1506954185,1506954185,0),(223,1,4468,1506957864,1506957865,0),(224,1,6526,1506959158,1506959158,0),(225,1,6486,1506965506,1506966900,1),(226,1,6529,1506979332,1506981117,1),(227,1,6531,1507076529,1507081446,1),(228,1,5442,1507040981,1507042777,1),(229,1,4816,1507064224,1507064321,0),(230,1,6521,1507077638,1507080334,1),(231,1,1105,1507085871,1507087469,1),(232,1,5438,1507086786,1507087820,1),(233,1,4821,1507087931,1507088989,1);
/*!40000 ALTER TABLE `mdl_lesson_timer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-18  9:03:38
