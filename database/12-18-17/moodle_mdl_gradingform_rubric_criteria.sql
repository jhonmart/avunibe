-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mdl_gradingform_rubric_criteria`
--

DROP TABLE IF EXISTS `mdl_gradingform_rubric_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_gradingform_rubric_criteria` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `definitionid` bigint(10) NOT NULL,
  `sortorder` bigint(10) NOT NULL,
  `description` longtext,
  `descriptionformat` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mdl_gradrubrcrit_def_ix` (`definitionid`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COMMENT='Stores the rows of the rubric grid.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_gradingform_rubric_criteria`
--

LOCK TABLES `mdl_gradingform_rubric_criteria` WRITE;
/*!40000 ALTER TABLE `mdl_gradingform_rubric_criteria` DISABLE KEYS */;
INSERT INTO `mdl_gradingform_rubric_criteria` VALUES (1,1,1,'Análisis de la información: Los argumentos reflejan dominio de los temas en profundidad, con detalles y ejemplos de aplicación en entornos laborales.',0),(2,1,2,'Redacción: el alumno demuestra un alto nivel de expresión escrita que facilita la comprensión.\r\n',0),(3,1,3,'Ortografía: No presenta errores gramaticales.',0),(4,1,4,'Información completa: Están colgadas todas las actividades identificadas en el sílabo.',0),(5,1,5,'Diseño y creatividad: Se incluyen elementos de diseño de importancia que mejoran el aspecto del portafolio, demostrando tener ideas creativas e ingeniosas.',0),(6,1,6,'Organización: Las informaciones colgadas siguen una secuencia y orden lógico que facilitará evidenciar el desarrollo del estudiante.',0),(7,2,1,'Tipo de evaluación:\r\nHETEROEVALUACIÓN\r\nEs la evaluación\r\nrealizada por EL\r\nPROFESOR O\r\nFACILITADOR , luego\r\nde realizase una\r\ndeterminada tarea.',0),(8,2,2,'Dominio del tema. El\r\nmaterial asignado fue\r\ninvestigado con\r\ndiferentes fuentes\r\nbibliográficas,\r\ncomprendido y\r\naplicado.',0),(9,2,3,'Organización. La\r\ninformación ofrecida\r\nfue sintetizada y\r\norganizada.',0),(10,2,4,'Volumen de voz.\r\nExpresión oral con\r\nvolumen para ser\r\nescuchado de manera\r\nclara y modulada.',0),(11,2,5,'Uso del tiempo. La\r\ninformación es\r\nofrecida en el tiempo\r\npautado.',0),(12,2,6,'Postura del cuerpo y\r\ncontacto visual. El\r\nexpositor mantiene\r\nbuena postura,\r\nentusiasmo, interés y\r\ncontacto visual con los\r\nespectadores o\r\naudiencia.',0),(13,2,7,'Presentación visual\r\ndel contenido o tema\r\na tratar. Uso de\r\ngráficas, cuadros e\r\nimágenes con el\r\ncontenido de manera\r\ndinámica, entretenida\r\ny creativa.',0),(14,3,1,'Entrega del Post\r\n(2 puntos)\r\n',0),(15,3,2,'Estructura del Post\r\n(2 puntos)\r\n',0),(16,3,3,'Redacción del Post\r\n\r\n(3 puntos)\r\n',0),(17,3,4,'Ortografía\r\n\r\n(1 punto)\r\n',0),(18,4,1,'Puntualidad\r\n(2 puntos)\r\n',0),(19,4,2,'Interpretación de los datos\r\n(3 puntos)\r\n',0),(20,4,3,'Integración y/o Promoción del blog en Redes Sociales\r\n\r\n(3 puntos)\r\n',0),(21,4,4,'Ortografía\r\n\r\n(2 puntos)\r\n',0),(22,5,1,'Entrega del Post\r\n(2 puntos)\r\n',0),(23,5,2,'Estructura del Post\r\n(2 puntos)\r\n',0),(24,5,3,'Redacción del Post\r\n\r\n(3 puntos)\r\n',0),(25,5,4,'Ortografía\r\n\r\n(1 punto)\r\n',0),(26,6,1,'Entrega del Post\n(2 puntos)\n',0),(27,6,2,'Estructura del Post\n(2 puntos)\n',0),(28,6,3,'Redacción del Post\n\n(3 puntos)\n',0),(29,6,4,'Ortografía\n\n(1 punto)\n',0),(30,7,1,'Entrega del Post\n(2 puntos)\n',0),(31,7,2,'Estructura del Post\n(2 puntos)\n',0),(32,7,3,'Redacción del Post\n\n(3 puntos)\n',0),(33,7,4,'Ortografía\n\n(1 punto)\n',0),(34,8,1,'Puntualidad\n(2 puntos)\n',0),(35,8,2,'Interpretación de los datos\n(3 puntos)\n',0),(36,8,3,'Integración y/o Promoción del blog en Redes Sociales\n\n(3 puntos)\n',0),(37,8,4,'Ortografía\n\n(2 puntos)\n',0),(38,9,1,'Entrega del Post\n(2 puntos)\n',0),(39,9,2,'Estructura del Post\n(2 puntos)\n',0),(40,9,3,'Redacción del Post\n\n(3 puntos)\n',0),(41,9,4,'Ortografía\n\n(1 punto)\n',0),(42,10,1,'Entrega del Post\n(2 puntos)\n',0),(43,10,2,'Estructura del Post\n(2 puntos)\n',0),(44,10,3,'Redacción del Post\n\n(3 puntos)\n',0),(45,10,4,'Ortografía\n\n(1 punto)\n',0),(46,11,1,'Puntualidad\n(2 puntos)\n',0),(47,11,2,'Interpretación de los datos\n(3 puntos)\n',0),(48,11,3,'Integración y/o Promoción del blog en Redes Sociales\n\n(3 puntos)\n',0),(49,11,4,'Ortografía\n\n(2 puntos)\n',0);
/*!40000 ALTER TABLE `mdl_gradingform_rubric_criteria` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-18  9:04:44
