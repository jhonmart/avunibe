-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mdl_glossary_alias`
--

DROP TABLE IF EXISTS `mdl_glossary_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_glossary_alias` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `entryid` bigint(10) NOT NULL DEFAULT '0',
  `alias` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `mdl_glosalia_ent_ix` (`entryid`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8 COMMENT='entries alias';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_glossary_alias`
--

LOCK TABLES `mdl_glossary_alias` WRITE;
/*!40000 ALTER TABLE `mdl_glossary_alias` DISABLE KEYS */;
INSERT INTO `mdl_glossary_alias` VALUES (1,5,'trabajo en equipo'),(2,6,'responsabilidad de manera individual y grupal'),(3,7,'intercambio de información'),(5,9,'Criterios, Evaluación'),(6,10,'enunciados'),(7,11,'realidad, claridad'),(8,12,'instrumento de evaluación, evaluación con criterio.'),(9,20,'Lista de cotejo'),(10,21,'Aprendizaje'),(11,22,'proceso de formación'),(12,41,'Algoritmo,Finito,Definido,Preciso'),(15,43,'Algoritmo,Definido'),(16,44,'tecnologia educativa analoga'),(19,47,'Herramientas Tecnología'),(21,50,'Aulas digitales moviles'),(22,51,'Aula Invertida'),(24,53,'Disciplinas académicas'),(25,54,'digital, nativo, inmigrante'),(26,55,'Tecnología Educativa'),(27,62,'Texto'),(28,64,'Generacion'),(29,68,'Aplicaciones educativas'),(33,69,'herramienta, actualizacion, innovacion, plataformas...'),(34,70,'Rotafolio'),(35,71,'Rotafolio'),(36,73,'Videojuego'),(37,77,'Telecomunicaciones'),(38,78,'Web'),(39,79,'Alfabetización'),(40,83,'Pizarra Digital Interactiva'),(41,85,'Plataforma Virtual'),(42,87,'Formación digital'),(43,90,'Pizarra Interactiva/Digital'),(44,48,'Video Educativo'),(48,99,'PROGRAMA, ALGORITMO'),(52,110,'Telecomunicaciones'),(53,111,'Web'),(54,112,'Alfabetización'),(60,137,'Juan Esteban Martinez'),(66,144,'Alfabetización'),(67,145,'Telecomunicaciones'),(69,147,'Web'),(80,150,'Brecha'),(81,141,'informacion no procesada'),(82,148,'Disco'),(83,151,'Exclusión'),(84,155,'Empresa'),(85,156,'Link'),(86,157,'Procesador'),(89,166,'CNSIC'),(90,167,'Internet'),(91,168,'Redes Sociales'),(92,169,'Portafolio, Portfolio, Webfolio'),(93,186,'Weblog'),(94,186,'Blog'),(95,186,'Web'),(96,187,'Portafolio del profesor'),(97,187,'Reflexión'),(98,188,'autoevaluación'),(99,192,'Arbol'),(100,193,'Bosque'),(101,194,'Hoja'),(103,198,'Rúbrica, Evaluación, Retroalimentación'),(104,199,'Cognición, Autoaprendizaje'),(105,201,'Evaluación, Competencias'),(107,202,'Reflexivo'),(108,215,'Evidencias'),(109,215,'Evaluación'),(110,219,'Algoritmo, Definicion'),(112,220,'artefacto'),(113,221,'Bitácora de trabajo'),(115,223,'carpeta de aprendizaje'),(116,224,'innovación'),(117,222,'Kickstarter'),(119,228,'Vertices'),(120,229,'Arista'),(121,230,'Camino'),(122,239,'Robot'),(123,240,'Cyber'),(124,241,'Algoritmo'),(125,243,'Inteligencia'),(126,242,'Black box'),(127,244,'Desiciones'),(128,247,'Computer Vision'),(129,249,'Turing Machine'),(130,256,'Lingüística Computacional'),(131,256,'Procesamiento del Lenguaje Natural'),(132,258,'Agentes Virtuales'),(133,258,'Chatbots'),(134,260,'Retroalimentación'),(135,260,'App'),(136,260,'Web page'),(137,260,'Comunicación'),(138,263,'S'),(139,264,'F'),(140,265,'T'),(141,278,'Inteligencia Sintética'),(145,279,'Deep Learning'),(146,279,'Aprendizaje Profundo');
/*!40000 ALTER TABLE `mdl_glossary_alias` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-18  9:02:41
