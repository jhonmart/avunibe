-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mdl_scale_history`
--

DROP TABLE IF EXISTS `mdl_scale_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_scale_history` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `action` bigint(10) NOT NULL DEFAULT '0',
  `oldid` bigint(10) NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  `timemodified` bigint(10) DEFAULT NULL,
  `loggeduser` bigint(10) DEFAULT NULL,
  `courseid` bigint(10) NOT NULL DEFAULT '0',
  `userid` bigint(10) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `scale` longtext NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mdl_scalhist_act_ix` (`action`),
  KEY `mdl_scalhist_old_ix` (`oldid`),
  KEY `mdl_scalhist_cou_ix` (`courseid`),
  KEY `mdl_scalhist_log_ix` (`loggeduser`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='History table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_scale_history`
--

LOCK TABLES `mdl_scale_history` WRITE;
/*!40000 ALTER TABLE `mdl_scale_history` DISABLE KEYS */;
INSERT INTO `mdl_scale_history` VALUES (1,1,6,NULL,1486161527,3,36,3,'Escala Cualitativa','D, C, B, A','<p><strong>Equivalencia de la Escala.</strong></p>\r\n<p><strong>D= Insuficiente</strong><br /><strong>C= Regular</strong><br /><strong>B= Bueno</strong><br /><strong>A= Muy bueno / Excelente</strong></p>\r\n<p></p>'),(2,2,3,NULL,1486161627,3,0,3,'Calificación Cualitativa','C,B,A','<p>Estas escalas de calificaciones muestran la aprobación o no de una actividad especifica.</p>\r\n<p></p>'),(3,2,6,NULL,1486162672,3,36,3,'Escala Cuantitativa','F, D, C, B, A','<p><strong>Equivalencia de la Escala.</strong></p>\r\n<p><strong>F= Insificiente                                0-59<br /></strong><strong>D= Insuficiente                              60-69<br /></strong><strong>C= Sificiente                                  70-79                                  <br /></strong><strong>B= Bueno                                      80-89<br /></strong><strong>A= Excelente                                 90-100</strong></p>\r\n<p></p>'),(4,2,6,NULL,1486162730,3,0,3,'Escala Cuantitativa','F, D, C, B, A','<p><strong>Equivalencia de la Escala.</strong></p>\r\n<p><strong>F= Insificiente                                0-59<br /></strong><strong>D= Insuficiente                              60-69<br /></strong><strong>C= Sificiente                                  70-79                                  <br /></strong><strong>B= Bueno                                      80-89<br /></strong><strong>A= Excelente                                 90-100</strong></p>\r\n<p></p>'),(5,2,6,NULL,1486406723,3,0,3,'Calificación Cuantitativa','F, D, C, B, A\r\nF= Insificiente ; D= Insuficiente; C= Suficiente; B= Bueno;  A=Excelente                                                                       \r\n','<p><strong>Equivalencia de la Escala UNIBE.</strong></p>\r\n<p><strong>F= Insificiente                                0-59<br /></strong><strong>D= Insuficiente                              60-69<br /></strong><strong>C= Sificiente                                  70-79                                  <br /></strong><strong>B= Bueno                                      80-89<br /></strong><strong>A= Excelente                                 90-100</strong></p>\r\n<p></p>'),(6,2,6,NULL,1486406869,3,0,3,'Calificación Cuantitativa','F, D, C, B, A *\r\n F= Insificiente ; D= Insuficiente; C= Suficiente; B= Bueno;  A=Excelente                                                                       \r\n','<p><strong>Equivalencia de la Escala UNIBE.</strong></p>\r\n<p><strong>F= Insificiente                                0-59<br /></strong><strong>D= Insuficiente                              60-69<br /></strong><strong>C= Sificiente                                  70-79                                  <br /></strong><strong>B= Bueno                                      80-89<br /></strong><strong>A= Excelente                                 90-100</strong></p>\r\n<p></p>'),(7,2,6,NULL,1492824252,3,0,3,'Calificación Cuantitativa','F, D, C, B, A *\r\nF= Insuficiente D= Insuficiente; C= Suficiente; B= Bueno;  A=Excelente                                                                       \r\n','<p><strong>Equivalencia de la Escala UNIBE.</strong></p>\r\n<p><strong>F= Insuficiente                                0-59<br /></strong><strong>D= Insuficiente                              60-69<br /></strong><strong>C= Sificiente                                  70-79                                  <br /></strong><strong>B= Bueno                                      80-89<br /></strong><strong>A= Excelente                                 90-100</strong></p>\r\n<p></p>');
/*!40000 ALTER TABLE `mdl_scale_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-18  9:02:44
