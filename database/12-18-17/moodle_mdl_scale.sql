-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mdl_scale`
--

DROP TABLE IF EXISTS `mdl_scale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_scale` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `courseid` bigint(10) NOT NULL DEFAULT '0',
  `userid` bigint(10) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `scale` longtext NOT NULL,
  `description` longtext NOT NULL,
  `descriptionformat` tinyint(2) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mdl_scal_cou_ix` (`courseid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='Defines grading scales';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_scale`
--

LOCK TABLES `mdl_scale` WRITE;
/*!40000 ALTER TABLE `mdl_scale` DISABLE KEYS */;
INSERT INTO `mdl_scale` VALUES (1,0,0,'Separate and Connected ways of knowing','Mostly separate knowing,Separate and connected,Mostly connected knowing','The scale based on the theory of separate and connected knowing. This theory describes two different ways that we can evaluate and learn about the things we see and hear.<ul><li><strong>Separate knowers</strong> remain as objective as possible without including feelings and emotions. In a discussion with other people, they like to defend their own ideas, using logic to find holes in opponent\'s ideas.</li><li><strong>Connected knowers</strong> are more sensitive to other people. They are skilled at empathy and tends to listen and ask questions until they feel they can connect and \"understand things from their point of view\". They learn by trying to share the experiences that led to the knowledge they find in other people.</li></ul>',0,1482522157),(2,0,0,'Default competence scale','Not yet competent,Competent','A binary rating scale that provides no further information beyond whether someone has demonstrated proficiency or not.',0,1482522157),(3,0,3,'Calificación Cualitativa','C,B,A','<p>Estas escalas de calificaciones muestran la aprobación o no de una actividad especifica.</p>\r\n<p></p>',1,1486161627),(4,0,3,'Vías de conocimiento separadas y conectadas','Muestra un aprendizaje principalmente individualista,Ambos,Muestra un aprendizaje principalmente solidario','Individual posts can be rated using a scale based on the theory of <strong>separate and connected knowing</strong>.<br />\n<br />\n<br />\n<br />\nThis theory may help you to look at human interactions in a new way. It describes two different ways that we can evaluate and learn about the things we see and hear.<br />\n<br />\n<br />\n<br />\nAlthough each of us may use these two methods in different amounts at different times, it may be useful to imagine two people as examples, one who is a mostly separate knower (Jim) and the other a mostly connected knower (Mary).<br />\n <ul><li>Jim likes to remain as \'objective\' as possible without including his feelings and emotions. When in a discussion with other people who may have different ideas, he likes to defend his own ideas, using logic to find holes in his opponent\'s ideas. He is critical of new ideas unless they are proven facts from reputable sources such as textbooks, respected teachers or his own direct experience. Jim is a very <strong>separate knower</strong>.<br />\n<br />\n  </li><li>Mary is more sensitive to other people. She is skilled at empathy and tends to listen and ask questions until she feels she can connect and &quot;understand things from their point of view&quot;. She learns by trying to share the experiences that led to the knowledge she finds in other people. When talking to others, she avoids confrontation and will often try to help the other person if she can see a way to do so, using logical suggestions. Mary is a very <strong>connected knower</strong>.</li></ul> <br />\nDid you notice in these examples that the separate knower is male and the connected knower is female? Some studies have shown that statistically this tends to be the case, however individual people can be anywhere in the spectrum between these two extremes.<br />\n<br />\n<br />\n<br />\nFor a collaborative and effective group of learners it may be best if everyone were able to use BOTH ways of knowing.<br />\n<br />\n<br />\n<br />\nIn a particular situation like an online forum, a single post by a person may exhibit either of these characteristics, or even both. Someone who is generally very connected may post a very separate-sounding message, and vice versa. The purpose of rating each post using this scale is to:<br />\n <blockquote> <br />\na) help you think about these issues when reading other posts<br />\n<br />\nb) provide feedback to each author on how they are being seen by others<br />\n </blockquote> <br />\nThe results are not used towards student assessment in any way, they are just to help improve communication and learning.<br />\n <hr /> <br />\nIn case you\'re interested, here are some references to papers by the authors who originally developed these ideas:<br />\n <ul><li>Belenky, M.F., Clinchy, B.M., Goldberger, N.R., &amp; Tarule, J.M. (1986). <br />\n<br />\n    Women\'s ways of knowing: the development of self, voice, and mind. New York, <br />\n<br />\n    NY: Basic Books.</li><li>Clinchy, B.M. (1989a). The development of thoughtfulness in college women: <br />\n<br />\n    Integrating reason and care. American Behavioural Scientist, 32(6), 647-657.</li><li>Clinchy, B.M. (1989b). On critical thinking &amp; connected knowing. Liberal <br />\n<br />\n    education, 75(5), 14-19.</li><li>Clinchy, B.M. (1996). Connected and separate knowing; Toward a marriage <br />\n<br />\n    of two minds. In N.R. Goldberger, Tarule, J.M., Clinchy, B.M. &amp;</li><li>Belenky, M.F. (Eds.), Knowledge, Difference, and Power; Essays inspired <br />\n<br />\n    by &#8220;Women&#8217;s Ways of Knowing&#8221; (pp. 205-247). New York, NY: <br />\n<br />\n    Basic Books.</li><li>Galotti, K. M., Clinchy, B. M., Ainsworth, K., Lavin, B., &amp; Mansfield, <br />\n<br />\n    A. F. (1999). A New Way of Assessing Ways of Knowing: The Attitudes Towards <br />\n<br />\n    Thinking and Learning Survey (ATTLS). Sex Roles, 40(9/10), 745-766.</li><li>Galotti, K. M., Reimer, R. L., &amp; Drebus, D. W. (2001). Ways of knowing <br />\n<br />\n    as learning styles: Learning MAGIC with a partner. Sex Roles, 44(7/8), 419-436. <br />\n<br />\n    <br /></li></ul> <br />\n<br />\n<br />\n',1,1164917668),(5,0,3,'Christian Esquea','0,1,1.5','',1,1357659964),(6,0,3,'Calificación Cuantitativa','F, D, C, B, A *\r\nF= Insuficiente D= Insuficiente; C= Suficiente; B= Bueno;  A=Excelente                                                                       \r\n','<p><strong>Equivalencia de la Escala UNIBE.</strong></p>\r\n<p><strong>F= Insuficiente                                0-59<br /></strong><strong>D= Insuficiente                              60-69<br /></strong><strong>C= Sificiente                                  70-79                                  <br /></strong><strong>B= Bueno                                      80-89<br /></strong><strong>A= Excelente                                 90-100</strong></p>\r\n<p></p>',1,1492824252),(7,0,355,'Evaluación por escala','0,1,1.5,2,2.5,3,3.5,4,4.5,5','<p>Esto es una escala de Prueba</p>',1,1355149910),(8,0,9,'PRUA','0,0.1,0.2,0.3,0.4,0.5,0.6,0.7','',1,1357765151),(9,1343,355,'Escala en base a 20 ','1,5,10,15,20','<p>Escala en base a 20</p>',1,1406729622),(10,1343,355,'Escala de 20','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','',1,1406729677),(11,0,3,'Calificación Cuantitativa','F, D, C, B, A *\nF= Insuficiente D= Insuficiente; C= Suficiente; B= Bueno;  A=Excelente                                                                       \n','<p><strong>Equivalencia de la Escala UNIBE.</strong></p>\n<p><strong>F= Insuficiente                                0-59<br /></strong><strong>D= Insuficiente                              60-69<br /></strong><strong>C= Sificiente                                  70-79                                  <br /></strong><strong>B= Bueno                                      80-89<br /></strong><strong>A= Excelente                                 90-100</strong></p>\n<p></p>',1,1492824252),(12,1774,4927,'Escala en base a 20 ','1,5,10,15,20','<p>Escala en base a 20</p>',1,1406729622),(13,1774,4927,'Escala de 20','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','',1,1406729677),(14,2143,4927,'Escala en base a 20 ','1,5,10,15,20','<p>Escala en base a 20</p>',1,1406729622),(15,2143,4927,'Escala de 20','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','',1,1406729677);
/*!40000 ALTER TABLE `mdl_scale` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-18  9:04:53
