-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mdl_imscp`
--

DROP TABLE IF EXISTS `mdl_imscp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_imscp` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `course` bigint(10) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `intro` longtext,
  `introformat` smallint(4) NOT NULL DEFAULT '0',
  `revision` bigint(10) NOT NULL DEFAULT '0',
  `keepold` bigint(10) NOT NULL DEFAULT '-1',
  `structure` longtext,
  `timemodified` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mdl_imsc_cou_ix` (`course`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='each record is one imscp resource';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_imscp`
--

LOCK TABLES `mdl_imscp` WRITE;
/*!40000 ALTER TABLE `mdl_imscp` DISABLE KEYS */;
INSERT INTO `mdl_imscp` VALUES (1,497,'Agregar paquetes de contenidos IMS','Esta opción puede ser utilizada para subir informaciones compridas en formatos compatibles con la plataforma virtual. Ej. Aplicación Ex, Scorm ',1,1,1,'a:1:{i:0;a:4:{s:4:\"href\";s:10:\"index.html\";s:5:\"title\";s:4:\"Home\";s:5:\"level\";i:0;s:8:\"subitems\";a:0:{}}}',1271254567),(2,506,'Agregar paquetes de contenidos IMS','Esta opción puede ser utilizada para subir informaciones compridas en formatos compatibles con la plataforma virtual. Ej. Aplicación Ex, Scorm',1,1,1,'a:1:{i:0;a:4:{s:4:\"href\";s:10:\"index.html\";s:5:\"title\";s:4:\"Home\";s:5:\"level\";i:0;s:8:\"subitems\";a:0:{}}}',1247519471),(3,510,'Agregar paquetes de contenidos IMS',' Esta opción puede ser utilizada para subir informaciones compridas en formatos compatibles con la plataforma virtual. Ej. Aplicación Ex, Scorm ',1,1,1,'a:1:{i:0;a:4:{s:4:\"href\";s:10:\"index.html\";s:5:\"title\";s:4:\"Home\";s:5:\"level\";i:0;s:8:\"subitems\";a:0:{}}}',1247519471),(4,513,'Agregar paquetes de contenidos IMS','Esta opción puede ser utilizada para subir informaciones compridas en formatos compatibles con la plataforma virtual. Ej. Aplicación Ex, Scorm',1,1,1,'a:1:{i:0;a:4:{s:4:\"href\";s:10:\"index.html\";s:5:\"title\";s:4:\"Home\";s:5:\"level\";i:0;s:8:\"subitems\";a:0:{}}}',1247519471),(5,565,'COCINAS','CULTURAS GASTRONOMICAS ',1,1,1,NULL,1201311092),(6,604,'Recurso','',1,1,1,NULL,1254968609);
/*!40000 ALTER TABLE `mdl_imscp` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-18  9:03:42
