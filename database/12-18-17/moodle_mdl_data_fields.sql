-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mdl_data_fields`
--

DROP TABLE IF EXISTS `mdl_data_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_data_fields` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `dataid` bigint(10) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `param1` longtext,
  `param2` longtext,
  `param3` longtext,
  `param4` longtext,
  `param5` longtext,
  `param6` longtext,
  `param7` longtext,
  `param8` longtext,
  `param9` longtext,
  `param10` longtext,
  PRIMARY KEY (`id`),
  KEY `mdl_datafiel_typdat_ix` (`type`,`dataid`),
  KEY `mdl_datafiel_dat_ix` (`dataid`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED COMMENT='every field available';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_data_fields`
--

LOCK TABLES `mdl_data_fields` WRITE;
/*!40000 ALTER TABLE `mdl_data_fields` DISABLE KEYS */;
INSERT INTO `mdl_data_fields` VALUES (1,1,'text','Nombre del Estudiante','Nombre del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,1,'textarea','Nombre y Descripción Algoritmo1','Nombre y Descripción Algoritmo1',0,'','60','35',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,'url','Enlace a la Fuente de Algoritmo1','Enlace a la  Fuente de Algoritmo1',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,1,'textarea','Nombre y Descripción Algoritmo2','Nombre y Descripción Algoritmo2',0,'','60','35',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,1,'url','Enlace a la Fuente de Algoritmo2','Enlace a la  Fuente de Algoritmo2',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,2,'text','Termino','Concepto aportado',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,2,'text','Concepto','Texto aportado',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,3,'file','Cronograma de trabajo.','actividades evaluativas',0,'','','','','','','','','',''),(9,3,'file','Cronograma de Actividades.','Evaluaciones',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,4,'file','COCINAS DEL MUNDO','PRESENTACIONES',0,'','','','','','','','','',''),(11,5,'file','Consultoría de Samaná 1.','Lectura',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,8,'url','Enlace Obtenido','Enlace al Articulo',0,'1','','','','','','','','',''),(13,8,'text','Nombre del Estudiante','Nombre del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,8,'text','Matricula','Matricula',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,8,'date','Fecha','Fecha',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,9,'text','Nombre del Estudiante','Nombre del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,9,'text','Matrícula del Estudiante','Matrícula del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,9,'url','Enlace (Link) de lo Aportado','Enlace (Link) de lo Aportado',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,10,'text','Nombre del Estudiante','Nombre del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,10,'text','Matrícula del Estudiante','Matrícula del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,10,'url','Página Aportada','Página Aportada',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,10,'textarea','Comentario sobre la página','Comentario sobre la página',0,'','30','15',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,11,'menu','Cronograma','Febrero',0,'','','','','','','','','',''),(24,12,'menu','Cronograma','Febrero',0,'','','','','','','','','',''),(25,13,'url','HOW TO READ A PAPER','',0,'1','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,14,'file','Normativa','Reglamentacion del Poligono',0,'','','20000000','','','','','','',''),(28,18,'file','Cronograma de trabajo.','actividades evaluativas',0,'','','','','','','','','',''),(29,18,'file','Cronograma de Actividades.','Evaluaciones',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,19,'file','Cronograma de trabajo.','actividades evaluativas',0,'','','','','','','','','',''),(31,19,'file','Cronograma de Actividades.','Evaluaciones',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,20,'file','Normativa','Reglamentacion del Poligono',0,'','','20000000','','','','','','',''),(33,21,'file','Normativa','Reglamentacion del Poligono',0,'','','20000000','','','','','','',''),(34,22,'file','Normativa','Reglamentacion del Poligono',0,'','','20000000','','','','','','',''),(35,23,'file','Cronograma de trabajo.','actividades evaluativas',0,'','','','','','','','','',''),(36,23,'file','Cronograma de Actividades.','Evaluaciones',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,24,'file','Cronograma de trabajo.','actividades evaluativas',0,'','','','','','','','','',''),(38,24,'file','Cronograma de Actividades.','Evaluaciones',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,25,'text','Nombre del Estudiante','Nombre del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,25,'text','Matrícula del Estudiante','Matrícula del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(41,25,'url','Enlace (Link) de lo Aportado','Enlace (Link) de lo Aportado',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(42,26,'text','Nombre del Estudiante','Nombre del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(43,26,'text','Matrícula del Estudiante','Matrícula del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(44,26,'url','Página Aportada','Página Aportada',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(45,26,'textarea','Comentario sobre la página','Comentario sobre la página',0,'','30','15',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(46,27,'url','Enlace Obtenido','Enlace al Articulo',0,'1','','','','','','','','',''),(47,27,'text','Nombre del Estudiante','Nombre del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(48,27,'text','Matricula','Matricula',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(49,27,'date','Fecha','Fecha',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50,28,'url','Enlace Obtenido','Enlace al Articulo',0,'1','','','','','','','','',''),(51,28,'text','Nombre del Estudiante','Nombre del Estudiante',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(52,28,'text','Matricula','Matricula',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(53,28,'date','Fecha','Fecha',0,'','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `mdl_data_fields` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-18  9:02:49
