-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mdl_scorm`
--

DROP TABLE IF EXISTS `mdl_scorm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_scorm` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `course` bigint(10) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `scormtype` varchar(50) NOT NULL DEFAULT 'local',
  `reference` varchar(255) NOT NULL DEFAULT '',
  `intro` longtext NOT NULL,
  `introformat` smallint(4) NOT NULL DEFAULT '0',
  `version` varchar(9) NOT NULL DEFAULT '',
  `maxgrade` double NOT NULL DEFAULT '0',
  `grademethod` tinyint(2) NOT NULL DEFAULT '0',
  `whatgrade` bigint(10) NOT NULL DEFAULT '0',
  `maxattempt` bigint(10) NOT NULL DEFAULT '1',
  `forcecompleted` tinyint(1) NOT NULL DEFAULT '0',
  `forcenewattempt` tinyint(1) NOT NULL DEFAULT '0',
  `lastattemptlock` tinyint(1) NOT NULL DEFAULT '0',
  `masteryoverride` tinyint(1) NOT NULL DEFAULT '1',
  `displayattemptstatus` tinyint(1) NOT NULL DEFAULT '1',
  `displaycoursestructure` tinyint(1) NOT NULL DEFAULT '0',
  `updatefreq` tinyint(1) NOT NULL DEFAULT '0',
  `sha1hash` varchar(40) DEFAULT NULL,
  `md5hash` varchar(32) DEFAULT '',
  `revision` bigint(10) NOT NULL DEFAULT '0',
  `launch` bigint(10) NOT NULL DEFAULT '0',
  `skipview` tinyint(1) NOT NULL DEFAULT '1',
  `hidebrowse` tinyint(1) NOT NULL DEFAULT '0',
  `hidetoc` tinyint(1) NOT NULL DEFAULT '0',
  `nav` tinyint(1) NOT NULL DEFAULT '1',
  `navpositionleft` bigint(10) DEFAULT '-100',
  `navpositiontop` bigint(10) DEFAULT '-100',
  `auto` tinyint(1) NOT NULL DEFAULT '0',
  `popup` tinyint(1) NOT NULL DEFAULT '0',
  `options` varchar(255) NOT NULL DEFAULT '',
  `width` bigint(10) NOT NULL DEFAULT '100',
  `height` bigint(10) NOT NULL DEFAULT '600',
  `timeopen` bigint(10) NOT NULL DEFAULT '0',
  `timeclose` bigint(10) NOT NULL DEFAULT '0',
  `timemodified` bigint(10) NOT NULL DEFAULT '0',
  `completionstatusrequired` tinyint(1) DEFAULT NULL,
  `completionscorerequired` tinyint(2) DEFAULT NULL,
  `completionstatusallscos` tinyint(1) DEFAULT NULL,
  `displayactivityname` smallint(4) NOT NULL DEFAULT '1',
  `autocommit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mdl_scor_cou_ix` (`course`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='each table is one SCORM module and its configuration';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_scorm`
--

LOCK TABLES `mdl_scorm` WRITE;
/*!40000 ALTER TABLE `mdl_scorm` DISABLE KEYS */;
INSERT INTO `mdl_scorm` VALUES (9,597,'Identificando portales web 2.0 (3 puntos)','local','web20.zip','<p>Despues de la tarea anterior donde exploraste algunos portales web 2.0 trata de identificar estos portales mediantes las definiciones de sus funcionalidades. Tienes dos intentos, siempre se te contara la puntuacion mas alta.</p>',1,'SCORM_1.2',3,1,0,2,0,0,0,1,1,0,0,NULL,'b51f397da148b0cf2a7efbb429534bdb',0,24,1,0,1,1,-100,-100,0,1,'resizable=0,scrollbars=0,directories=0,location=0,menubar=0,toolbar=0,status=0',1000,1000,0,0,1338905116,NULL,NULL,NULL,1,0),(10,933,'Prueba Scorm','local','BenjaminAlcantaraMoreta/What_data_to_keep_and_why_module_4_v2.zip','<p>Prueba</p>',1,'SCORM_1.3',100,1,0,6,0,0,0,1,1,0,0,NULL,'16ffc631daf8479d0bf3e38b0ba0041d',0,26,0,0,0,1,-100,-100,0,1,'resizable=1,scrollbars=1,directories=0,location=0,menubar=0,toolbar=0,status=0',600,500,0,0,1479922704,NULL,NULL,NULL,1,0),(14,1713,'Prueba Scorm','local','BenjaminAlcantaraMoreta/What_data_to_keep_and_why_module_4_v2.zip','<p>Prueba</p>',1,'SCORM_1.3',100,1,0,6,0,0,0,1,1,0,0,NULL,'16ffc631daf8479d0bf3e38b0ba0041d',0,34,0,0,0,1,-100,-100,0,1,'resizable=1,scrollbars=1,directories=0,location=0,menubar=0,toolbar=0,status=0',600,500,0,0,1479922704,NULL,NULL,0,1,0),(16,758,'Recurso1','local','curso_Lenguajes_de_Programacion_II_20171005.zip','<p>Recurso test</p>',1,'SCORM_1.3',100,1,0,0,0,0,0,1,1,0,0,'2f4dd0262f3882a1322df055f869f95e5cf1de81','',1,38,0,0,0,1,-100,-100,0,0,'',100,500,0,0,0,NULL,NULL,0,1,0),(17,36,'hot','local','hot.zip','',0,'SCORM_1.2',100,1,0,0,0,0,0,1,1,0,0,'6b6c64277ce69bbf9914d8ee4818083bd86cb977','',1,40,0,0,0,1,-100,-100,0,0,'',100,500,0,0,0,NULL,NULL,0,1,0),(18,36,'ff','local','hot.zip','',1,'SCORM_1.2',100,1,0,0,0,0,0,1,1,0,0,'5ede45dc0d06e4ee92f50dc46baf58a92aca9aa2','',1,42,0,0,0,1,-100,-100,0,0,'',100,500,0,0,0,NULL,NULL,0,1,0),(19,36,'hot','local','hot.zip','',0,'SCORM_1.2',100,1,0,0,0,0,0,1,1,0,0,'99504591fd84d638b1d2fde1a1d4d2a0a47fb337','',1,44,0,0,0,1,-100,-100,0,0,'',100,500,0,0,0,NULL,NULL,0,1,0),(20,36,'hot','local','hot.zip','',0,'SCORM_1.2',100,1,0,0,0,0,0,1,1,0,0,'097a4e77c025d3ed461bafed781ed626c312c9fe','',1,46,0,0,0,1,-100,-100,0,0,'',100,500,0,0,0,NULL,NULL,0,1,0);
/*!40000 ALTER TABLE `mdl_scorm` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-18  9:03:38
