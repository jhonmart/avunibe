-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `mdl_hotpot`
--

LOCK TABLES `mdl_hotpot` WRITE;
/*!40000 ALTER TABLE `mdl_hotpot` DISABLE KEYS */;
INSERT INTO `mdl_hotpot` VALUES (1,36,'HotPot','/\\/phobias.jqz','hp_6_jquiz_xml',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511794479,0,0.00,0,0),(2,36,'HotPot','/\\/phobias.jqz','hp_6_jquiz_xml',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511794484,0,0.00,0,0),(3,36,'HotPot','/\\/phobias.jqz','hp_6_jquiz_xml',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511794724,0,0.00,0,0),(4,36,'HotPot','/\\/jj.zip','',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511795220,0,0.00,0,0),(5,36,'HotPot','/\\/kl.txt','',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511795279,0,0.00,0,0),(6,36,'HotPot','/\\/phobias1.jbc','',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511795368,0,0.00,0,0),(7,36,'qz','/\\/phobias.jqz','hp_6_jquiz_xml',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511797785,0,0.00,0,0),(8,36,'qz','/\\/phobias.htm','hp_6_jquiz_html',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511797879,0,0.00,0,0),(9,36,'qz','/\\/phobias.htm','hp_6_jquiz_html',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511799207,0,0.00,0,0),(10,36,'qz','/\\/phobias.jqz','hp_6_jquiz_xml',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511799275,0,0.00,0,0),(11,36,'qz','/\\/phobias.htm','hp_6_jquiz_html',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511799651,0,0.00,0,0),(12,36,'qz','/hotpotato/phobias.jqz','hp_6_jquiz_xml',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511802514,0,0.00,0,0),(13,36,'qz','/hotpotato/phobias.jqz','hp_6_jquiz_xml',0,'',0,0,100,0,'',1,0,0,'',1,0,83493,100,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',62415,0,1,100,0,0,1511802803,1511803272,0.00,0,0),(14,36,'The First Modern Olympic Games','/\\/olympics.jqz','hp_6_jquiz_xml',0,'',0,0,100,0,'',1,0,0,'',1,0,0,0,'0',1,3,0,'',0,0,0,'',0,'',0,0,0,0,0,0,'','',0,0,1,100,0,0,1512077161,0,0.00,0,0);
/*!40000 ALTER TABLE `mdl_hotpot` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08 10:40:33
