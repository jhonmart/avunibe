-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `mdl_mnet_session`
--

LOCK TABLES `mdl_mnet_session` WRITE;
/*!40000 ALTER TABLE `mdl_mnet_session` DISABLE KEYS */;
INSERT INTO `mdl_mnet_session` VALUES (74,6061,'18-0009','28922ac9cd14448fc4ee3b264ab65676b30f7900',3,'019b2cf9330edf5c21170e6d9f721624dbeac561',1507217192,'fc6vqa48aqp5esabihq15cdqp4',1507562762),(75,5655,'18-0400','6a2f1a1faeb74a2ae3cbb8d086c1a82c7c4b3973',3,'1993d703c71315fadd24f79c1a212f5143eab0f3',1507255694,'rkjjsh9uglnsl6a8hcenct2622',1507601264),(76,907,'17-0417','9ae72b34fad9bd2448ee3f4828f6125619403af1',3,'631b4a1978add5506f741b55a69a12f269f602c2',1507258385,'2kbj5tvlof19ub5ruu4dr01fa5',1507603955),(77,6202,'18-0235','19586169ef87d34f5d473617c7ddef07f6a31e21',3,'019b2cf9330edf5c21170e6d9f721624dbeac561',1507263634,'3hsdm1nptqnnf5mcviult5mdf3',1507609204),(78,923,'17-0039','60cde618bf624defc5a708fbecac39d603064755',3,'1993d703c71315fadd24f79c1a212f5143eab0f3',1507526760,'deb199ovr2t6bnkqb8t47c3sa4',1507872330),(81,2373,'17-1083','a1dced95508d3176310198f99e3e1ec7f433a709',3,'631b4a1978add5506f741b55a69a12f269f602c2',1507418447,'t9vq8j4c91ij2ims06qhvtkr80',1507764017),(83,929,'17-0311','d29fbaaf24e91201b7d6a045e242b060923fc99c',3,'1993d703c71315fadd24f79c1a212f5143eab0f3',1507519424,'bvep67es93irvk2b7n0qq2vj45',1507864994),(85,905,'17-0277','582d21cde8a26e0e66d23c763a2a7e4a820e8fdc',3,'6bff0b11fa66a9de4ca447473522787687478fe2',1507486679,'t7n13b44nerb12od4jam1c54n7',1507832249),(86,904,'17-0276','ce0030dde6948c790f1e7fc784071c2a241ead4c',3,'019b2cf9330edf5c21170e6d9f721624dbeac561',1507494271,'hjkgi2bt43aamdgf67tnntk0m5',1507839841),(87,924,'17-0056','11e8357b81b1efeae8243fddbafe848d825b45c5',3,'631b4a1978add5506f741b55a69a12f269f602c2',1507494898,'6j5j1dkfhr0fbplg5dpl2l21c4',1507840468),(89,6029,'18-0215','4fec2fbbe10f7059e10b46ce21c855ae72b9e07d',3,'ca7c96e96bbe6a5ea2322e4f220e1fa4f7305303',1507505541,'qniblcebd4pd8arphfg9d99jq3',1507851111),(90,942,'17-0610','1027bcaf4be8629a977cec12b87052c17ffb870f',3,'84d0c48b2df5b1df93a54302ca8a53d68e3a294c',1507505843,'obs9v28ljauajui7up3k3rfdu4',1507851413),(91,939,'17-0555','0053bf063fb0f040969d80ed4c69026408bd03ad',3,'64812b6e5959347f93e50b386a32566e5c186d36',1507507606,'np5c4v2830bbip7n2q1b8uiu81',1507853176),(93,946,'17-0725','61aacfa34af0074fcec21023805f9709e86ed000',3,'8dcdbad4bf097ed8ed8441fcc0ae38c85a9c2762',1507507840,'jdvehor31v15pjo8egbb4dos87',1507853410),(94,940,'17-0585','4e24e9be83ac2d54c3cdd32ee335d7c0015ec925',3,'c46f24d108d292583dbedc6204ba1e38b123c505',1507508069,'q6mr0pdvu1hvamvgq18ics6bl2',1507853639),(95,926,'17-0153','b1ac898b7b3e2efbb3e26550d5d937d99f95ceea',3,'1993d703c71315fadd24f79c1a212f5143eab0f3',1507508646,'ciueaoj33jrbgof1ammebtuq94',1507854216),(96,931,'17-0320','2a2362cd6444e908af6c04f9d9617681ceff2532',3,'1993d703c71315fadd24f79c1a212f5143eab0f3',1507520720,'hgb713rvvs8m28tfjsb15to5c6',1507866290),(97,945,'17-0715','6e1fba8119098ff8264f501050d970c8372f5126',3,'1993d703c71315fadd24f79c1a212f5143eab0f3',1507558849,'291t11a1mnbmoc70e9264osc12',1507904419);
/*!40000 ALTER TABLE `mdl_mnet_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08 10:42:25
