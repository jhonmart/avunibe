-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `mdl_scorm_scoes_track`
--

LOCK TABLES `mdl_scorm_scoes_track` WRITE;
/*!40000 ALTER TABLE `mdl_scorm_scoes_track` DISABLE KEYS */;
INSERT INTO `mdl_scorm_scoes_track` VALUES (15,663,16,38,1,'x.start.time','1507161243',1507161243),(16,663,16,38,1,'cmi.completion_status','completed',1507161378),(17,663,16,38,1,'cmi.suspend_data','1|1*0|0*0|0',1507161267),(18,663,16,38,1,'cmi.session_time','PT0000H02M26S',1507161506),(19,663,16,38,1,'cmi.exit','normal',1507161507),(20,663,16,38,1,'cmi.total_time','PT2M45S',1507161507),(21,663,16,38,1,'cmi.location','1_0',1507161492),(22,663,16,38,1,'cmi.score.scaled','1',1507161376),(23,663,16,38,1,'cmi.score.raw','100',1507161376),(24,663,16,38,1,'cmi.score.min','0',1507161377),(25,663,16,38,1,'cmi.score.max','100',1507161377),(26,43,14,34,1,'x.start.time','1508790418',1508790418),(27,6738,17,40,1,'x.start.time','1511799893',1511799893),(28,6738,17,40,1,'cmi.core.score.max','100',1511799894),(29,6738,17,40,1,'cmi.core.score.min','0',1511799895),(30,6738,17,40,1,'cmi.core.lesson_status','browsed',1511799895),(31,6738,17,40,1,'cmi.core.total_time','00:00:00.00',1511799897),(32,6738,18,42,1,'x.start.time','1511801503',1511801503),(33,6738,18,42,1,'cmi.core.score.max','100',1511801505),(34,6738,18,42,1,'cmi.core.score.min','0',1511801505),(35,6738,18,42,1,'cmi.core.lesson_status','browsed',1511801505),(36,6738,18,42,1,'cmi.core.total_time','00:00:00.00',1511801591),(37,6738,19,44,1,'x.start.time','1511801599',1511801599),(38,6738,19,44,1,'cmi.core.score.max','100',1511801600),(39,6738,19,44,1,'cmi.core.score.min','0',1511801600),(40,6738,19,44,1,'cmi.core.lesson_status','browsed',1511801601),(41,6738,19,44,1,'cmi.core.total_time','00:00:00.00',1511801607),(42,6738,20,46,1,'x.start.time','1511801880',1511801880),(43,6738,20,46,1,'cmi.core.score.max','100',1511801881),(44,6738,20,46,1,'cmi.core.score.min','0',1511801881),(45,6738,20,46,1,'cmi.core.lesson_status','browsed',1511801881),(46,6738,20,46,1,'cmi.core.total_time','00:00:00.00',1511801901);
/*!40000 ALTER TABLE `mdl_scorm_scoes_track` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08 10:43:03
