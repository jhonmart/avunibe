-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `mdl_backup_controllers`
--

LOCK TABLES `mdl_backup_controllers` WRITE;
/*!40000 ALTER TABLE `mdl_backup_controllers` DISABLE KEYS */;
INSERT INTO `mdl_backup_controllers` VALUES (14794,'acda088e7c0b5dc791a2aba77ea94cc5','backup','activity',28364,'moodle2',0,10,2,1000,1,0,'10e3b9f14dbdb4309164eb7243209df5',1505008293,1505008294,''),(14795,'eb69807244bf2ff0e16c83afc3a7f8dd','backup','activity',28366,'moodle2',0,10,2,1000,1,0,'21442b2d114438a8e8964240a367b953',1505008296,1505008297,''),(14796,'1797dd96845aa71d4db200afd4103d06','backup','activity',28367,'moodle2',0,10,2,1000,1,0,'43d83cab11fb2b31c94bc43682cbbece',1505008298,1505008299,''),(14797,'a08eb8cb42452b04cd2a746e8354c2b2','backup','activity',28328,'moodle2',0,10,2,1000,1,0,'4567926116704867e5a0a7125c1bb255',1505008593,1505008594,''),(14798,'97d31f48173c88fcc8348fa09168e897','backup','activity',28325,'moodle2',0,10,2,1000,1,0,'ce668409dd8dbcea78d6c2cde118e1c6',1505008596,1505008597,''),(14799,'53547aee05911fcdd22583be71511c56','backup','activity',28327,'moodle2',0,10,2,1000,1,0,'784b023b8b7b3ed9325dbe67b6321aa6',1505008598,1505008599,''),(14800,'b30050342dd3affebd37a892bf181655','backup','activity',22609,'moodle2',0,10,2,1000,1,0,'a6295746c67b42a60e5230db1b2ecc67',1505009192,1505009193,''),(14801,'afbb25cc46859aec5ad76e9753c518b7','backup','activity',22610,'moodle2',0,10,2,1000,1,0,'8c8e3f7b23cf06908605d12a801de5d2',1505009195,1505009196,''),(14802,'ec7d6855b9dff0d0b7351417176fb667','backup','activity',22611,'moodle2',0,10,2,1000,1,0,'e2d1b7450db451ccf8719ba7baef6642',1505009197,1505009198,''),(14803,'8c8c69dd6154de8129aa1019af904a15','backup','activity',22612,'moodle2',0,10,2,1000,1,0,'a8c902554cf73d7c8e411086c9539ad1',1505009200,1505009200,''),(14804,'42ee96cac19fe6e315bd7ea7cb2a680b','backup','activity',22613,'moodle2',0,10,2,1000,1,0,'0cb9a12e7355f991b7848d2acf7ee1e7',1505009202,1505009203,''),(14805,'b74385c744c7d452d0d995302c6ea79b','backup','activity',22621,'moodle2',0,10,2,1000,1,0,'7b748a144c151d71e22239784f6d6874',1505009792,1505009793,''),(14806,'69b53ab4ca0798633abecfebc9e9637c','backup','activity',22617,'moodle2',0,10,2,1000,1,0,'7193cc598033df9f2561d22e5f179066',1505009795,1505009796,''),(14807,'2c2a843faa3d4493333c88e62885ffee','backup','activity',22618,'moodle2',0,10,2,1000,1,0,'2cc2fa45e31568652ffa4cd7ea5f8d65',1505009798,1505009798,''),(14808,'d43cd4b118ba8883f9534157d264b4aa','backup','activity',22619,'moodle2',0,10,2,1000,1,0,'fdb2f9d17e00de4fa9ffc26bf027629a',1505009800,1505009801,''),(14809,'ad31080fea01985d591f46d3fbf5dddc','backup','activity',22620,'moodle2',0,10,2,1000,1,0,'314b88af7692a9ff4342b6861369afa1',1505009803,1505009803,''),(14810,'3573b3c60f17fde6c5ecd95f687f7223','backup','activity',27988,'moodle2',0,10,2,1000,1,0,'6e7e3aea22f129c3904dee3ffd323c6d',1505010092,1505010094,''),(14811,'90b4dc170d93fbf91bc564f8c1440eb5','backup','activity',27985,'moodle2',0,10,2,1000,1,0,'d21c27b0efeafa8dd3dc57e69db86c71',1505010096,1505010098,''),(14812,'dd3e1158c67312bc9839ce2edaf59077','backup','activity',27987,'moodle2',0,10,2,1000,1,0,'e49b21f7080c8748a102d21cedd554fa',1505010100,1505010101,''),(14813,'06e0e5b3334dfb962cb32567cd9d95ff','backup','activity',28042,'moodle2',0,10,2,1000,1,0,'73590a663e9367eeb22bc25693e9c648',1505012492,1505012494,''),(14814,'09c8745f233ccf74ee5f2db34e685a3c','backup','course',831,'moodle2',1,10,4927,1000,1,0,'be406bff58bcdd1a6951b2c57c31657f',1505050212,1505050258,''),(14815,'47c3b131b90aa214fb1aaa6adc08b506','restore','course',1943,'moodle2',1,10,4927,1000,1,0,'a5c6a2923eca11a85fd137227ce3eef2',1505050410,1505050502,''),(14816,'1f46f10112f21f48993f64004b9835fe','backup','activity',156416,'moodle2',0,10,2,1000,1,0,'5d368e33e8d3a52fce2634da8866d6ef',1505051493,1505051494,''),(14817,'8dc69a9780edd0333411f8f705dd92fb','backup','course',696,'moodle2',1,10,4927,1000,1,0,'b260d7002fb3d894e570a3c3da573e8c',1505052441,1505052462,''),(14818,'0dac48c1ffafeb68f6e16fc944e19c14','restore','course',1944,'moodle2',1,10,4927,1000,1,0,'140901cfb37e110332ffdff69489d5d2',1505052531,1505052612,''),(14819,'170eecf565b4a6e2bb692decfdce05e3','backup','course',1945,'moodle2',1,10,4927,1000,1,0,'e375339c3831617d2840ea77d35c269b',1505054996,1505055004,''),(14820,'3097c7c04ebd11cefe0fd62c6fb50811','restore','course',1946,'moodle2',1,10,4927,1000,1,0,'1102a34545548af7945ec9eff7c9317c',1505055226,1505055260,''),(14821,'d74dd552c03d6a984468006976580956','restore','course',1947,'moodle2',1,10,4927,1000,1,0,'29829f7b1e60f8191e6a65455efc4e58',1505055844,1505056085,''),(14822,'3fd5dd92425a8336c02fe5c03371368e','backup','course',2103,'moodle2',1,10,2,1000,1,0,'d61c953c3465eadf5f617500bee8ca82',1509732635,1509732699,'');
/*!40000 ALTER TABLE `mdl_backup_controllers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08 10:42:08
