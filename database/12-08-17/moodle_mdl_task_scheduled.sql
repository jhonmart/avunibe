-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `mdl_task_scheduled`
--

LOCK TABLES `mdl_task_scheduled` WRITE;
/*!40000 ALTER TABLE `mdl_task_scheduled` DISABLE KEYS */;
INSERT INTO `mdl_task_scheduled` VALUES (1,'moodle','\\core\\task\\session_cleanup_task',1507560089,1507560120,0,'*','*','*','*','*',0,0,0),(2,'moodle','\\core\\task\\delete_unconfirmed_users_task',1507557690,1507561200,0,'0','*','*','*','*',0,0,0),(3,'moodle','\\core\\task\\delete_incomplete_users_task',1507557988,1507561500,0,'5','*','*','*','*',0,0,0),(4,'moodle','\\core\\task\\backup_cleanup_task',1507558296,1507561800,0,'10','*','*','*','*',0,0,0),(5,'moodle','\\core\\task\\tag_cron_task',1509549029,1509606840,0,'14','3','*','*','*',0,0,0),(6,'moodle','\\core\\task\\context_cleanup_task',1507559194,1507562700,0,'25','*','*','*','*',0,0,0),(7,'moodle','\\core\\task\\cache_cleanup_task',1507559488,1507563000,0,'30','*','*','*','*',0,0,0),(8,'moodle','\\core\\task\\messaging_cleanup_task',1507560088,1507563300,0,'35','*','*','*','*',0,0,0),(9,'moodle','\\core\\task\\send_new_user_passwords_task',1507560089,1507560120,0,'*','*','*','*','*',0,0,0),(10,'moodle','\\core\\task\\send_failed_login_notifications_task',1507560089,1507560120,0,'*','*','*','*','*',0,0,0),(11,'moodle','\\core\\task\\create_contexts_task',1509549028,1509595200,1,'0','0','*','*','*',0,0,0),(12,'moodle','\\core\\task\\legacy_plugin_cron_task',1507560077,1507560120,0,'*','*','*','*','*',0,0,0),(13,'moodle','\\core\\task\\grade_cron_task',1507560077,1507560120,0,'*','*','*','*','*',0,0,0),(14,'moodle','\\core\\task\\events_cron_task',1507560077,1507560120,0,'*','*','*','*','*',0,0,0),(15,'moodle','\\core\\task\\completion_regular_task',1507560078,1507560120,0,'*','*','*','*','*',0,0,0),(16,'moodle','\\core\\task\\completion_daily_task',1507551089,1507637340,0,'9','8','*','*','*',0,0,0),(17,'moodle','\\core\\task\\portfolio_cron_task',1507560078,1507560120,0,'*','*','*','*','*',0,0,0),(18,'moodle','\\core\\task\\plagiarism_cron_task',1507560078,1507560120,0,'*','*','*','*','*',0,0,0),(19,'moodle','\\core\\task\\calendar_cron_task',1507560078,1507560120,0,'*','*','*','*','*',0,0,0),(20,'moodle','\\core\\task\\blog_cron_task',1507560078,1507560120,0,'*','*','*','*','*',0,0,0),(21,'moodle','\\core\\task\\question_cron_task',1507560078,1507560120,0,'*','*','*','*','*',0,0,0),(22,'moodle','\\core\\task\\registration_cron_task',1509549026,1509671520,0,'12','21','*','*','4',0,0,0),(23,'moodle','\\core\\task\\check_for_updates_task',1507557690,1507564800,0,'0','*/2','*','*','*',0,0,0),(24,'moodle','\\core\\task\\cache_cron_task',1507557088,1507560600,0,'50','*','*','*','*',0,0,0),(25,'moodle','\\core\\task\\automated_backup_task',1507557088,1507560600,0,'50','*','*','*','*',0,0,0),(26,'moodle','\\core\\task\\badges_cron_task',1507560078,1507560300,0,'*/5','*','*','*','*',0,0,0),(27,'moodle','\\core\\task\\file_temp_cleanup_task',1509549030,1509555300,0,'55','*/6','*','*','*',0,0,0),(28,'moodle','\\core\\task\\file_trash_cleanup_task',1507546620,1507568100,0,'55','*/6','*','*','*',0,0,0),(29,'moodle','\\core\\task\\search_index_task',1507559488,1507561200,0,'*/30','*','*','*','*',0,0,0),(30,'moodle','\\core\\task\\search_optimize_task',1509549028,1509552900,0,'15','*/12','*','*','*',0,0,0),(31,'moodle','\\core\\task\\stats_cron_task',1509549028,1509595200,0,'0','0','*','*','*',0,1,0),(32,'moodle','\\core\\task\\password_reset_cleanup_task',1509549030,1509552000,0,'0','*/6','*','*','*',0,0,0),(33,'moodle','\\core\\task\\complete_plans_task',1507559194,1507562520,0,'22','*','*','*','*',0,0,0),(34,'moodle','\\core\\task\\sync_plans_from_template_cohorts_task',1507558888,1507562400,0,'20','*','*','*','*',0,0,0),(35,'mod_forum','\\mod_forum\\task\\cron_task',1507560771,1507560780,0,'*','*','*','*','*',0,0,0),(36,'auth_cas','\\auth_cas\\task\\sync_task',0,1493784000,0,'0','0','*','*','*',0,0,1),(37,'auth_ldap','\\auth_ldap\\task\\sync_task',0,1493784000,0,'0','0','*','*','*',0,0,1),(38,'enrol_flatfile','\\enrol_flatfile\\task\\flatfile_sync_task',0,1493741700,0,'15','*','*','*','*',0,0,0),(39,'enrol_imsenterprise','\\enrol_imsenterprise\\task\\cron_task',0,1493741400,0,'10','*','*','*','*',0,0,0),(40,'enrol_lti','\\enrol_lti\\task\\sync_grades',0,1493740800,0,'*/30','*','*','*','*',0,0,0),(41,'enrol_lti','\\enrol_lti\\task\\sync_members',0,1493740800,0,'*/30','*','*','*','*',0,0,0),(42,'editor_atto','\\editor_atto\\task\\autosave_cleanup_task',0,1493957640,0,'14','0','*','*','5',0,0,0),(43,'tool_cohortroles','\\tool_cohortroles\\task\\cohort_role_sync',1507558588,1507562040,0,'14','*','*','*','*',0,0,0),(44,'tool_langimport','\\tool_langimport\\task\\update_langpacks_task',1509549030,1509610860,0,'21','4','*','*','*',0,0,0),(45,'tool_messageinbound','\\tool_messageinbound\\task\\pickup_task',1507560771,1507560780,0,'*','*','*','*','*',0,0,0),(46,'tool_messageinbound','\\tool_messageinbound\\task\\cleanup_task',1509549029,1509602100,0,'55','1','*','*','*',0,0,0),(47,'tool_monitor','\\tool_monitor\\task\\clean_events',1507560771,1507560780,0,'*','*','*','*','*',0,0,0),(48,'tool_monitor','\\tool_monitor\\task\\check_subscriptions',1509549026,1509564060,0,'21','15','*','*','*',0,0,0),(49,'tool_recyclebin','\\tool_recyclebin\\task\\cleanup_course_bin',1507559494,1507561200,0,'*/30','*','*','*','*',0,0,0),(50,'tool_recyclebin','\\tool_recyclebin\\task\\cleanup_category_bin',1507559494,1507561200,0,'*/30','*','*','*','*',0,0,0),(51,'assignfeedback_editpdf','\\assignfeedback_editpdf\\task\\convert_submissions',1507559499,1507560300,0,'*/15','*','*','*','*',0,0,0),(52,'logstore_legacy','\\logstore_legacy\\task\\cleanup_task',0,1493802960,0,'16','5','*','*','*',0,0,0),(53,'logstore_standard','\\logstore_standard\\task\\cleanup_task',1509549029,1509610140,0,'9','4','*','*','*',0,0,0);
/*!40000 ALTER TABLE `mdl_task_scheduled` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08 10:41:14
