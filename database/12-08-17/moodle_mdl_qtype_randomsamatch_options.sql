-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: moodle
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `mdl_qtype_randomsamatch_options`
--

LOCK TABLES `mdl_qtype_randomsamatch_options` WRITE;
/*!40000 ALTER TABLE `mdl_qtype_randomsamatch_options` DISABLE KEYS */;
INSERT INTO `mdl_qtype_randomsamatch_options` VALUES (2,29817,6,1,'',1,'',1,'',1,0),(3,38844,4,1,'',1,'',1,'',1,0),(4,63559,2,1,'',1,'',1,'',1,0),(5,63616,3,1,'',1,'',1,'',1,0),(6,69010,2,1,'',1,'',1,'',1,0),(7,72014,5,1,'',1,'',1,'',1,0),(8,83308,2,1,'',1,'',1,'',1,0),(9,83551,3,1,'',1,'',1,'',1,0),(10,83700,4,1,'',1,'',1,'',1,0),(11,91224,2,1,'',1,'',1,'',1,0),(12,91467,3,1,'',1,'',1,'',1,0),(13,91616,4,1,'',1,'',1,'',1,0),(14,94137,2,1,'',1,'',1,'',1,0),(15,94380,3,1,'',1,'',1,'',1,0),(16,94529,4,1,'',1,'',1,'',1,0),(17,114605,2,1,'',1,'',1,'',1,0),(18,114848,3,1,'',1,'',1,'',1,0),(19,114997,4,1,'',1,'',1,'',1,0),(20,116002,2,1,'',1,'',1,'',1,0),(21,116245,3,1,'',1,'',1,'',1,0),(22,116394,4,1,'',1,'',1,'',1,0),(23,117833,2,1,'',1,'',1,'',1,0),(24,118076,3,1,'',1,'',1,'',1,0),(25,118225,4,1,'',1,'',1,'',1,0);
/*!40000 ALTER TABLE `mdl_qtype_randomsamatch_options` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08 10:42:56
