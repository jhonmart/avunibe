<?php

// API DE CURSOS DESTACADOS

set_time_limit(0);

require_once(__DIR__.'../../../config.php');

error_reporting(E_ALL);

class FeaturedCourse{

    public $db;
    public $user;

    public function __construct(){
        global $DB, $USER;

        $this->db = $DB;

        // HABILITAR A TRUE PARA SALIDA SQL
        $this->db->set_debug(false);

        $this->user = $USER;
    }
    // TRAE TODOS LOS REGISTROS POR USUARIO
    public function index(){

        $request = array();
        // TRAE LOS DATOS DE LA TABLA course, VALIDANDO LOS CURSOS QUE EL USUARIO TIENE REGISTRADO

        $sql = 'SELECT course.id, course.fullname FROM {course} course JOIN {courses_featured} cf ON cf.id_course = course.id WHERE cf.id_user ='.$this->user->id.';';

        $courses = $this->db->get_records_sql($sql);

        foreach ($courses as $record){
            if(count($record) > 0){
                array_push($request, array(
                    'course'=>$record
                ));
            }
        }

        echo json_encode($request);
    }
    // NUEVO CURSO DESTACADO SI NO EXISTE, SI EXISTE EL CURSO LO ELIMINA
    public function add($id_course){
        $id_course = intval($id_course);
        $request = array();
        // COMPRUEBA SI EL CURSO EXISTE ACTUALMENTE
        $exists = $this->db->record_exists('courses_featured', array('id_course'=>$id_course));

        // TRAE LOS DATOS DE LA TABLA users y courses
        $sql = 'SELECT course.id, course.fullname FROM {course} course JOIN {courses_featured} cf ON cf.id_course = course.id WHERE cf.id_user ='.$this->user->id.';';

        $courses = $this->db->get_records_sql($sql);

        foreach ($courses as $record){
            array_push($request, array(
                'course'=>$record
            ));
        }

        if(!$exists){
            $record = new stdClass();
            $record->id_user = $this->user->id;
            $record->id_course = $id_course;

            $this->db->insert_record('courses_featured', $record);

        }else{
            $this->remove($id_course);
        }

        echo json_encode($request);
    }

    // REMUEVE UN CURSO SELECCIONADO SI EXISTE EN EL LOG DEL USUARIO
    public function remove($id_course){

        $exists = $this->db->record_exists('courses_featured', array('id_course'=>$id_course));

        if($exists){
            $this->db->delete_records('courses_featured', array('id_course'=>$id_course));
        }else{
            print('No existe este curso en destacados para eliminarlo.');
        }
    }

    // REMUEVE TODOS LOS CURSOS DESTACADOS POR USUARIO
    public function removeAll(){
        $rs = $this->db->get_recordset('courses_featured', array('id_user'=>$this->user->id));

        if($rs->valid()){
            $this->db->delete_records('courses_featured', array('id_user'=>$this->user->id));
        }else{
            print('Sin cursos destacados.');
        }

    }

}


?>