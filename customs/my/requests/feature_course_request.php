<?php
require_once(__DIR__.'../../featured_course.php');

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':
        $fc = new FeaturedCourse();
        $fc->index();
        break;
    case 'POST':
        $fc = new FeaturedCourse();

        if(isset($_POST['add'])){
            $id_course = str_replace('course-', '', $_POST['add']);
            $fc->add($id_course);
        }
        else if(isset($_POST['delete'])){
            $id_course = str_replace('course-', '', $_POST['delete']);
            $fc->remove($id_course);
        }
        else if(isset($_POST['delete_all'])){
            $fc->removeAll();
            print 'No hay cursos seleccionados como destacados.';
        }

        break;
}

?>