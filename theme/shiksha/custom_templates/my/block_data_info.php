<?php
// ARCHIVO MUESTRA DATOS A LOS ADMINISTRADORES DE DISTINTAS TABLAS EN LA BASE DE DATOS

// Objeto trae los datos guardados en los lenguajes y en la base de datos
$block_data_info = array(
    'courses'=> [
        'id' => 'show_search_course',
        'title' => '<i class="fa fa-university" aria-hidden="true"></i> ' . get_string('courses'),
        'value' => $DB->count_records('course'),
        'show_all' => get_string('search') . ' ' . get_string('course')
    ],
    'participants' => [
        'id' => 'show_search_participant',
        'title' => '<i class="fa fa-users" aria-hidden="true"></i> ' . get_string('participants'),
        'value' => $DB->count_records("user"),
        'show_all' => get_string('search'). ' ' . get_string('participant')
    ],
);

?>

<div class="resume_admin">
<?php
// Recorrido de datos a mostrar
    foreach ($block_data_info as $list_blocks){
        echo '<div id="'. $list_blocks['id'] .'" class="item_resume_admin">'
                .'<h4>'. $list_blocks['title'] .'</h4>'
                .'<p>'. number_format($list_blocks['value'], 0, '', ',') .'</p>'
                .'<label class="show_all">'. '<i class="fa fa-search" aria-hidden="true" id="yui_3_17_2_1_1511798421886_240"></i> ' .$list_blocks['show_all'].'</label>'
            .'</div>';
    }

    include_once('modals/search_course_modal.php');
    include_once('modals/search_participant_modal.php');

?>

</div>