<div class="">

    <h3 class="title_dest_course"><i class="fa fa-star" aria-hidden="true"></i> <?php echo get_string('featured_courses'); ?></h3>

    <div class="herrs_dest_course">
        <button id="btn_delete_all_dest" class="btn btn_delete_all_dest"><i class="fa fa-trash icon_herr_dest_course" aria-hidden="true"></i> <?php echo get_string('deleteall'); ?></button>
    </div>

    <div id="list_selected_courses" class="list_selected_courses"><?php echo get_string('featured_courses_no_data'); ?> </div>

    <script>
        $(document).ready(function () {
            selected_course._init_();
        });
    </script>

</div>

