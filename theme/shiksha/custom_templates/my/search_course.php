<div class="con_search_course_my">
    <label for="search_course" class="label_buscar_curso"><i class="fa fa-search" aria-hidden="true"></i> <?php echo get_string('search') . ' ' . get_string('course')?></label>
    <input type="text" id="search_course" placeholder="<?php echo get_string('search_course_placeholder'); ?>">
</div>