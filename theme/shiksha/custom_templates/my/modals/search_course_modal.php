<div class="background_for_open_modal_courses" style="display: none;">
    <div id="modal_search_course" class="modal fase" role="dialog" style="display: none;">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <input type="text" id="search_data_in_list" class="search_read" placeholder="<?php echo 'Buscar por: código | nombre' ?>" style="margin:auto;">
                    <button class="search_go" class="btn btn-success"><i class="fa fa-search" aria-hidden="true" id="yui_3_17_2_1_1511798421886_240"></i></button>
                </div>
                <div class="modal-body body_modal_search">
                    <table class="table_search">
                        <thead>
                            <th>C&oacute;digo</th>
                            <th>Nombre</th>
                            <th>Ir</th>
                        </thead>
                        <tbody>
                        <?php
                        $rs  =  $DB->get_recordset('course');
                        foreach ($rs as $record){
                            echo '<tr class="item_row_to_search">'
                                .'<td>'.$record->shortname.'</td>'
                                .'<td>'.$record->fullname.'</td>'
                                .'<td><a class="link_in_search_modal" href="'.$CFG->wwwroot.'/course/view.php?id='.$record->id.'"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a></td>'
                                .'</tr>';
                        }
                        $rs->close();

                        ?>
                        </tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_modal" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>