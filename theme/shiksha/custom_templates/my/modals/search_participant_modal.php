<div class="background_for_open_modal_part" style="display: none;">
    <div id="modal_search_participant" class="modal fase" role="dialog" style="display: none;">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <input type="text" id="search_data_in_list" class="search_read" placeholder="<?php echo 'Buscar por: usuario | nombre ' ?>" style="margin:auto;">
                    <button class="search_go" class="btn btn-success"><i class="fa fa-search" aria-hidden="true" id="yui_3_17_2_1_1511798421886_240"></i></button>
                </div>
                <div class="modal-body body_modal_search">
                    <table class="table_search">
                        <thead>
                        <th>Usuario</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Ir</th>
                        </thead>
                        <tbody>
                        <?php
                        $rs  =  $DB->get_recordset('user');
                        foreach ($rs  as  $record)  {
                            echo '<tr class="item_row_to_search">'
                                .'<td class="to_search">'.$record->username.'</td>'
                                .'<td class="to_search">'.$record->firstname.'</td>'
                                .'<td class="to_search">'.$record->lastname.'</td>'
                                .'<td><a class="link_in_search_modal" href="'.$CFG->wwwroot.'/user/profile.php?id='.$record->id.'"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a></td>'
                                .'</tr>';
                        }
                        $rs->close();

                        ?>
                        </tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close_modal" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>
