<?php
$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Shiksha</h2>
<p><img class=img-polaroid src="shiksha/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>Theme Credits</h3>
<p>Authors: Ensine Knowledge System PVT LTD.<br>
Contact: shiksha@dualcube.com<br>
Website: <a href="https://dualcube.com/">https://dualcube.com/</a>
</p>
</div></div>';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['generalsettings' ] = 'General settings';
$string['colorsettings'] = 'Color settings';
$string['fontsettings' ] = 'Font settings';
$string['frontpagesettings'] = 'Frontpage settings';
$string['configtitle'] = 'Shiksha';
$string['urlvalue'] = 'javascript:void(0);';
$string['msgsent'] = 'Message sent';
$string['emptynameemail'] = 'Please enter name and email address.';
/*controller for sections*/
$string['enable'] = 'Enable section';
$string['enabledesc'] = 'If You enable this section this section will be visible, otherwise not';
$string['on'] = 'Enable';
$string['off'] = 'Disable';
/*logo*/
$string['logo'] = 'Logo';
$string['logodesc'] = 'You may add the logo to be displayed on the header. Note- Preferred aspect ratio is 4:1. In case you wish to customise, you can do so from the     custom CSS box below.';
$string['logobackgroundimage'] = 'Header background image'; 
$string['logobackgroundimagedesc'] = 'You may add a header image for the logo.';
$string['logoorsitename'] = 'Choose site logo format';
$string['logoorsitenamedesc'] = 'You may customise how the site header logo looks like. The options available are: Logo - Only the logo will be shown; Sitename - Only the sitename will be shown; Icon+sitename - An icon along with the sitename will be shown.';//
$string['onlylogo'] = 'Logo only';
$string['onlysitename'] = 'Sitename only';
$string['iconsitename'] = 'Icon and sitename both';
$string['logoicon'] = 'Logoicon';
$string['logoicondesc'] = 'You may add the logo icon to be displayed.';
$string['pluginname'] = 'Shiksha';

/*favicon*/
$string['favicon'] = 'Favicon';
$string['favicondesc'] = 'Your site’s “favourite icon”. Here, you may insert the favicon for your site.';

/*custom css*/
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'You may customise the CSS from the text box above. The changes will be reflected on all the pages of your site.';

/*footnotes*/
$string['rightfootnote'] = 'Insert right footnote';
$string['rightfootnotedesc'] = 'You may insert a footnote to be displayed on the right side of the footer throughout the site.';
$string['rightfootnotevalue'] = '© 2015 Shiksha. All rights reserved.';

$string['leftfootnotesection1'] = 'Insert footnote section 1 (left)';
$string['leftfootnotedescsection1'] = 'You may insert a text to be displayed.';
$string['leftfootnotesection1value'] = 'PRIVACY POLICY';

$string['leftfootnotesection2'] = 'Insert footnote section 2 (left)';
$string['leftfootnotedescsection2'] = 'You may insert a text to be displayed.';
$string['leftfootnotesection2value'] = 'TERMS & CONDITIONS';

$string['leftfootnotesection3'] = 'Insert footnote section 3 (left)';
$string['leftfootnotedescsection3'] = 'You may insert a text to be displayed.';
$string['leftfootnotesection3value'] = 'CONTACT US';

$string['leftfootnotesection4'] = 'Insert footnote section 4 (left)';
$string['leftfootnotedescsection4'] = 'You may insert a text to be displayed.';
$string['leftfootnotesection4value'] = 'CREDITS';


$string['leftfootnotesectionlink1'] = 'Footnote section 1 link (left)';
$string['leftfootnotelinkdescsection1'] = 'You may insert the link to be redirected at, for the above text.';
$string['leftfootnotesectionlink2'] = 'Footnote section 2 link (left)';
$string['leftfootnotelinkdescsection2'] = 'You may insert the link to be redirected at, for the above text.';
$string['leftfootnotesectionlink3'] = 'Footnote section 3 link (left)';
$string['leftfootnotelinkdescsection3'] = 'You may insert the link to be redirected at, for the above text.';
$string['leftfootnotesectionlink4'] = 'Footnote section 4 link (left)';
$string['leftfootnotelinkdescsection4'] = 'You may insert the link to be redirected at, for the above text.';


/************************************font section start*********************************/

$string['fontselectdesc'] = 'You may choose from the ‘Standard’ fonts or add your customised fonts by selecting ‘Custom’ font.';
$string['fonttypestandard'] = 'Font type standard';
$string['fonttypecustom'] = 'Font type custom';
$string['fontselect'] = 'Select font';
$string['fontnameheading'] = 'Fontname for heading';
$string['fontnameheadingdesc'] = 'You may add the font name for the headings in your site i.e. site headings will be displayed in this font.';

$string['fontnamebody'] = 'Fontname for body';
$string['fontnamebodydesc'] = 'You may add the font name for the body of your site  i.e. the body of the site will be displayed in this font.';

$string['fontfiles'] = 'Font files';
$string['fontfilesdesc'] = 'Upload your font files here.';
$string['fontfilettfheading'] = 'Heading TTF font file';
$string['fontfileotfheading'] = 'Heading OTF font file';
$string['fontfilewoffheading'] = 'Heading WOFF font file';
$string['fontfilewofftwoheading'] = 'Heading WOFF2 font file';
$string['fontfileeotheading'] = 'Heading EOT font file';
$string['fontfilesvgheading'] = 'Heading SVG font file';
$string['fontfilettfbody'] = 'Body TTF font file';
$string['fontfileotfbody'] = 'Body OTF font file';
$string['fontfilewoffbody'] = 'Body WOFF font file';
$string['fontfilewofftwobody'] = 'Body WOFF2 font file';
$string['fontfileeotbody'] = 'Body EOT font file';
$string['fontfilesvgbody'] = 'Body SVG font file';
/************************************font section end*********************************/


/********************color section start*************************/
$string['color'] = 'Pick a color scheme';
$string['colordesc'] = 'You may select a color scheme for your theme from the available options.';
/********************color section end*************************/


// Analytics.
$string['analytics'] = 'Analytics';
$string['analyticsheadingsub'] = 'Powerful analytics for Moodle';
$string['analyticsdesc'] = '';
$string['analyticstrackingid'] = 'Google analytics ID';
$string['analyticstrackingiddesc'] = 'Here, you may insert the GA tracking ID created exclusively for your site.';
$string['analyticstrackingscript'] = 'Google Analytics Script';
$string['analyticstrackingscriptdesc'] = 'Copy the GA script from the google analytics page and paste it here.';

/*theme_Shiksha_frontpage*/

$string['frontpageimagecontent'] = 'Header content';
$string['frontpageimagecontentdesc'] = ' This section relates to the top portion of your frontpage.';
$string['frontpageimagecontentstyle'] = 'Style';
$string['frontpageimagecontentstyledesc'] = 'You can choose between Static content & Slider content.';
$string['staticcontent'] = 'Static content';
$string['slidercontent'] = 'Slider content';
$string['addtext'] = 'Add text';
$string['addtextdesc'] = 'Here you may add the text to be displayed on the front page, preferably in HTML.';
$string['addtextvalue'] = '<h2>The great aim of education is not knowledge but action.</h2><h4>Education not only broadens the mind but adds the most important component for critical thought, depth.</h4>';
$string['addlink'] = 'Add URL';
$string['addlinkdesc'] = 'You may add a URL that the user will be redirected at if they click on the button.';
$string['addbuttontext'] = 'Add button text';
$string['addbuttontextdesc'] = '';
$string['addbuttontextvalue'] = 'View programs';
$string['bannerimage'] = 'Upload bannerimage';
$string['bannerimagedesc'] = '';
$string['slidercount'] = 'No of slides';
$string['slidercountdesc'] = '';
$string['catslidercount'] = 'No of category slides';
$string['catslidercountdesc'] = '';
$string['catslideimage'] = 'Upload images for slider';
$string['catslideimagedesc'] = 'You may upload the images for the slider.';
$string['catslidertext'] = 'Enter text for slider';
$string['catslidertextdesc'] = 'You may upload the text for the slider.';
$string['catslidertextvalue'] = 'Category';
$string['catsliderurl'] = 'Enter link address for slider';
$string['catsliderurldesc'] = 'You may enter the address where the user will be redirected to.';
$string['one'] = '1';
$string['two'] = '2';
$string['three'] = '3';
$string['four'] = '4';
$string['five'] = '5';
$string['six'] = '6';
$string['seven'] = '7';
$string['eight'] = '8';
$string['nine'] = '9';
$string['ten'] = '10';


$string['slideimage'] = 'Upload images for Slider';
$string['slideimagedesc'] = 'You may insert the necessary images for the slider content.';
$string['slidertext'] = 'Add slider text';
$string['slidertextdesc'] = 'You may insert the text content of your slider. Preferably in HTML.';
$string['sliderurl'] = 'Slider text button link';
$string['sliderbuttontext'] = 'Add text button on slider'; 
$string['sliderbuttontextdesc'] = 'You may insert a text button on your slider.';
$string['sliderurldesc'] = 'You may insert the link of the page where the user will be redirected at once they click on the text button.';
$string['slideinterval'] = 'Slide interval';
$string['slideintervaldesc'] = 'You may set the transition time between the slides. In case if there is only a single slide, this option will have no effect. Enter 0 to stop autoplay';

$string['true'] = 'Yes';
$string['false'] = 'No';
$string['slidereffect'] = 'Slider effect';
$string['bannereffect'] = 'Static banner effect';
$string['slidereffectdesc'] = '';
$string['catslidersection'] = 'Category slider section';
$string['catslidersectiondesc'] = 'Category slider section';
$string['repeat'] = 'Repeat';
$string['static'] = 'Static';
$string['parallax'] = 'Parallax';
$string['cover'] = 'Cover';
$string['sliderbackgroundcolor'] = 'Slider background color';
$string['sliderbackgroundcolordesc'] = '';

$string['staticbackgroundcolor'] = 'Slider background color';
$string['staticbackgroundcolordesc'] = '';
$string['frontpageblocks'] = 'News and update section';
$string['frontpageblocksdesc'] = 'News and update section Desc';

$string['frontpageblockheading'] = 'Body heading';
$string['frontpageblockheadingdesc'] = '';
$string['frontpageblockheadingvalue'] = 'News &amp; Updates';

$string['frontpageblock'] = 'Body text';
$string['frontpageblockdesc'] = 'You may add a text to be shown below the heading.';
$string['frontpageblockvalue'] = 'View calendar';

$string['frontpageblocklink'] = 'Body text link';
$string['frontpageblocklinkdesc'] = 'You may insert the link for the body text, where the user will be redirected after they click on it.';

/*block section 1*/
$string['frontpageblocksection1'] = 'Body section 1 title';
$string['frontpageblocksectiondesc1'] = 'This is where you may add the text for section 1';
$string['frontpageblocksection1value'] = "Ahead of World Teachers' Day, UN highlights education challenges:";
$string['frontpageblocklinksection1'] = 'Body section 1 link';
$string['frontpageblocklinksectiondesc1'] = 'Here, you may insert the link at which the user will be redirected at.';
$string['frontpageblockdescriptionsection1'] = 'Body section 1 description';
$string['frontpageblockdescriptionsectiondesc1'] = 'A brief description in the context of the title.';
$string['frontpageblockdescriptionsection1value'] = 'Friday, July 27, 2015';

$string['frontpageblocksection2'] = 'Body section 2 title';
$string['frontpageblocksectiondesc2'] = 'This is where you may add the text for section 2';
$string['frontpageblocksection2value'] = "MIT launches first ever free online courses that could lead to degree:";
$string['frontpageblocklinksection2'] = 'Body section 2 link';
$string['frontpageblocklinksectiondesc2'] = 'Here, you may insert the link at which the user will be redirected at.';
$string['frontpageblockdescriptionsection2'] = 'Body section 2 description';
$string['frontpageblockdescriptionsectiondesc2'] = 'A brief description in the context of the title.';
$string['frontpageblockdescriptionsection2value'] = 'Monday, August 15, 2015';

$string['frontpageblocksection3'] = 'Body section 3 title';
$string['frontpageblocksectiondesc3'] = 'This is where you may add the text for section 3';
$string['frontpageblocksection3value'] = 'Essentials for Effective Online Courses in K-12:';
$string['frontpageblocklinksection3'] = 'Body section 3 link';
$string['frontpageblocklinksectiondesc3'] = 'Here, you may insert the link at which the user will be redirected at.';
$string['frontpageblockdescriptionsection3'] = 'Body section 3 description';
$string['frontpageblockdescriptionsectiondesc3'] = 'A brief description in the context of the title.';
$string['frontpageblockdescriptionsection3value'] = 'Saturday, June 5, 2015';

$string['contactwithusheading'] = '‘Contact Us’ text customisation';
$string['contactwithusheadingdesc'] = 'You may change the text content here. The default is ‘Contact Us’.';
$string['contactwithusfontawesomeicon'] = 'Upload the ‘Contact Us’ icon';
$string['contactwithusfontawesomeicondesc'] = 'You may add an icon to be displayed beside the text ‘ Contact Us’.';
$string['contactwithus'] = 'Footer section';
$string['contactwithusdesc'] = '';
$string['footerlogo'] = 'Footer logo';
$string['footerlogodesc'] = 'You may add the logo which will be displayed on the footer. Footer logo section is on the left side of the footer row.';


$string['socialicon1'] = 'Upload social link icon 1';
$string['socialicondesc1'] = 'You may upload the icon for social link 1.';
$string['socialiconlink1'] = 'Enter social link 1';
$string['socialiconlinkdesc1'] = '';

$string['socialicon2'] = 'Upload social link icon 2';
$string['socialicondesc2'] = 'You may upload the icon for social link 2.';
$string['socialiconlink2'] = 'Enter social link 2';
$string['socialiconlinkdesc2'] = '';

$string['socialicon3'] = 'Upload social link icon 3';
$string['socialicondesc3'] = 'You may upload the icon for social link 3.';
$string['socialiconlink3'] = 'Enter social link 3';
$string['socialiconlinkdesc3'] = '';

$string['socialicon4'] = 'Upload social link icon 4';
$string['socialicondesc4'] = 'You may upload the icon for social link 4.';
$string['socialiconlink4'] = 'Enter social link 3';
$string['socialiconlinkdesc4'] = '';

$string['address'] = 'Enter Address';
$string['addressdesc'] = 'You may enter your Address.';
$string['addressvalue'] = 'BB 164, Salt Lake Sector 1 Kolkata 700064 West Bengal, India';

$string['phoneheading'] = 'Enter text to be displayed for telephone';
$string['phoneheadingdesc'] = 'You may add custom text to be displayed instead of telephone, for example- mobile. Default text- ‘Tel :’';
$string['phoneheadingvalue'] = 'Tel :';
$string['phone'] = 'Enter Phone Number';
$string['phonedesc'] = 'You may enter your Phone Number.';
$string['phonevalue'] = '+91 33 64578322';

$string['faxheading'] = 'Enter text to be displayed for fax - ';
$string['faxheadingdesc'] = 'You may add text to be displayed instead of Fax. Default text - ‘Fax: ’';
$string['faxheadingvalue'] = 'Fax :';
$string['fax'] = 'Enter Fax Number';
$string['faxdesc'] = 'You may enter your Fax Number.';
$string['faxvalue'] = '+91 33 64578322';

$string['emailheading'] = 'Enter text to be displayed for Email';
$string['emailheadingdesc'] = 'You may add custom text to be displayed instead of email, Default text- ‘Email :’';
$string['emailheadingvalue'] = 'Email :';
$string['email'] = 'Enter Email Address';
$string['emaildesc'] = 'You may enter your Email.';
$string['emailvalue'] = 'shiksha@dualcube.com';

$string['someinformation'] = ' Enter text to be displayed below footer logo';
$string['someinformationdesc'] = 'You may add some brief text to be displayed below the footer';
$string['someinformationvalue'] = '<h4>Sophie Gaston</h4><span>Press and Communications Manager</span>';

$string['quicklinksheading'] = 'Enter heading text for Quick Links';
$string['quicklinksheadingdesc'] = 'You may add heading text for Quick Links section.';
$string['quicklinksheadingvalue'] = 'Quick links';
$string['quicklinkscolumns'] = 'Enter number of columns for Quick Links section';
$string['quicklinkscolumnsdesc'] = 'You may add the required number of columns that the Quick Links section will have.';

$string['quicklinksrows'] = 'Enter number of rows for column ';
$string['quicklinksrowsdesc'] = 'You may add the required number of rows that column 1 in Quick Links section will have.';

$string['text'] = 'Enter Text'; 
$string['textdesc'] = 'Enter text for the link where the user will be redirected.';
$string['link'] = 'Enter Link';
$string['linkdesc'] = 'Enter the link address where the user will be redirected.';
$string['columnheading'] = 'Enter Column heading';
$string['columnheadingdesc'] = '';

/**************************success story section start********************************/
$string['successstorysection'] = 'Infographics section';
$string['successstorysectiondesc'] = '';

$string['successstoryhead'] = 'Infographics heading';
$string['successstoryheaddesc'] = 'You may add a heading for the infographics section.';
$string['successstoryheadvalue'] = 'Why Shiksha Theme';

$string['subsuccessstoryhead'] = 'Infographics sub heading';
$string['subsuccessstoryheaddesc'] = 'You may add a sub-heading for the infographics section.';
$string['subsuccessstoryheadvalue'] = 'OUR SUCCESS STORIES';

$string['successstorytext1'] = ' Infographic column 1 title';
$string['successstorytextdesc1'] = 'You may add the title for the infographic column.';
$string['successstorytext1value'] = 'INTERNATIONAL <br/>Placements';
$string['successstorynumber1'] = 'Infographic column  1 statistics ';
$string['successstorynumberdesc1'] = 'You may insert the statistic value for the particular infographic title.';

$string['successstorytext2'] = 'Infographic column 2 title';
$string['successstorytextdesc2'] = 'You may add the title for the infographic column.';
$string['successstorytext2value'] = 'RESEARCH <br/>Scholarships';
$string['successstorynumber2'] = 'Infographic column 2 statistics ';
$string['successstorynumberdesc2'] = 'You may insert the statistic value for the particular infographic title.';

$string['successstorytext3'] = 'Infographic column 3 title';
$string['successstorytextdesc3'] = 'You may add the title for the infographic column.';
$string['successstorytext3value'] = 'INCREMENT <br/>in Enrollment';
$string['successstorynumber3'] = 'Infographic column 3 statistics ';
$string['successstorynumberdesc3'] = 'You may insert the statistic value for the particular infographic title.';

$string['successstorytext4'] = 'Infographic column 4 title';
$string['successstorytextdesc4'] = 'You may add the title for the infographic column.';
$string['successstorytext4value'] = 'SUCCESSFUL <br/>Graduates';
$string['successstorynumber4'] = 'Infographic column 4 statistics ';
$string['successstorynumberdesc4'] = 'You may insert the statistic value for the particular infographic title.';
/****************************success story section end********************************/

/****************************featured course section start**************************/
$string['featuredcoursesection'] = 'Featured course section';
$string['featuredcoursesectiondesc'] = '';

$string['featuredcourseheading'] = 'Featured course heading';
$string['featuredcourseheadingdesc'] = 'You may add a heading for the Featured course section.';
$string['featuredcourseheadingvalue'] = 'Our Featured Courses - ';

$string['featuredcoursesubheading'] = 'Featured course sub-heading';
$string['featuredcoursesubheadingdesc'] = 'You may add a sub-heading for the Featured course section.';
$string['featuredcoursesubheadingvalue'] = 'Best courses for students accross the worldView All Courses';

$string['featuredcourse1'] = 'Featured course 1';
$string['featuredcoursedesc1'] = 'You may choose a featured course 1';

$string['featuredcourse2'] = 'Featured course 2';
$string['featuredcoursedesc2'] = 'You may choose a featured course 2';

$string['featuredcourse3'] = 'Featured course 3';
$string['featuredcoursedesc3'] = 'You may choose a featured course 3';

$string['featuredcourse4'] = 'Featured course 4';
$string['featuredcoursedesc4'] = 'You may choose a featured course 4';

/****************************featured course section end************************/

/****************************Course and programme start*************************/
$string['courseprogrammesection'] = 'Featured content';
$string['courseprogrammesectiondesc'] = '';

$string['courseprogrammeheading'] = 'Heading for featured content ';
$string['courseprogrammeheadingdesc'] = 'You may enter the heading text.';
$string['courseprogrammeheadingvalue'] = 'COURSES AND PROGRAMS';

$string['courseprogrammesubheading'] = 'Sub heading for featured content';
$string['courseprogrammesubheadingdesc'] = 'You may enter the sub heading text.';
$string['courseprogrammesubheadingvalue'] = 'WHAT WE OFFER';

$string['courseprogrammecontent'] = 'Brief Description for Featured content';
$string['courseprogrammecontentdesc'] = 'You may enter a description for the featured content.';
$string['courseprogrammecontentvalue'] = 'We offer a wide ranges of courses stretching from Engineering and medicine to Applied arts and social sciences. Our institute boasts of a brilliant faculty combined with excellent infrastructure to contribute towards your all-round development.'; 

$string['courseprogrammecount'] = 'Number of featured content columns';
$string['courseprogrammecountdesc'] = 'Maximum - 4';

$string['courseprogrammetext'] = 'Featured content column text';
$string['courseprogrammetextdesc'] = 'You may enter the text for featured content.';
$string['courseprogrammetextvalue'] = 'UNDERGRAD MAJORS';

$string['courseprogrammesubtext'] = 'Featured content column short description';
$string['courseprogrammesubtextdesc'] = 'You may enter a short description for the above text.';
$string['courseprogrammesubtextvalue'] = 'NULLA EGET GRAVIDA MIS';
$string['courseprogrammeimage'] = 'Featured content column image';
$string['courseprogrammeimagedesc'] = 'You may Upload the image for column featured content.';

$string['courseprogrammelink'] = 'Featured content column link';
$string['courseprogrammelinkdesc'] = 'You may enter the link address where the user will be redirected.';
/****************************Course and programme end**************************/
/***************************parallax section start*****************************/
$string['parallaxsection'] = 'Parallax section';
$string['parallaxsectiondesc'] = '';

$string['parallaxsectionbackgroundimage'] = 'Parallax background image';
$string['parallaxsectionbackgroundimagedesc'] = 'You may Upload a background image for the parallax section.';

$string['left'] = 'Left';
$string['right'] = 'Right';

$string['parallaxsectioncontentposition'] = 'Select accordion position';
$string['parallaxsectioncontentpositiondesc'] = 'You may choose the accordion section to be either on the left or right side of the parallax section.';

$string['parallaxsectionaccodioncount'] = 'No of Accordion';
$string['parallaxsectionaccodioncountdesc'] = 'You may enter the no of accordions you want.(Maximum - 5)';

$string['parallaxsectioncontent'] = 'Parallax section content';
$string['parallaxsectioncontentdesc'] = 'You may add content for the parallax section.';
$string['parallaxsectioncontentvalue'] = '<h2>About Shiksha </h2><h3>Founded by Dualcube</h3><p> We have grown from a small college to a full fledged shiksha that has attained “a centre of excellence” status. Home to some of the brightest minds of this country, we strive for perfection in the field of education. We have an excellent record at placements and some of our alumni have had distinguished careers in their respective domains. Constantly being ranked in the top three of the best colleges in the country, it is a feat that has not been replicated by any other.  We have received the president’s award for best educational society and been nominated for the best shiksha of the year award 2015.</p>';

$string['parallaxaccordionheading'] = 'Add  Accordion heading';
$string['parallaxaccordionheadingdesc'] = 'You may add the heading for the accordion.';
$string['parallaxaccordionheadingvalue'] = 'BOOK BANK';

$string['parallaxaccordioncontent'] = 'parallax accordion content';
$string['parallaxaccordioncontentdesc'] = 'You may add the content for the accordion.';
$string['parallaxaccordioncontentvalue'] = '<h3>Sed eu tellus lobortis, facilisis nisl quis, dictum leo. </h3>
<p>Nam nec egestas risus. Donec egestas enim nec blandit egestas. Suspendisse felis augue, faucibus at hendrerit eu, consequat eu ante. Duis et fermentum urna. Nam et bibendum arcu. Vivamus eu lectus ... </p><p><a href="javascript:void(0);">Know More</a></p>';
$string['parallaxaccordionicon'] = 'Upload parallax accordion icon';
$string['parallaxaccordionicondesc'] = 'You may upload an icon for the Accordion.';
/***************************parallax section end*****************************/
/****************************map section start*********************************/
$string['map'] = 'Map section';
$string['mapdesc'] = '';

$string['place'] = 'Enter your place (city/town/village)';
$string['placedesc'] = '';
$string['placevalue'] = 'Kolkata';


$string['country'] = 'Enter your country';
$string['countrydesc'] = '';
$string['countryvalue'] = 'India';
/****************************map section end**********************************/
/****************************contact email start******************************/
$string['contactemailsection'] = 'Contact section';
$string['contactemailsectiondesc'] = '';
$string['emailcontact'] = 'Email';
$string['emailcontactdesc'] = '';
$string['emailsubject'] = 'Email subject';
$string['emailsubjectdesc'] = 'You may add the subject for your email.';

$string['emailcontactheading'] = 'Contact section heading';
$string['emailcontactheadingdesc'] = 'You may add a heading for the contact section.';
$string['emailcontactheadingvalue'] = 'CONTACT US';

$string['emailcontactsubheading'] = 'Contact section Sub-heading';
$string['emailcontactsubheadingdesc'] = 'You may add a Sub-heading for the contact section.';
$string['emailcontactsubheadingvalue'] = 'Maecenas sed est sit amet erat eleifend pellentesque id quis purus.';

$string['columnheadingvalue'] = 'Shiksha:';
$string['textvalue'] = 'Quick Link';
