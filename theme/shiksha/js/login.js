jQuery(document).ready(function() {
	var varusername = $("#logininfo").val();
	var varpassword = $("#logininfo").attr('name');
	var gotohome = $("#gotohome").val();
	var placeholderusername = {
	    'username' : varusername
	};
	var placeholderspassword = {
	    'password' : varpassword
	};
	$('input[type=text]').each(function(i,el) {
		if (!el.value || el.value == '') {
	        el.placeholder = placeholderusername[el.id];
	    }   
	});
	$('input[type=password]').each(function(i,el) {
		if (!el.value || el.value == '') {
	        el.placeholder = placeholderspassword[el.id];
	    }   
	});
	$('.signupform').find('input[type=submit]').addClass('signup');
	$('.loginbox').removeClass('twocolumns').addClass('onecolumns');
	$('.nav-collapse').removeClass('collapse');
	$(".btn-navbar").on("click",function() {
		$(this).toggleClass("active-drop");
		$('.nav-collapse').toggleClass('in collapse').removeClass('collapse');
		$('.usermenu-show').removeClass('usermenu-show');
	});
	$('.dropdown').on("click",function() {
		$(this).toggleClass('open');
	});
	
	
});

