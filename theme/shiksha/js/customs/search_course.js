var search_course_config = function () {

    function init() {
        configCtrls();
    }

    function configCtrls() {

        $('#search_course').val('');

        $('#search_course').focus(function () {

            $(this).animate({
                'height': '30px',
                'width': '50%'
            }, 200);

            $(this).css({
                'font-size': '18px',
                'transition-delay': '3s',
                '-webkit-transition-delay':'3s',
            });

        });

        $('#search_course').focusout(function () {

            $(this).animate({
                'height': '20px',
                'width': '206px'
            }, 200);

            $(this).css({
                'font-size':'14px'
            });
        });

        $('#search_course').keyup(function () {
            var value = $(this).val().toLowerCase();
            filter(value);
        });

    }

    function filter(value) {

        $('.coursebox').each(function (i, v) {
            var text_to_search = $(v).children('.course_title').find('.title').children().html();

            if(text_to_search.toLowerCase().indexOf(value) === -1){
                $(this).hide();
            }else{
                $(this).show();
            }
        });

    }

    return {
        _init_: function () {
            init();
        }
    }

};
var search_course = new search_course_config();

$(document).ready(function () {
    search_course._init_();
});