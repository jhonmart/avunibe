var myAdmin = function () {

    function init() {
        configCtrls();
    }

    function configCtrls() {
        $('#show_search_course').click(function (e) {
            $('#modal_search_course, .background_for_open_modal_courses').show();
            e.preventDefault();
        });

        $('#show_search_participant').click(function (e) {
            $('#modal_search_participant, .background_for_open_modal_part').show();
            e.preventDefault();
        });

        $('.close_modal').click(function (e) {
            $('.search_read').val('');
            $('.search_go').click();
            $('.background_for_open_modal_courses, .background_for_open_modal_part').hide();
            $('#modal_search_participant, #modal_search_course').hide(200);
            e.preventDefault();
        });

        $('.langmenu').click(function(e){
            $(this).children('.dropdown-menu').toggleClass('display');
            e.preventDefault();
        });

        $('.langmenu .dropdown-menu li a').click(function(e){
            window.location = $(this).attr('href');
            e.preventDefault();
        });
    }

    return{
        _init_: function () {
            init();
        }
    }
};

$(document).ready(function () {
   var my = new myAdmin();
   my._init_();
});