var search_run = function () {

    function init() {
        configCtrls();
    }

    function configCtrls() {

        $('.search_read').val('');

        $('.search_go').click(function () {
            var value = $(this).parent().find('.search_read').val().toLowerCase();
            filter(value);
        });

        $('.search_read').keypress(function(e) {
            if(e.which == 13) {
                var value = $(this).val().toLowerCase();
                filter(value);
            }
        });

        $('.search_read').change(function () {
            if($(this).val() === ''){
                $('.search_go').click();
            }
        });

    }

    function filter(value) {

        $('.item_row_to_search').each(function (i, v) {

            var text_to_search = $(v).children().html();

            if(text_to_search.toLowerCase().indexOf(value) === -1){
                text_to_search = $(v).children().next().html();
                if(text_to_search.toLowerCase().indexOf(value) === -1){
                    $(this).hide();
                }else{
                    $(this).show();
                }

            }else{
                $(this).show();
            }
        });
    }

    return {
        _init_: function () {
            init();
        }
    }

};
var search = new search_run();

$(document).ready(function () {
    search._init_();
});