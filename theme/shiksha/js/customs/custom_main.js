var custom_main = function () {

    function init() {
        configChosen();
    }

    function getChosen(id) {
        $(id).chosen();
    }

    function configChosen(){
        var list_chosen = ['#menuuser', '#menudate', '#menumodid', '#menuinstanceid', '#id_category', '#id_country'];

        for(item in list_chosen){
            getChosen(list_chosen[item]);
        }
    }

    // function loadingConfig(action) {
    //     if(action){
    //         var elem_loading = '<img src="../theme/shiksha/css/img/loading.gif"/>';
    //         // elem_loading.src = '../theme/shiksha/css/img/loading.gif';
    //         console.log('elem_loading', elem_loading);
    //         $('.loading').html(elem_loading);
    //     }else{
    //         $('.loading').html('');
    //     }
    // }

    return {
        _init_: function () {
            init();
        }
    }

};

