'use strict';
var courseConfig = function () {

    var controlNoReset = [];
    var count = 0;

    function configCtrls() {
        $('#reset_prev_all_courses').click(function (e) {
            requestCourse(count);
            e.preventDefault();
        });
    }

    function requestCourse(idNoReset){
        resetAllCourse(idNoReset);
    }

    function resetAllCourse(idNoReset) {

        $.ajax({
            method: 'POST',
            url: '../administrar/reset_bulk_3.php',
            data: {'no_reset': idNoReset},
            dataType: 'json',
            beforeSend: function () {
                $('#result_reset_all_course').html('Reiniciando cursos...');
            },
            success: function (data, status, jqXHR) {

                // console.log('controlNoReset.indexOf(data.newCourseId) === -1', controlNoReset.indexOf(data.newCourseId) === -1);

                // requestCourse(data.newCourseId);
                console.log('data.newCourseId', data.newCourseId);
                console.log('controlNoReset', controlNoReset);
                console.log('count'+count);

                requestCourse(count);

                count++;

                // if(controlNoReset.indexOf(parseInt(data.newCourseId)) === -1){
                //
                //     controlNoReset.push(count);
                //
                //     requestCourse(parseInt(data.newCourseId));
                //
                //     $('#result_reset_all_course').append('<p>' + data.data + '</p>');
                //     // console.log('data.newCourseId', data.newCourseId);
                // }

            },
            error: function(err){
                $('#result_reset_all_course').append(err);
            },
            complete: function () {
                $('#result_reset_all_course').append('Reinicio completado!');
            }
        });
    }

    return{
        getCtrls: function () {
            configCtrls();
        }
    }

};
var course = new courseConfig();
