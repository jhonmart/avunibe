'use strict';
var course_list = function () {

    function init() {
        $('.coursebox').prepend('<div class="select_course_dest"><i class="fa fa-star-o" aria-hidden="true"></i></div>');
        loadDest();
        verifySelectedCourses();
        configCtrls();
        window.onload = function () {
            configCourseOverview();
        };
    }

    function configCtrls(){

        // AGREGA UN NUEVO CURSO AL BLOQUE DE DESTACADOS
        $('.select_course_dest').click(function (e) {

            var saveIdParent = $(this).parent().attr('id');

            addCourse(saveIdParent);
            e.preventDefault();
        });

        // ELIMINACION DE UN ITEM INDIVIDUAL YA AGREGADO
        $('.close_dest_course').click(function (e) {

            var data_id = $(this).parent().data('id');
            $(this).parent().remove();
            delDest(data_id);

            e.preventDefault();
        });

        // ELIMINA TODOS LOS ITEMS EN BLOQUE DE DESTACADOS
        $('#btn_delete_all_dest').click(function (e) {
            $('.item_course_selected').remove();
            deleteAllCourse();
            e.preventDefault();
        });

    }

    function addCourse(id_course){

        $.ajax({
            method: 'POST',
            url: '../customs/my/requests/feature_course_request.php',
            cache: false,
            dataType: 'json',
            data: {'add': id_course},
            success: function (data) {
                loadDest();
            }
        });
    }

    // VERIFICA LOS ITEMS SELECCIONADOS COMO DESTACADOS Y LOS MUESTRA EN BLOQUE DE DESTACADOS
    function loadDest() {

        var featured = [];

        $.ajax({
            method: 'GET',
            url: '../customs/my/requests/feature_course_request.php',
            dataType: 'json',
            success: function (data) {

                if(data.length > 0){
                    for(var i in data){

                        var html = '<div class="item_course_selected" data-id=course-'+ data[i].course.id +'>' +
                            '<i class="fa fa-times-circle close_dest_course" aria-hidden="true"></i>' +
                            '<a href="../course/view.php?id='+data[i].course.id+'">'+data[i].course.fullname+'</a>' +
                            '</div>';

                        featured.push(html);
                    }

                    $('#list_selected_courses').html(featured);

                    configCourseOverview();
                    verifySelectedCourses();
                    removeAllEvents();
                    configCtrls();
                }else{
                    $('.item_course_selected').remove();
                    configCourseOverview();
                    verifySelectedCourses();
                    removeAllEvents();
                    configCtrls();
                }
            }
        });

    }

    // ELIMINA ITEM DEL LISTADO GUARDADO DE CURSOS DESTACADOS
    function delDest(id_course){

        $.ajax({
            method: 'POST',
            url: '../customs/my/requests/feature_course_request.php',
            dataType: 'json',
            data: {'delete': id_course}
        });

        configCourseOverview();
        verifySelectedCourses();
    }

    function deleteAllCourse() {
        $.ajax({
            method: 'POST',
            url: '../customs/my/requests/feature_course_request.php',
            dataType: 'json',
            data: {'delete_all': true}
        });
        configCourseOverview();
        verifySelectedCourses();
    }

    // CONTROL DEL COMPORTAMIENTO DE LOS CURSOS EN OVERVIEW AL AGREGAR O ELIMINAR UN CURSO DEL LISTADO DE DESTACADOS
    function configCourseOverview() {
        var id_course_selected = [];

        $('.item_course_selected').each(function (i, v) {
            id_course_selected.push($(v).attr('data-id'));
        });

        $('.coursebox').each(function (i, v) {
            if(id_course_selected.indexOf($(v).attr('id')) !== -1){
                $(v).find('.select_course_dest').html('<i class="fa fa-star" aria-hidden="true"></i>');
            }else{
                $(v).find('.select_course_dest').html('<i class="fa fa-star-o" aria-hidden="true"></i>');
            }
        });
    }

    // VERIFICA SI EXISTEN CURSOS SELECCIONADOS COMO DESTACADOS
    function verifySelectedCourses(){
        if($('.item_course_selected').length === 0){
            $('#btn_delete_all_dest').hide();
        }else{
            $('#btn_delete_all_dest').show();
        }
    }

    function removeAllEvents() {
        removeEvents('#btn_delete_all_dest');
        removeEvents('.close_dest_course');
        removeEvents('.select_course_dest');
        removeEvents('.star_checked_dest');
    }

    function removeEvents(elem) {
        $(elem).off();
    }

    return {
        _init_: function () {
            init();
        }
    }
};
var selected_course = new course_list();
