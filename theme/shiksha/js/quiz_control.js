// JavaScript Document
var CtrlQuiz = function () {

    function initConfig(){

        timeQuizConfig();

        document.addEventListener("fullscreenchange", function () {
            if(!getfullscreenelement()){
                document.getElementsByClassName('modal_quiz_mode_security').style.display = 'block';
            }else{
                document.getElementById('modal_quiz_mode_security').style.display = 'none';
            }
        }, false);

        document.addEventListener("mozfullscreenchange", function () {
            if(!getfullscreenelement()){
                document.getElementsByClassName('modal_quiz_mode_security').style.display = 'block';
            }else{
                document.getElementById('modal_quiz_mode_security').style.display = 'none';
            }
        }, false);

        document.addEventListener("webkitfullscreenchange", function () {
            if(!getfullscreenelement()){
                document.getElementById('modal_quiz_mode_security').style.display = 'block';
            }else{
                document.getElementById('modal_quiz_mode_security').style.display = 'none';
            }
        }, false);

        document.addEventListener("msfullscreenchange", function () {
            if(!getfullscreenelement()){
                document.getElementById('modal_quiz_mode_security').style.display = 'block';

            }else{
                document.getElementById('modal_quiz_mode_security').style.display = 'none';
            }
        }, false);


        window.addEventListener("keydown", function(e){
            if (e.keyCode === 122){
                e.preventDefault();
            }
        }, false);

    }

    function actionConfig(action){
        if(!action){
            is_confirm = confirm("Al cancelar esta operación perderá este intento.");
            if(is_confirm === true){
                history.go(-1);
            }
        }
        else if(action){
            toggleFullScreen();
        }
    }

    function toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        }
    }
    // Retorna valores para validar si el navegador está en fullscreen
    function getfullscreenelement(){
        return document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
    }

    // CONTROLADOR PARA EL TIEMPO RESTANTE
    function timeQuizConfig() {

        window.onload = function () {
            var time = document.getElementById('quiz-time-left');

            var observeDOM = (function(){
                var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
                    eventListenerSupported = window.addEventListener;

                return function(obj, callback){
                    if( MutationObserver ){

                        var obs = new MutationObserver(function(mutations, observer){
                            if( mutations[0].addedNodes.length || mutations[0].removedNodes.length )
                                callback();
                        });

                        obs.observe(obj, {childList:true, subtree:true});
                    }
                    else if( eventListenerSupported ){
                        obj.addEventListener('DOMNodeInserted', callback, false);
                        obj.addEventListener('DOMNodeRemoved', callback, false);
                    }
                };
            })();

            // Observa un elemento DOM específico:
            observeDOM(time ,function(){
                if(time.innerHTML < '0:05:00'){
                    time.style.color = '#de0808';
                }else{
                    time.style.color = '#1db723';
                }
            });
        };

    }

    return {
        action: function(action){
            actionConfig(action);
        },
        _init_: function () {
            initConfig();
        }
    }

};
var quiz = new CtrlQuiz();
quiz._init_();
