$(document).ready(function ($) {
	//$("#msgresponse").css('display', 'none');
	var carouseldatainterval = $('.carousel').attr('data-interval');
    $('.bxslider').bxSlider({
		minSlides: 2,
		maxSlides: 7,
		slideWidth: 150,
		startSlide: 1,
		slideMargin: 0,
		pager: false

	});
	if (carouseldatainterval != 0) {
		$('.carousel').carousel({
		  interval: carouseldatainterval,
		})
	} else {
		$('.carousel').carousel('pause')
	}
	var chartcolor = $(".color").val();
 	var wowchart = new WOW(
	  {
	    boxClass:     'chart',      // animated element css class (default is wow)
	    animateClass: 'animated', // animation css class (default is animated)
	    offset:       0,          // distance to the element when triggering the animation (default is 0)
	    mobile:       true,       // trigger animations on mobile devices (default is true)
	    live:         true,       // act on asynchronously loaded content (default is true)
	    callback:     function(box) {
	    	$('.chart').easyPieChart({
				easing: '',
				barColor:chartcolor,
				trackColor:"#f1f1f1",
				scaleColor:"",
				scaleLength:9,
				lineCap:"round",
				lineWidth:9,
				size:200,
				rotate:0,
				onStep: function(from, to, percent) {
					$(this.el).find('.percent').text(Math.round(percent));
				}
			});
			var chart = window.chart = $('.chart').data('easyPieChart');
	      // the callback is fired every time an animation is started
	      // the argument that is passed in is the DOM node being animated
	    },
	    scrollContainer: null // optional scroll container selector, otherwise use window
	  }
	);
	wowchart.init();
	$(function() {
    	$( ".accordion" ).accordion();
    });
    snav();
	$( window ).resize(function() {
		snav();
	});
	function snav() {
		var winw = $(window).width();
		if(winw > 1024 ) {
			$(".navbar-static-top").sticky({topSpacing:0});
		}	
	}
	$('.dropdown').on("click",function() {
		$(this).toggleClass('open');
	});
	if ($("html").attr("dir") == "rtl") {
		$(".landing-page").addClass("dir-rtl");
		$(".img-slider").attr("dir", "ltr" );
		$(".bx-prev, .bx-next").html("");
	}
	
	
	$(".send").click(function(e){
		var url = $('.hiddenform').val();
		var name = $('.msgname').val();
		var email = $('.home-email-pad').val();
		var msgsent = $('.msgsent').val();
		var msg = $('.msg').val();
		var emptynameemail = $('.emptynameemail').val();
		if( name != "" && email != "") {
		    $.ajax({url: url, 
			    	data : { name : name, email : email, msg : msg},
			    	type: 'post',
			    	success: function(result){
			    		$("#msgresponse").css('display', 'block');
			        	$("#msgresponse").html(msgsent);
			        	$('.msgname').val("");
			        	$('.home-email-pad').val("");
			        	$('.msg').val("");
			    	}
			});
		} else {
			$("#msgresponse").css('display', 'block');
			$("#msgresponse").html(emptynameemail);
		}
	});
});