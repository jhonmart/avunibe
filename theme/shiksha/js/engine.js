$(document).ready(function ($) {
	$('.usermenu').on("click",function() {
		$('.opendropdown').toggleClass('open');
	});
	$('.nav-collapse').removeClass('collapse');
	$(".btn-navbar").on("click",function() {
		$(this).toggleClass("active-drop");
		$('.nav-collapse').toggleClass('in collapse').removeClass('collapse');
		$('.usermenu-show').removeClass('usermenu-show');
	});
	
	$('.dropdown').on("click",function() {
		$(this).toggleClass('open');
	});
	snav();
	$( window ).resize(function() {
		snav();
	});
	function snav() {
		var winw = $(window).width();
		if(winw > 1024 ) {
			$(".navbar-static-top").sticky({topSpacing:0});
		}	
	}
});