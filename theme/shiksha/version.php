<?php
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2015102802;
$plugin->release   = '3.0.3';
$plugin->requires  = 2014051200;
$plugin->component = 'theme_shiksha';
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2014051200,
);
