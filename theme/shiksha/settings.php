<?php
defined('MOODLE_INTERNAL') || die;
$settings = null;
if (is_siteadmin()) {
    $ADMIN->add('themes', new admin_category('theme_shiksha', 'Shiksha'));
    $temp = new admin_settingpage('theme_shiksha_general',  get_string('generalsettings', 'theme_shiksha'));

    $name = 'theme_shiksha/logoorsitename';
    $title = get_string('logoorsitename', 'theme_shiksha');
    $description = get_string('logoorsitenamedesc', 'theme_shiksha');
    $default = 'logo';
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        'logo' => get_string('onlylogo', 'theme_shiksha'),
        'sitename' => get_string('onlysitename', 'theme_shiksha'),
        'iconsitename' => get_string('iconsitename', 'theme_shiksha')
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    if (get_config('theme_shiksha', 'logoorsitename') === "logo") {
        // Logo file setting.
        $name = 'theme_shiksha/logo';
        $title = get_string('logo','theme_shiksha');
        $description = get_string('logodesc', 'theme_shiksha');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);
    } else if (get_config('theme_shiksha', 'logoorsitename') === "iconsitename") {
        // Logo file setting.
        $name = 'theme_shiksha/icon';
        $title = get_string('logoicon','theme_shiksha');
        $description = get_string('logoicondesc', 'theme_shiksha');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'icon');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);
    }
    // Logo background file setting.
    $name = 'theme_shiksha/logobackgroundimage';
    $title = get_string('logobackgroundimage','theme_shiksha');
    $description = get_string('logobackgroundimagedesc', 'theme_shiksha');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logobackgroundimage');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
     //custom favicon temp
    $name = 'theme_shiksha/faviconurl';
    $title = get_string('favicon', 'theme_shiksha');
    $description = get_string('favicondesc', 'theme_shiksha');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'faviconurl');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/socialicon1';
    $title = get_string('socialicon1', 'theme_shiksha');
    $description = get_string('socialicondesc1', 'theme_shiksha');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'socialicon1');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/socialiconlink1';
    $title = get_string('socialiconlink1', 'theme_shiksha');
    $description = get_string('socialiconlinkdesc1', 'theme_shiksha');
    $default = get_string('urlvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/socialicon2';
    $title = get_string('socialicon2', 'theme_shiksha');
    $description = get_string('socialicondesc2', 'theme_shiksha');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'socialicon2');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/socialiconlink2';
    $title = get_string('socialiconlink2', 'theme_shiksha');
    $description = get_string('socialiconlinkdesc2', 'theme_shiksha');
    $default = get_string('urlvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/socialicon3';
    $title = get_string('socialicon3', 'theme_shiksha');
    $description = get_string('socialicondesc3', 'theme_shiksha');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'socialicon3');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/socialiconlink3';
    $title = get_string('socialiconlink3', 'theme_shiksha');
    $description = get_string('socialiconlinkdesc3', 'theme_shiksha');
    $default = get_string('urlvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/socialicon4';
    $title = get_string('socialicon4', 'theme_shiksha');
    $description = get_string('socialicondesc4', 'theme_shiksha');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'socialicon4');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/socialiconlink4';
    $title = get_string('socialiconlink4', 'theme_shiksha');
    $description = get_string('socialiconlinkdesc4', 'theme_shiksha');
    $default = get_string('urlvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);


    // Custom CSS file.
    $name = 'theme_shiksha/customcss';
    $title = get_string('customcss', 'theme_shiksha');
    $description = get_string('customcssdesc', 'theme_shiksha');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);                            

    $temp->add(new admin_setting_heading('theme_shiksha_contactus', get_string('contactwithus', 'theme_shiksha'),
        format_text(get_string('contactwithusdesc', 'theme_shiksha'), FORMAT_MARKDOWN)));
    //contact with us

    $name = 'theme_shiksha/controlquicklinksection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

   	$name = 'theme_shiksha/footerlogo';
	$title = get_string('footerlogo', 'theme_shiksha');
	$description = get_string('footerlogodesc', 'theme_shiksha');
	$default = 'footerlogo';
	$setting = new admin_setting_configstoredfile($name, $title, $description, $default);
	$setting->set_updatedcallback('theme_reset_all_caches');
	$temp->add($setting);

	$name = 'theme_shiksha/someinformation';
    $title = get_string('someinformation', 'theme_shiksha');
    $description = get_string('someinformationdesc', 'theme_shiksha');
    $default = get_string('someinformationvalue', 'theme_shiksha');
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

	$name = 'theme_shiksha/address';
    $title = get_string('address', 'theme_shiksha');
    $description = get_string('addressdesc', 'theme_shiksha');
    $default = get_string('addressvalue', 'theme_shiksha');
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/phoneheading';
    $title = get_string('phoneheading', 'theme_shiksha');
    $description = get_string('phoneheadingdesc', 'theme_shiksha');
    $default = get_string('phoneheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/phone';
    $title = get_string('phone', 'theme_shiksha');
    $description = get_string('phonedesc', 'theme_shiksha');
    $default = get_string('phonevalue', 'theme_shiksha');
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/faxheading';
    $title = get_string('faxheading', 'theme_shiksha');
    $description = get_string('faxheadingdesc', 'theme_shiksha');
    $default = get_string('faxheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/fax';
    $title = get_string('fax', 'theme_shiksha');
    $description = get_string('faxdesc', 'theme_shiksha');
    $default = get_string('faxvalue', 'theme_shiksha');
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/emailheading';
    $title = get_string('emailheading', 'theme_shiksha');
    $description = get_string('emailheadingdesc', 'theme_shiksha');
    $default = get_string('emailheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/email';
    $title = get_string('email', 'theme_shiksha');
    $description = get_string('emaildesc', 'theme_shiksha');
    $default = get_string('emailvalue', 'theme_shiksha');
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /***********************Quick links*****************************/

    $name = 'theme_shiksha/quicklinksheading';
    $title = get_string('quicklinksheading', 'theme_shiksha');
    $description = get_string('quicklinksheadingdesc', 'theme_shiksha');
    $default = get_string('quicklinksheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/quicklinkscolumns';
    $title = get_string('quicklinkscolumns', 'theme_shiksha');
    $description = get_string('quicklinkscolumnsdesc', 'theme_shiksha');
    $setting = new admin_setting_configselect($name, $title, $description, 1,
    array(
            1 => get_string('one', 'theme_shiksha'),
            2 => get_string('two', 'theme_shiksha'),
            3 => get_string('three', 'theme_shiksha'),
        ));
    $temp->add($setting);

    for($quicklinkcols = 1; $quicklinkcols <= get_config('theme_shiksha', 'quicklinkscolumns'); $quicklinkcols = $quicklinkcols + 1) {
    	
        $name = 'theme_shiksha/quicklinksrows'.$quicklinkcols;
	    $title = get_string('quicklinksrows', 'theme_shiksha');
	    $description = get_string('quicklinksrowsdesc', 'theme_shiksha');
	    $setting = new admin_setting_configselect($name, $title, $description, 1,
	    array(
	            1 => get_string('one', 'theme_shiksha'),
	            2 => get_string('two', 'theme_shiksha'),
	            3 => get_string('three', 'theme_shiksha'),
	            4 => get_string('four', 'theme_shiksha'),
	            5 => get_string('five', 'theme_shiksha'),
	            6 => get_string('six', 'theme_shiksha'),
	            7 => get_string('seven', 'theme_shiksha'),
	        ));
	    $temp->add($setting);
        $name = 'theme_shiksha/columnheading'.$quicklinkcols;
        $title = get_string('columnheading', 'theme_shiksha');
        $description = get_string('columnheadingdesc', 'theme_shiksha');
        $default = '';
        $setting = new admin_setting_configtext($name, $title,  $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);
	    for($quicklinkrows = 1; $quicklinkrows <= get_config('theme_shiksha', 'quicklinksrows'.$quicklinkcols); $quicklinkrows = $quicklinkrows + 1) {
	    	$name = 'theme_shiksha/text'.$quicklinkcols.'_'.$quicklinkrows;
		    $title = get_string('text', 'theme_shiksha');
		    $description = get_string('textdesc', 'theme_shiksha');
		    $default = '';
		    $setting = new admin_setting_configtext($name, $title,  $description, $default);
		    $setting->set_updatedcallback('theme_reset_all_caches');
		    $temp->add($setting);

		    $name = 'theme_shiksha/link'.$quicklinkcols.'_'.$quicklinkrows;
		    $title = get_string('link', 'theme_shiksha');
		    $description = get_string('linkdesc', 'theme_shiksha');
		    $default = '';
		    $setting = new admin_setting_configtext($name, $title,  $description, $default);
		    $setting->set_updatedcallback('theme_reset_all_caches');
		    $temp->add($setting);
	    }
    }	
    // Footnote setting.
    $name = 'theme_shiksha/controlsmallfootersection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/footnote';
    $title = get_string('rightfootnote', 'theme_shiksha');
    $description = get_string('rightfootnotedesc', 'theme_shiksha');
    $default = get_string('rightfootnotevalue', 'theme_shiksha');
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    

    $name = 'theme_shiksha/leftfootnotesection1';
    $title = get_string('leftfootnotesection1', 'theme_shiksha');
    $description = get_string('leftfootnotedescsection1', 'theme_shiksha');
    $default = get_string('leftfootnotesection1value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/leftfootnotesectionlink1';
    $title = get_string('leftfootnotesectionlink1', 'theme_shiksha');
    $description = get_string('leftfootnotelinkdescsection1', 'theme_shiksha');
    $default = 'javascript:void(0);';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/leftfootnotesection2';
    $title = get_string('leftfootnotesection2', 'theme_shiksha');
    $description = get_string('leftfootnotedescsection2', 'theme_shiksha');
    $default = get_string('leftfootnotesection2value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/leftfootnotesectionlink2';
    $title = get_string('leftfootnotesectionlink2', 'theme_shiksha');
    $description = get_string('leftfootnotelinkdescsection2', 'theme_shiksha');
    $default = 'javascript:void(0);';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/leftfootnotesection3';
    $title = get_string('leftfootnotesection3', 'theme_shiksha');
    $description = get_string('leftfootnotedescsection3', 'theme_shiksha');
    $default = get_string('leftfootnotesection3value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/leftfootnotesectionlink3';
    $title = get_string('leftfootnotesectionlink3', 'theme_shiksha');
    $description = get_string('leftfootnotelinkdescsection3', 'theme_shiksha');
    $default = 'javascript:void(0);';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/leftfootnotesection4';
    $title = get_string('leftfootnotesection4', 'theme_shiksha');
    $description = get_string('leftfootnotedescsection4', 'theme_shiksha');
    $default = get_string('leftfootnotesection3value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/leftfootnotesectionlink4';
    $title = get_string('leftfootnotesectionlink4', 'theme_shiksha');
    $description = get_string('leftfootnotelinkdescsection4', 'theme_shiksha');
    $default = 'javascript:void(0);';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    

    $ADMIN->add('theme_shiksha', $temp);


    //frontpage temp
    $temp = new admin_settingpage('theme_shiksha_frontpage',  get_string('frontpagesettings', 'theme_shiksha'));
    $temp->add(new admin_setting_heading('theme_shiksha_upsection', get_string('frontpageimagecontent', 'theme_shiksha'),
        format_text(get_string('frontpageimagecontentdesc', 'theme_shiksha'), FORMAT_MARKDOWN)));

    $name = 'theme_shiksha/controlbannerorslidersection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageimagecontent';
    $title = get_string('frontpageimagecontentstyle', 'theme_shiksha');
    $description = get_string('frontpageimagecontentstyledesc', 'theme_shiksha');
    $setting = new admin_setting_configselect($name, $title, $description, 1,
    array(
            0 => get_string('staticcontent', 'theme_shiksha'),
            1 => get_string('slidercontent', 'theme_shiksha'),
        ));
    $temp->add($setting);
    if (get_config('theme_shiksha', 'frontpageimagecontent') === "0") {
        $name = 'theme_shiksha/addbannertext';
        $title = get_string('addtext', 'theme_shiksha');
        $description = get_string('addtextdesc', 'theme_shiksha');
        $default = get_string('addtextvalue', 'theme_shiksha');
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        $name = 'theme_shiksha/addlink';
        $title = get_string('addlink', 'theme_shiksha');
        $description = get_string('addlinkdesc', 'theme_shiksha');
        $default = 'javascript:void(0);';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        $name = 'theme_shiksha/addbuttontext';
        $title = get_string('addbuttontext', 'theme_shiksha');
        $description = get_string('addbuttontextdesc', 'theme_shiksha');
        $default = get_string('addbuttontextvalue', 'theme_shiksha');
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        $name = 'theme_shiksha/bannerimage';
        $title = get_string('bannerimage', 'theme_shiksha');
        $description = get_string('bannerimagedesc', 'theme_shiksha');
        $default = 'bannerimage';
        $setting = new admin_setting_configstoredfile($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        $name = 'theme_shiksha/bannereffect';
        $title = get_string('bannereffect', 'theme_shiksha');
        $description = get_string('slidereffectdesc', 'theme_shiksha');
        $setting = new admin_setting_configselect($name, $title, $description, 0,
        array(
                1 => get_string('repeat', 'theme_shiksha'),
                2 => get_string('static', 'theme_shiksha'),
                3 => get_string('parallax', 'theme_shiksha'),
                4 => get_string('cover', 'theme_shiksha'),
            ));
        $temp->add($setting);

        $name = 'theme_shiksha/staticbackgroundcolor';
        $title = get_string('staticbackgroundcolor', 'theme_shiksha');
        $description = get_string('staticbackgroundcolordesc', 'theme_shiksha');
        $default = '#FFFFFF';
        $previewconfig = null;
        $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        
    } else if (get_config('theme_shiksha', 'frontpageimagecontent') === "1"){
        $name = 'theme_shiksha/slideinterval';
        $title = get_string('slideinterval', 'theme_shiksha');
        $description = get_string('slideintervaldesc', 'theme_shiksha');
        $default = 5000;
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        $name = 'theme_shiksha/slidercount';
        $title = get_string('slidercount', 'theme_shiksha');
        $description = get_string('slidercountdesc', 'theme_shiksha');
        $setting = new admin_setting_configselect($name, $title, $description, 0,
        array(
                1 => get_string('one', 'theme_shiksha'),
                2 => get_string('two', 'theme_shiksha'),
                3 => get_string('three', 'theme_shiksha'),
                4 => get_string('four', 'theme_shiksha'),
                5 => get_string('five', 'theme_shiksha'),
            ));
        $temp->add($setting);

        for($slidecounts = 1; $slidecounts <= get_config('theme_shiksha', 'slidercount'); $slidecounts = $slidecounts + 1) {
            $name = 'theme_shiksha/slideimage'.$slidecounts;
            $title = get_string('slideimage', 'theme_shiksha');
            $description = get_string('slideimagedesc', 'theme_shiksha');
            $setting = new admin_setting_configstoredfile($name, $title, $description, 'slideimage'.$slidecounts);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);

            $name = 'theme_shiksha/slidertext'.$slidecounts;
            $title = get_string('slidertext', 'theme_shiksha');
            $description = get_string('slidertextdesc', 'theme_shiksha');
            $default = get_string('addtextvalue', 'theme_shiksha');
            $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);

            $name = 'theme_shiksha/sliderbuttontext'.$slidecounts;
            $title = get_string('sliderbuttontext', 'theme_shiksha');
            $description = get_string('sliderbuttontextdesc', 'theme_shiksha');
            $default = get_string('addbuttontextvalue', 'theme_shiksha');
            $setting = new admin_setting_configtext($name, $title, $description, $default);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);

            $name = 'theme_shiksha/sliderurl'.$slidecounts;
            $title = get_string('sliderurl', 'theme_shiksha');
            $description = get_string('sliderurldesc', 'theme_shiksha');
            $default = '#';
            $setting = new admin_setting_configtext($name, $title, $description, $default);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);

            $name = 'theme_shiksha/slidereffect'.$slidecounts;
            $title = get_string('slidereffect', 'theme_shiksha');
            $description = get_string('slidereffectdesc', 'theme_shiksha');
            $setting = new admin_setting_configselect($name, $title, $description, 0,
            array(
                    1 => get_string('repeat', 'theme_shiksha'),
                    2 => get_string('static', 'theme_shiksha'),
                    3 => get_string('parallax', 'theme_shiksha'),
                    4 => get_string('cover', 'theme_shiksha'),
                ));
            $temp->add($setting);

            $name = 'theme_shiksha/sliderbackgroundcolor'.$slidecounts;
            $title = get_string('sliderbackgroundcolor', 'theme_shiksha');
            $description = get_string('sliderbackgroundcolordesc', 'theme_shiksha');
            $default = '#FFFFFF';
            $previewconfig = null;
            $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);

        }
    }
    $temp->add(new admin_setting_heading('theme_shiksha_catslidersection', get_string('catslidersection', 'theme_shiksha'),
    format_text(get_string('catslidersectiondesc', 'theme_shiksha'), FORMAT_MARKDOWN)));

    $name = 'theme_shiksha/controlcatslidersection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/catslidercount';
    $title = get_string('catslidercount', 'theme_shiksha');
    $description = get_string('catslidercountdesc', 'theme_shiksha');
    $setting = new admin_setting_configselect($name, $title, $description, 0,
    array(
            1 => get_string('one', 'theme_shiksha'),
            2 => get_string('two', 'theme_shiksha'),
            3 => get_string('three', 'theme_shiksha'),
            4 => get_string('four', 'theme_shiksha'),
            5 => get_string('five', 'theme_shiksha'),
            6 => get_string('six', 'theme_shiksha'),
            7 => get_string('seven', 'theme_shiksha'),
            8 => get_string('eight', 'theme_shiksha'),
            9 => get_string('nine', 'theme_shiksha'),
            10 => get_string('ten', 'theme_shiksha'),
        ));
    $temp->add($setting);
    for($slidecounts = 1; $slidecounts <= get_config('theme_shiksha', 'catslidercount'); $slidecounts = $slidecounts + 1) {
            $name = 'theme_shiksha/catslideimage'.$slidecounts;
            $title = get_string('catslideimage', 'theme_shiksha');
            $description = get_string('catslideimagedesc', 'theme_shiksha');
            $setting = new admin_setting_configstoredfile($name, $title, $description, 'catslideimage'.$slidecounts);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);

            $name = 'theme_shiksha/catslidertext'.$slidecounts;
            $title = get_string('catslidertext', 'theme_shiksha');
            $description = get_string('catslidertextdesc', 'theme_shiksha');
            $default = get_string('catslidertextvalue', 'theme_shiksha');
            $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);

            $name = 'theme_shiksha/catsliderurl'.$slidecounts;
            $title = get_string('catsliderurl', 'theme_shiksha');
            $description = get_string('catsliderurldesc', 'theme_shiksha');
            $default = '#';
            $setting = new admin_setting_configtext($name, $title, $description, $default);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);
    }

    $temp->add(new admin_setting_heading('theme_shiksha_courseprogrammesection', get_string('courseprogrammesection', 'theme_shiksha'),
        format_text(get_string('courseprogrammesectiondesc', 'theme_shiksha'), FORMAT_MARKDOWN)));
    
    $name = 'theme_shiksha/controlfeaturedcontentsection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    $name = 'theme_shiksha/courseprogrammeheading';
    $title = get_string('courseprogrammeheading', 'theme_shiksha');
    $description = get_string('courseprogrammeheadingdesc', 'theme_shiksha');
    $default = get_string('courseprogrammeheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/courseprogrammesubheading';
    $title = get_string('courseprogrammesubheading', 'theme_shiksha');
    $description = get_string('courseprogrammesubheadingdesc', 'theme_shiksha');
    $default = get_string('courseprogrammesubheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/courseprogrammecontent';
    $title = get_string('courseprogrammecontent', 'theme_shiksha');
    $description = get_string('courseprogrammecontentdesc', 'theme_shiksha');
    $default = get_string('courseprogrammecontentvalue', 'theme_shiksha');
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/courseprogrammecount';
    $title = get_string('courseprogrammecount', 'theme_shiksha');
    $description = get_string('courseprogrammecountdesc', 'theme_shiksha');
    $setting = new admin_setting_configselect($name, $title, $description, 0,
    array(
            1 => get_string('one', 'theme_shiksha'),
            2 => get_string('two', 'theme_shiksha'),
            3 => get_string('three', 'theme_shiksha'),
            4 => get_string('four', 'theme_shiksha'),
        ));
    $temp->add($setting);

    for($courseprogrammecounts = 1; $courseprogrammecounts <= get_config('theme_shiksha', 'courseprogrammecount'); $courseprogrammecounts = $courseprogrammecounts + 1) {
        $name = 'theme_shiksha/courseprogrammetext'.$courseprogrammecounts;
        $title = get_string('courseprogrammetext', 'theme_shiksha');
        $description = get_string('courseprogrammetextdesc', 'theme_shiksha');
        $default = get_string('courseprogrammetextvalue', 'theme_shiksha');
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        $name = 'theme_shiksha/courseprogrammesubtext'.$courseprogrammecounts;
        $title = get_string('courseprogrammesubtext', 'theme_shiksha');
        $description = get_string('courseprogrammesubtextdesc', 'theme_shiksha');
        $default = get_string('courseprogrammesubtextvalue', 'theme_shiksha');
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        $name = 'theme_shiksha/courseprogrammelink'.$courseprogrammecounts;
        $title = get_string('courseprogrammelink', 'theme_shiksha');
        $description = get_string('courseprogrammelinkdesc', 'theme_shiksha');
        $default = '#';
        $setting = new admin_setting_configtext($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        $name = 'theme_shiksha/courseprogrammeimage'.$courseprogrammecounts;
        $title = get_string('courseprogrammeimage', 'theme_shiksha');
        $description = get_string('courseprogrammeimagedesc', 'theme_shiksha');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'courseprogrammeimage'.$courseprogrammecounts);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);
    }

    $temp->add(new admin_setting_heading('theme_shiksha_parallaxsection', get_string('parallaxsection', 'theme_shiksha'),
        format_text(get_string('parallaxsectiondesc', 'theme_shiksha'), FORMAT_MARKDOWN)));

    $name = 'theme_shiksha/controlparallaxsection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/parallaxsectionbackgroundimage';
    $title = get_string('parallaxsectionbackgroundimage', 'theme_shiksha');
    $description = get_string('parallaxsectionbackgroundimagedesc', 'theme_shiksha');
    $default = 'parallaxsectionbackgroundimage';
    $setting = new admin_setting_configstoredfile($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/parallaxsectioncontentposition';
    $title = get_string('parallaxsectioncontentposition', 'theme_shiksha');
    $description = get_string('parallaxsectioncontentpositiondesc', 'theme_shiksha');
    $setting = new admin_setting_configselect($name, $title, $description, 2,
    array(
            1 => get_string('left', 'theme_shiksha'),
            2 => get_string('right', 'theme_shiksha'),
        ));
    $temp->add($setting);

    $name = 'theme_shiksha/parallaxsectioncontent';
    $title = get_string('parallaxsectioncontent', 'theme_shiksha');
    $description = get_string('parallaxsectioncontentdesc', 'theme_shiksha');
    $default = get_string('parallaxsectioncontentvalue', 'theme_shiksha');
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/parallaxsectionaccodioncount';
    $title = get_string('parallaxsectionaccodioncount', 'theme_shiksha');
    $description = get_string('parallaxsectionaccodioncountdesc', 'theme_shiksha');
    $setting = new admin_setting_configselect($name, $title, $description, 1,
    array(
            1 => get_string('one', 'theme_shiksha'),
            2 => get_string('two', 'theme_shiksha'),
            3 => get_string('three', 'theme_shiksha'),
            4 => get_string('four', 'theme_shiksha'),
            5 => get_string('five', 'theme_shiksha'),
        ));
    $temp->add($setting);

    for($parallaxaccordioncounts = 1; $parallaxaccordioncounts <= get_config('theme_shiksha', 'parallaxsectionaccodioncount'); $parallaxaccordioncounts = $parallaxaccordioncounts + 1) {
            $name = 'theme_shiksha/parallaxaccordionheading'.$parallaxaccordioncounts;
            $title = get_string('parallaxaccordionheading', 'theme_shiksha');
            $description = get_string('parallaxaccordionheadingdesc', 'theme_shiksha');
            $default = get_string('parallaxaccordionheadingvalue', 'theme_shiksha');
            $setting = new admin_setting_configtext($name, $title, $description, $default);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);

            $name = 'theme_shiksha/parallaxaccordioncontent'.$parallaxaccordioncounts;
            $title = get_string('parallaxaccordioncontent', 'theme_shiksha');
            $description = get_string('parallaxaccordioncontentdesc', 'theme_shiksha');
            $default = get_string('parallaxaccordioncontentvalue', 'theme_shiksha');
            $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);

            $name = 'theme_shiksha/parallaxaccordionicon'.$parallaxaccordioncounts;
            $title = get_string('parallaxaccordionicon', 'theme_shiksha');
            $description = get_string('parallaxaccordionicondesc', 'theme_shiksha');
            $setting = new admin_setting_configstoredfile($name, $title, $description, 'parallaxaccordionicon'.$parallaxaccordioncounts);
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);
    }



    $temp->add(new admin_setting_heading('theme_shiksha_featuredcoursesection', get_string('featuredcoursesection', 'theme_shiksha'),
        format_text(get_string('featuredcoursesectiondesc', 'theme_shiksha'), FORMAT_MARKDOWN)));
    $name = 'theme_shiksha/controlfeaturedcoursesection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting); 

    $course = $DB->get_records_sql('SELECT c.* FROM {course} c where id != ?',array(1));
    $coursedetailsarray = array();
    foreach ($course as $key => $coursevalue) { 
        $coursedetailsarray[$coursevalue->id]= $coursevalue->fullname;
    }

    $name = 'theme_shiksha/featuredcourseheading';
    $title = get_string('featuredcourseheading', 'theme_shiksha');
    $description = get_string('featuredcourseheadingdesc', 'theme_shiksha');
    $default = get_string('featuredcourseheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/featuredcoursesubheading';
    $title = get_string('featuredcoursesubheading', 'theme_shiksha');
    $description = get_string('featuredcoursesubheadingdesc', 'theme_shiksha');
    $default = get_string('featuredcoursesubheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    if ($course) {
        $name = 'theme_shiksha/featuredcourse1';
        $title = get_string('featuredcourse1', 'theme_shiksha');
        $description = get_string('featuredcoursedesc1', 'theme_shiksha');
        $setting = new admin_setting_configselect($name, $title, $description, 0, $coursedetailsarray);
        $temp->add($setting);

        $name = 'theme_shiksha/featuredcourse2';
        $title = get_string('featuredcourse2', 'theme_shiksha');
        $description = get_string('featuredcoursedesc2', 'theme_shiksha');
        $setting = new admin_setting_configselect($name, $title, $description, 0, $coursedetailsarray);
        $temp->add($setting);

        $name = 'theme_shiksha/featuredcourse3';
        $title = get_string('featuredcourse3', 'theme_shiksha');
        $description = get_string('featuredcoursedesc3', 'theme_shiksha');
        $setting = new admin_setting_configselect($name, $title, $description, 0, $coursedetailsarray);
        $temp->add($setting);

        $name = 'theme_shiksha/featuredcourse4';
        $title = get_string('featuredcourse4', 'theme_shiksha');
        $description = get_string('featuredcoursedesc4', 'theme_shiksha');
        $setting = new admin_setting_configselect($name, $title, $description, 0, $coursedetailsarray);
        $temp->add($setting);
    }
    
    $temp->add(new admin_setting_heading('theme_shiksha_successstorysection', get_string('successstorysection', 'theme_shiksha'),
        format_text(get_string('successstorysectiondesc', 'theme_shiksha'), FORMAT_MARKDOWN)));
    
    $name = 'theme_shiksha/controlsuccessstorysection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting); 

    $name = 'theme_shiksha/successstoryheading';
    $title = get_string('successstoryhead', 'theme_shiksha');
    $description = get_string('successstoryheaddesc', 'theme_shiksha');
    $default = get_string('successstoryheadvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);


    $name = 'theme_shiksha/successstorysubheading';
    $title = get_string('subsuccessstoryhead', 'theme_shiksha');
    $description = get_string('subsuccessstoryheaddesc', 'theme_shiksha');
    $default = get_string('subsuccessstoryheadvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/successstorytext1';
    $title = get_string('successstorytext1', 'theme_shiksha');
    $description = get_string('successstorytextdesc1', 'theme_shiksha');
    $default = get_string('successstorytext1value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/successstorynumber1';
    $title = get_string('successstorynumber1', 'theme_shiksha');
    $description = get_string('successstorynumberdesc1', 'theme_shiksha');
    $default = '76';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/successstorytext2';
    $title = get_string('successstorytext2', 'theme_shiksha');
    $description = get_string('successstorytextdesc2', 'theme_shiksha');
    $default = get_string('successstorytext2value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/successstorynumber2';
    $title = get_string('successstorynumber2', 'theme_shiksha');
    $description = get_string('successstorynumberdesc2', 'theme_shiksha');
    $default = '56';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/successstorytext3';
    $title = get_string('successstorytext3', 'theme_shiksha');
    $description = get_string('successstorytextdesc3', 'theme_shiksha');
    $default = get_string('successstorytext3value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/successstorynumber3';
    $title = get_string('successstorynumber3', 'theme_shiksha');
    $description = get_string('successstorynumberdesc3', 'theme_shiksha');
    $default = '44';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/successstorytext4';
    $title = get_string('successstorytext4', 'theme_shiksha');
    $description = get_string('successstorytextdesc4', 'theme_shiksha');
    $default = get_string('successstorytext4value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/successstorynumber4';
    $title = get_string('successstorynumber4', 'theme_shiksha');
    $description = get_string('successstorynumberdesc4', 'theme_shiksha');
    $default = '90';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    
    

    $temp->add(new admin_setting_heading('theme_shiksha_mapsection', get_string('map', 'theme_shiksha'),
    format_text(get_string('mapdesc', 'theme_shiksha'), FORMAT_MARKDOWN)));

    $name = 'theme_shiksha/controlmapsection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/place';
    $title = get_string('place', 'theme_shiksha');
    $description = get_string('placedesc', 'theme_shiksha');
    $default = get_string('placevalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    $name = 'theme_shiksha/country';
    $title = get_string('country', 'theme_shiksha');
    $description = get_string('countrydesc', 'theme_shiksha');
    $default = get_string('countryvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $temp->add(new admin_setting_heading('theme_shiksha_contactsection', get_string('contactemailsection', 'theme_shiksha'),
    format_text(get_string('contactemailsectiondesc', 'theme_shiksha'), FORMAT_MARKDOWN)));

    $name = 'theme_shiksha/controlcontactsection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/emailcontactheading';
    $title = get_string('emailcontactheading', 'theme_shiksha');
    $description = get_string('emailcontactheadingdesc', 'theme_shiksha');
    $default = get_string('emailcontactheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/emailcontactsubheading';
    $title = get_string('emailcontactsubheading', 'theme_shiksha');
    $description = get_string('emailcontactsubheadingdesc', 'theme_shiksha');
    $default = get_string('emailcontactsubheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/emailcontact';
    $title = get_string('emailcontact', 'theme_shiksha');
    $description = get_string('emailcontactdesc', 'theme_shiksha');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/emailsubject';
    $title = get_string('emailsubject', 'theme_shiksha');
    $description = get_string('emailsubjectdesc', 'theme_shiksha');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);



    $temp->add(new admin_setting_heading('theme_shiksha_blocksection', get_string('frontpageblocks', 'theme_shiksha'),
        format_text(get_string('frontpageblocksdesc', 'theme_shiksha'), FORMAT_MARKDOWN)));

    $name = 'theme_shiksha/controlnewssection';
    $title = get_string('enable', 'theme_shiksha');
    $description = get_string('enabledesc', 'theme_shiksha');
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        0 => get_string('on', 'theme_shiksha'),
        1 => get_string('off', 'theme_shiksha'),
    ));
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageblockheading';
    $title = get_string('frontpageblockheading', 'theme_shiksha');
    $description = get_string('frontpageblockheadingdesc', 'theme_shiksha');
    $default = get_string('frontpageblockheadingvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageblock';
    $title = get_string('frontpageblock', 'theme_shiksha');
    $description = get_string('frontpageblockdesc', 'theme_shiksha');
    $default = get_string('frontpageblockvalue', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageblocklink';
    $title = get_string('frontpageblocklink', 'theme_shiksha');
    $description = get_string('frontpageblocklinkdesc', 'theme_shiksha');
    $default = 'javascript:void(0);';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /*block section 1*/
    $name = 'theme_shiksha/frontpageblocksection1';
    $title = get_string('frontpageblocksection1', 'theme_shiksha');
    $description = get_string('frontpageblocksectiondesc1', 'theme_shiksha');
    $default = get_string('frontpageblocksection1value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageblocklinksection1';
    $title = get_string('frontpageblocklinksection1', 'theme_shiksha');
    $description = get_string('frontpageblocklinksectiondesc1', 'theme_shiksha');
    $default = 'javascript:void(0);';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageblockdescriptionsection1';
    $title = get_string('frontpageblockdescriptionsection1', 'theme_shiksha');
    $description = get_string('frontpageblockdescriptionsectiondesc1', 'theme_shiksha');
    $default = get_string('frontpageblockdescriptionsection1value', 'theme_shiksha');
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /*block section 2*/
    $name = 'theme_shiksha/frontpageblocksection2';
    $title = get_string('frontpageblocksection2', 'theme_shiksha');
    $description = get_string('frontpageblocksectiondesc2', 'theme_shiksha');
    $default = get_string('frontpageblocksection2value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageblocklinksection2';
    $title = get_string('frontpageblocklinksection2', 'theme_shiksha');
    $description = get_string('frontpageblocklinksectiondesc2', 'theme_shiksha');
    $default = 'javascript:void(0);';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageblockdescriptionsection2';
    $title = get_string('frontpageblockdescriptionsection2', 'theme_shiksha');
    $description = get_string('frontpageblockdescriptionsectiondesc2', 'theme_shiksha');
    $default = get_string('frontpageblockdescriptionsection2value', 'theme_shiksha');
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /*block section 3*/
    $name = 'theme_shiksha/frontpageblocksection3';
    $title = get_string('frontpageblocksection3', 'theme_shiksha');
    $description = get_string('frontpageblocksectiondesc3', 'theme_shiksha');
    $default = get_string('frontpageblocksection3value', 'theme_shiksha');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageblocklinksection3';
    $title = get_string('frontpageblocklinksection3', 'theme_shiksha');
    $description = get_string('frontpageblocklinksectiondesc3', 'theme_shiksha');
    $default = 'javascript:void(0);';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_shiksha/frontpageblockdescriptionsection3';
    $title = get_string('frontpageblockdescriptionsection3', 'theme_shiksha');
    $description = get_string('frontpageblockdescriptionsectiondesc3', 'theme_shiksha');
    $default = get_string('frontpageblockdescriptionsection3value', 'theme_shiksha');
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);


    $ADMIN->add('theme_shiksha', $temp);

    
    /*color*/

    $temp = new admin_settingpage('theme_shiksha_colors',  get_string('colorsettings', 'theme_shiksha'));
    
    $name = 'theme_shiksha/color';
    $title = get_string('color', 'theme_shiksha');
    $description = get_string('colordesc', 'theme_shiksha');
    $default = '#e67e22';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_shiksha', $temp);
    /*font*/
    
    $temp = new admin_settingpage('theme_shiksha_font',  get_string('fontsettings', 'theme_shiksha'));
    $name = 'theme_shiksha/fontselect';
    $title = get_string('fontselect', 'theme_shiksha');
    $description = get_string('fontselectdesc', 'theme_shiksha');
    $default = 1;
    $choices = array(
        1 => get_string('fonttypestandard', 'theme_shiksha'),
        2 => get_string('fonttypecustom', 'theme_shiksha'),
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    // Heading font name
    $name = 'theme_shiksha/fontnameheading';
    $title = get_string('fontnameheading', 'theme_shiksha');
    $description = get_string('fontnameheadingdesc', 'theme_shiksha');
    $default = 'robotomedium';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Text font name

    $name = 'theme_shiksha/fontnamebody';
    $title = get_string('fontnamebody', 'theme_shiksha');
    $description = get_string('fontnamebodydesc', 'theme_shiksha');
    $default = 'robotoregular';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    if (get_config('theme_shiksha', 'fontselect') === "2") {

        if (floatval($CFG->version) >= 2014111005.01) {
            $woff2 = true;
        } else {
            $woff2 = false;
        }

        // This is the descriptor for the font files
        $name = 'theme_shiksha/fontfiles';
        $heading = get_string('fontfiles', 'theme_shiksha');
        $information = get_string('fontfilesdesc', 'theme_shiksha');
        $setting = new admin_setting_heading($name, $heading, $information);
        $temp->add($setting);

        // Heading Fonts.
        // TTF Font.
        $name = 'theme_shiksha/fontfilettfheading';
        $title = get_string('fontfilettfheading', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilettfheading');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // OTF Font.
        $name = 'theme_shiksha/fontfileotfheading';
        $title = get_string('fontfileotfheading', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfileotfheading');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // WOFF Font.
        $name = 'theme_shiksha/fontfilewoffheading';
        $title = get_string('fontfilewoffheading', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilewoffheading');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        if ($woff2) {
            // WOFF2 Font.
            $name = 'theme_shiksha/fontfilewofftwoheading';
            $title = get_string('fontfilewofftwoheading', 'theme_shiksha');
            $description = '';
            $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilewofftwoheading');
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);
        }

        // EOT Font.
        $name = 'theme_shiksha/fontfileeotheading';
        $title = get_string('fontfileeotheading', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfileweotheading');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // SVG Font.
        $name = 'theme_shiksha/fontfilesvgheading';
        $title = get_string('fontfilesvgheading', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilesvgheading');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // Body fonts.
        // TTF Font.
        $name = 'theme_shiksha/fontfilettfbody';
        $title = get_string('fontfilettfbody', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilettfbody');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // OTF Font.
        $name = 'theme_shiksha/fontfileotfbody';
        $title = get_string('fontfileotfbody', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfileotfbody');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // WOFF Font.
        $name = 'theme_shiksha/fontfilewoffbody';
        $title = get_string('fontfilewoffbody', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilewoffbody');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        if ($woff2) {
            // WOFF2 Font.
            $name = 'theme_shiksha/fontfilewofftwobody';
            $title = get_string('fontfilewofftwobody', 'theme_shiksha');
            $description = '';
            $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilewofftwobody');
            $setting->set_updatedcallback('theme_reset_all_caches');
            $temp->add($setting);
        }

        // EOT Font.
        $name = 'theme_shiksha/fontfileeotbody';
        $title = get_string('fontfileeotbody', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfileweotbody');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // SVG Font.
        $name = 'theme_shiksha/fontfilesvgbody';
        $title = get_string('fontfilesvgbody', 'theme_shiksha');
        $description = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'fontfilesvgbody');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);
    }
    $ADMIN->add('theme_shiksha', $temp);

    /* Analytics temp */
    $temp = new admin_settingpage('theme_shiksha_analytics', get_string('analytics', 'theme_shiksha'));
    $temp->add(new admin_setting_heading('theme_shiksha_analytics', get_string('analyticsheadingsub', 'theme_shiksha'),
        format_text(get_string('analyticsdesc', 'theme_shiksha'), FORMAT_MARKDOWN)));

    $name = 'theme_shiksha/analyticstrackingid';
    $title = get_string('analyticstrackingid', 'theme_shiksha');
    $description = get_string('analyticstrackingiddesc', 'theme_shiksha');
    $default = 'UA-XXXXXXXX-X';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_shiksha/analyticstrackingscript';
    $title = get_string('analyticstrackingscript', 'theme_shiksha');
    $description = get_string('analyticstrackingscriptdesc', 'theme_shiksha');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    $ADMIN->add('theme_shiksha', $temp);

}
