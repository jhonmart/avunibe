<?php
$THEME->name = 'shiksha';

/////////////////////////////////
// The only thing you need to change in this file when copying it to
// create a new theme is the name above. You also need to change the name
// in version.php and lang/en/theme_shiksha.php as well.
//////////////////////////////////
//
$THEME->doctype = 'html5';
$THEME->parents = array('bootstrapbase');
$THEME->sheets = array('custom');
$THEME->supportscssoptimisation = false;
$THEME->yuicssmodules = array();
$THEME->enable_dock = true;
$THEME->editor_sheets = array();
$THEME->blockrtlmanipulations = array(
    'side-pre' => 'side-pre',
    'side-post' => 'side-post'
);
$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->csspostprocess = 'theme_shiksha_process_css';
$THEME->layouts = array(
  // The site home page.
    'frontpage' => array(
        'file' => 'frontpage.php',
        'regions' => array(),
        'options' => array('nonavbar' => true),
    ),
    'login' => array(
        'file' => 'login.php',
        'regions' => array(),
        'options' => array('langmenu'=> true),
    ),
    'coursecategory' => array(
        'file' => 'columns3course.php',
        //'regions' => array('side-pre'), //AVUI Added
        //'defaultregion' => 'side-pre', //AVUI Added
        'defaultregion' => 'side-pre',
        'regions' => array('side-pre', 'side-post'),
        'options' => array('nonavbar' => false),
    ),
    'admin' => array(
        'file' => 'columns2.php', //UIAV - de 3 a 2 columnas
        'regions' => array('side-pre'), //AVUI Added
        'defaultregion' => 'side-pre', //AVUI Added
        //UIAV Comented //'defaultregion' => 'side-pre',
        //UIAV Comented //'regions' => array('side-pre', 'side-post'),
    ),
//   'mypublic' => array(
//         'file' => 'frontpage.php',
//         'regions' => array('side-pre'),
//         'defaultregion' => 'side-pre',
//     ),

);

