<?php

// Get the HTML for the settings bits.
$html = theme_shiksha_get_html_for_settings($OUTPUT, $PAGE);
GLOBAL $DB, $CFG, $OUTPUT, $USER;
$featuredcourses = array();
$featuredcourse = '';
array_push($featuredcourses, get_config('theme_shiksha', 'featuredcourse1'), get_config('theme_shiksha', 'featuredcourse2'), get_config('theme_shiksha', 'featuredcourse3'), get_config('theme_shiksha', 'featuredcourse4'));

$string = str_replace(array( '[', ']' ), '', json_encode($featuredcourses));//print_r($string);

$course = $DB->get_records_sql('SELECT course.*, course_categories.name as catname FROM {course} AS course INNER JOIN {course_categories} AS course_categories ON course.category = course_categories.id WHERE course.id IN ('.$string.')');

$coursedetailsarray = array();
foreach ($course as $key => $coursevalue) {   	
	$coursedetailsarray[$key]["courseid"] = $CFG->wwwroot."/course/view.php?id=".$coursevalue->id;
	$coursedetailsarray[$key]["coursename"] = $coursevalue->fullname;
	$courseteacher = $DB->get_record_sql('SELECT u.*
	FROM {course} c
	JOIN {context} ct ON c.id = ct.instanceid
	JOIN {role_assignments} ra ON ra.contextid = ct.id
	JOIN {user} u ON u.id = ra.userid
	JOIN {role} r ON r.id = ra.roleid Where c.id = ? and r.shortname = ?', array($coursevalue->id, 'editingteacher'));
	if(!empty($courseteacher)) {
		$coursedetailsarray[$key]["teacherid"] = $courseteacher->id;
		$coursedetailsarray[$key]["teachername"] = $courseteacher->firstname." ".$courseteacher->lastname;
	} else {
		$coursedetailsarray[$key]["teachername"] = "";
	}
	$coursedetailsarray[$key]["categoryname"] = $coursevalue->catname;
	$coursedetailsarray[$key]["summary"] = strlen($coursevalue->summary) > 117 ? substr($coursevalue->summary, 0, 117)."..." : $coursevalue->summary;
}
if ((get_config('theme_shiksha','controlmapsection') == 0) && (get_config('theme_shiksha','controlcontactsection') == 0)) {
	$span = 'span6';
} else if ((get_config('theme_shiksha','controlmapsection') == 1) && (get_config('theme_shiksha','controlcontactsection') == 0)) {
	$span = 'span12';
} else if ((get_config('theme_shiksha','controlmapsection') == 0) && (get_config('theme_shiksha','controlcontactsection') == 1)) {
	$span = 'span12';
}
/*------------------Logo section start-------------------*/
$haslogocheck = $PAGE->theme->setting_file_url('logo', 'logo');
if(!empty($haslogocheck)) {
	$haslogo = $PAGE->theme->setting_file_url('logo', 'logo');
} else {
	$haslogo = $CFG->wwwroot.'/theme/shiksha/css/img/logo.jpg';
}
$hasiconcheck = $PAGE->theme->setting_file_url('icon', 'icon');
if(!empty($hasiconcheck)) {
  $hasiconlogo = $PAGE->theme->setting_file_url('icon', 'icon');
} else {
  $hasiconlogo = $CFG->wwwroot.'/theme/shiksha/css/img/logo-icon.jpg';
}
$sliderinterval = get_config('theme_shiksha', 'slideinterval');
if (empty($sliderinterval)) {
	$sliderinterval = 0;
}
/*------------------Logo section end-------------------*/

$isregistration = $DB->get_record('config', array('name'=>'registerauth'));

$addtext = get_config('theme_shiksha', 'addbannertext');
$addlink = get_config('theme_shiksha', 'addlink');
$addbuttontext = get_config('theme_shiksha', 'addbuttontext');
$slideinterval = get_config('theme_shiksha', 'slideinterval');
$slideautoplay = get_config('theme_shiksha', 'sliderautoplay');

/*-------------------social contacts section start---------------------*/

$socialicon1link = get_config('theme_shiksha', 'socialiconlink1');
$socialicon2link = get_config('theme_shiksha', 'socialiconlink2');
$socialicon3link = get_config('theme_shiksha', 'socialiconlink3');
$socialicon4link = get_config('theme_shiksha', 'socialiconlink4');

$hassocialicon1check = $PAGE->theme->setting_file_url('socialicon1', 'socialicon1');
if(!empty($hassocialicon1check)) {
	$socialicon1 = $PAGE->theme->setting_file_url('socialicon1', 'socialicon1');
} else {
	$socialicon1 = $CFG->wwwroot."/theme/shiksha/css/img/icon-facebook.png";
}

$hassocialicon2check = $PAGE->theme->setting_file_url('socialicon2', 'socialicon2');
if(!empty($hassocialicon2check)) {
	$socialicon2 = $PAGE->theme->setting_file_url('socialicon2', 'socialicon2');
} else {
	$socialicon2 = $CFG->wwwroot."/theme/shiksha/css/img/icon-twitter.png";
}

$hassocialicon3check = $PAGE->theme->setting_file_url('socialicon3', 'socialicon3');
if(!empty($hassocialicon3check)) {
	$socialicon3 = $PAGE->theme->setting_file_url('socialicon3', 'socialicon3');
} else {
	$socialicon3 = $CFG->wwwroot."/theme/shiksha/css/img/icon-linkedin.png";
}

$hassocialicon4check = $PAGE->theme->setting_file_url('socialicon4', 'socialicon4');
if(!empty($hassocialicon4check)) {
	$socialicon4 = $PAGE->theme->setting_file_url('socialicon4', 'socialicon4');
} else {
	$socialicon4 = $CFG->wwwroot."/theme/shiksha/css/img/icon-instagram.png";
}

/*-------------------social contacts section end---------------------*/
/*-------------------img-slider section start------------------------*/
$catslideimagearray = array();

for($slidecounts = 1; $slidecounts <= get_config('theme_shiksha', 'catslidercount'); $slidecounts = $slidecounts + 1) {
	$checkcatimage = $PAGE->theme->setting_file_url('catslideimage'.$slidecounts, 'catslideimage'.$slidecounts);
	if(!empty($checkcatimage)) {
		$catslideimagearray[$slidecounts]['image'] = $PAGE->theme->setting_file_url('catslideimage'.$slidecounts, 'catslideimage'.$slidecounts);
	} else {
		$catslideimagearray[$slidecounts]['image'] = $CFG->wwwroot."/theme/shiksha/css/img/icon-cate-1.png";
	}
	$catslideimagearray[$slidecounts]['text'] = get_config('theme_shiksha', 'catslidertext'.$slidecounts);
	$catslideimagearray[$slidecounts]['url'] = get_config('theme_shiksha', 'catsliderurl'.$slidecounts);
}
/*-------------------img-slider section end------------------------*/

/*-------------------course programe section start------------------------*/
$courseprogrammearray = array();
for($courseprogrammecounts = 1; $courseprogrammecounts <= get_config('theme_shiksha', 'courseprogrammecount'); $courseprogrammecounts = $courseprogrammecounts + 1) {
	$checkcourseprogrammimage = $PAGE->theme->setting_file_url('courseprogrammeimage'.$courseprogrammecounts, 'courseprogrammeimage'.$courseprogrammecounts);
	if(!empty($checkcourseprogrammimage)) {
		$courseprogrammearray[$courseprogrammecounts]['image'] = $PAGE->theme->setting_file_url('courseprogrammeimage'.$courseprogrammecounts, 'courseprogrammeimage'.$courseprogrammecounts);
	} else {
		$courseprogrammearray[$courseprogrammecounts]['image'] = $CFG->wwwroot."/theme/shiksha/css/img/img-cat-1.jpg";
	}
	$courseprogrammearray[$courseprogrammecounts]['text'] = get_config('theme_shiksha', 'courseprogrammetext'.$courseprogrammecounts);
	$courseprogrammearray[$courseprogrammecounts]['subtext'] = get_config('theme_shiksha', 'courseprogrammesubtext'.$courseprogrammecounts);
	$courseprogrammearray[$courseprogrammecounts]['url'] = get_config('theme_shiksha', 'courseprogrammelink'.$courseprogrammecounts);
}
/*-------------------course programe section end------------------------*/

/*-------------------parallax accordion section start------------------------*/
$parallaxaccordionarray = array();
for($parallaxaccordioncounts = 1; $parallaxaccordioncounts <= get_config('theme_shiksha', 'parallaxsectionaccodioncount'); $parallaxaccordioncounts = $parallaxaccordioncounts + 1) {
	$paraaccordioniconcheck = $PAGE->theme->setting_file_url('parallaxaccordionicon'.$parallaxaccordioncounts, 'parallaxaccordionicon'.$parallaxaccordioncounts);
	if(!empty($paraaccordioniconcheck)) {
		$parallaxaccordionarray[$parallaxaccordioncounts]['icon'] = $PAGE->theme->setting_file_url('parallaxaccordionicon'.$parallaxaccordioncounts, 'parallaxaccordionicon'.$parallaxaccordioncounts);
	} else {
		$parallaxaccordionarray[$parallaxaccordioncounts]['icon'] = $CFG->wwwroot."/theme/shiksha/css/img/icon-para-1.png";
	}
	$parallaxaccordionarray[$parallaxaccordioncounts]['heading'] = get_config('theme_shiksha', 'parallaxaccordionheading'.$parallaxaccordioncounts);
	$parallaxaccordionarray[$parallaxaccordioncounts]['content'] = get_config('theme_shiksha', 'parallaxaccordioncontent'.$parallaxaccordioncounts);
}

/*-------------------parallax accordion section end------------------------*/

$address = get_config('theme_shiksha', 'address');
$phone = get_config('theme_shiksha', 'phone');
$email = get_config('theme_shiksha', 'email');


$frontpageblockheading = get_config('theme_shiksha', 'frontpageblockheading');
$frontpageblock = get_config('theme_shiksha', 'frontpageblock');
$frontpageblocklink = get_config('theme_shiksha', 'frontpageblocklink');

$frontpageblocksection1 = get_config('theme_shiksha', 'frontpageblocksection1');
$frontpageblocklinksection1 = get_config('theme_shiksha', 'frontpageblocklinksection1');
$frontpageblockdescriptionsection1 = get_config('theme_shiksha', 'frontpageblockdescriptionsection1');

$frontpageblocksection2 = get_config('theme_shiksha', 'frontpageblocksection2');
$frontpageblocklinksection2 = get_config('theme_shiksha', 'frontpageblocklinksection2');
$frontpageblockdescriptionsection2 = get_config('theme_shiksha', 'frontpageblockdescriptionsection2');

$frontpageblocksection3 = get_config('theme_shiksha', 'frontpageblocksection3');
$frontpageblocklinksection3 = get_config('theme_shiksha', 'frontpageblocklinksection3');
$frontpageblockdescriptionsection3 = get_config('theme_shiksha', 'frontpageblockdescriptionsection3');



if ( $CFG->version >= '2015051100.00' ) {
    $file = get_string('privatefiles');
    $hasstringbadge = get_string('badges', 'badges');
} else {
    $file = get_string('myfiles');
    $hasstringbadge = get_string('mybadges', 'badges');
}

$map = '<iframe class = "mapframe" height="430" src="https://www.google.com/maps/embed/v1/place?q='.get_config('theme_shiksha', 'place').',+'.get_config('theme_shiksha', 'country').'&amp;key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe>';

echo $OUTPUT->doctype();?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/bootstrap.css">
	<link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/bootstrap-responsive.css">
	<link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/jquery.bxslider.css">
	<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/font-awesome.css">
	<link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/style.css">
	<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/animation.css" />
	<script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/jquery-2.1.4.js"></script>
	<script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/bootstrap.min.js"></script>
	<script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/jquery.bxslider.js"></script>
	<script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/wow.min.js"></script>
	<script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/jquery-ui.js"></script>
	<script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/jquery.sticky.js"></script> 
	<script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/frontpage.js"></script>
	<script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/jquery.easing.min.js"></script> 
	<script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/jquery.easypiechart.min.js"></script> 
	<script>
 		new WOW().init();
	</script>
	<style>
		ul, ol {
		    margin: 0px !important;
		}
		*[role="main"] {
			display: none;
		}
	</style>
	<?php echo $OUTPUT->standard_head_html() ?>
</head>
	<body class="landing-page">
		<header>
			<div class="row-fluid">
	    		<div class="page-top-bar">
	      			<div class="container">
	      				<?php if (!empty($socialicon1link) || !empty($socialicon2link) || !empty($socialicon3link) || !empty($socialicon4link)) { ?>
		        			<div class="span3 top-social-icons">
			        			<?php if (!empty($socialicon1link)) { ?><a href="<?php echo $socialicon1link; ?>"><img src="<?php echo $socialicon1; ?>" alt=""></a><?php } ?>
			        			<?php if (!empty($socialicon2link)) { ?><a href="<?php echo $socialicon2link; ?>"><img src="<?php echo $socialicon2; ?>" alt=""></a><?php } ?>
			        			<?php if (!empty($socialicon3link)) { ?><a href="<?php echo $socialicon3link; ?>"><img src="<?php echo $socialicon3; ?>" alt=""></a><?php } ?>
			        			<?php if (!empty($socialicon4link)) { ?><a href="<?php echo $socialicon4link; ?>"><img src="<?php echo $socialicon4; ?>" alt=""></a><?php } ?>
		        			</div>
		        		<?php } else { ?>
		        			<div class="span3 top-social-icons"></div>
		        		<?php } ?>
	        			<div class="span9 logining-wr">
	        				<?php if (!isloggedin()) { ?>
	        					<?php if($isregistration->value == 'email') { ?>
		          					<div class="crate-account"><a href="<?php echo $CFG->wwwroot; ?>/login/signup.php?"><?php echo get_string('startsignup'); ?></a></div>
		          				<?php } ?>
						        <form action="<?php echo $CFG->wwwroot; ?>/login/index.php?authldap_skipntlmsso=1" method="post">
						        	<input type="text" placeholder="Username:" name="username">
						            <input type="password" placeholder="Password:" name="password">
						            <input type="submit" value="<?php echo get_string('login');?>">
						        </form>
						    <?php } else { ?>
					        <div class="usermenu dropdown pull-right">
					            <div class="actionmenu nowrap-items">
					              <ul class="nav">
					                <li><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="userbutton"><span class="avatars"><span class="avatar current" ><?php echo $OUTPUT->user_profile_picture(); ?></span></span><span class="welcome">Welcome,</span> <span class="usertext"><?php echo $USER->firstname .' '. $USER->lastname;?></span><i class="fa fa-chevron-down"></i> </span><!--<b class="caret"></b>--></a>
					                  <ul class="dropdown-menu align-tr-br" role="menu" aria-labelledby="dropdownMenu">
					                    <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/my" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('myhome'); ?></span></a></li>
					                    <li class="divider"></li>
					                    <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/user/profile.php?id=<?php echo $USER->id;?>" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('profile'); ?></span></a></li>
					                    <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/badges/view.php?type=1"><span class="menu-action-text"><?php echo $hasstringbadge;?></span></a></li>
					                    <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/message/index.php" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('messages', 'chat'); ?></span></a></li>
					                    <li class="divider"></li>
					                    <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/login/logout.php" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('logout');?></span></a></li>
					                  </ul>
					                </li>
					              </ul>
					            </div>
					        </div>
					        <?php } ?>
	        			</div>
	      			</div>
	    		</div>
			    <!-- Site Logo
			    ================================================== -->
			    <div class="container">
			    	<?php if (get_config('theme_shiksha', 'logoorsitename') === "logo") { ?>
				      <div class="logo-wr">
				        <a class="sitelogo-img" href="<?php echo $CFG->wwwroot; ?>"><img src="<?php echo $haslogo; ?>" alt=""></a>
				      </div>
				    <?php } else if (get_config('theme_shiksha', 'logoorsitename') === "sitename") { ?>
				    	<div class="logo-wr">
				    		<a class="sitelogo-text" href="<?php echo $CFG->wwwroot; ?>"><?php echo $SITE->fullname; ?></a>
				      	</div>
				    <?php } else if (get_config('theme_shiksha', 'logoorsitename') === "iconsitename") { ?>
				    	<div class="logo-wr">
				        <a class="sitelogo-icon-name" href="<?php echo $CFG->wwwroot; ?>"><img src="<?php echo $hasiconlogo; ?>" alt=""><?php echo $SITE->fullname; ?></a>
				      	</div>
				    <?php } ?>
				</div>
	   			<!-- Navbar
	    		================================================== -->
			    <div class="navbar navbar-inverse navbar-static-top">
			      <div class="navbar-inner">
			        <div class="container">
			          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
			          <div class="nav-collapse collapse">
			            <?php echo $OUTPUT->custom_menu();?>
			          </div>
			        </div>
			      </div>
			    </div>
	  		</div>
		</header>
		<div id="page" class="row-fluid">
			<?php if (get_config('theme_shiksha','controlbannerorslidersection') == 0) {?>
			  	<section class="clearfix">
			  		<?php if (get_config('theme_shiksha', 'frontpageimagecontent') === "0") { 
			  			$bannereffect = get_config('theme_shiksha', 'bannereffect');
						if ($bannereffect == 1) {
							$bannereffects = 'repeat';
						} else if ($bannereffect == 2) {
							$bannereffects = 'static';
						} else if ($bannereffect == 3) { // for parallax effect
							$bannereffects = 'fixed';
						} else if ($bannereffect == 4) {
							$bannereffects = 'cover';
						}
						$bannereffectbackgroundcolor = get_config('theme_shiksha', 'staticbackgroundcolor');?>
					    <div class="static-banner <?php if (!empty($bannereffectbackgroundcolor)) { ?>banner-bg-color<?php } ?> slider-bg-<?php echo $bannereffects; ?>">
					      <div class="container">
					        <div class="banner-content">
					          <?php if (!empty($addtext)) { echo $addtext; } ?>
					          <?php if (!empty($addbuttontext)) { ?>
					          	<a href="<?php if(!empty($addlink)) { echo $addlink; } ?>" class="banner-button"><?php echo $addbuttontext;?><img src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/img/steps-arrow-tick.png" alt=""></a>
					          <?php } ?>
					        </div>
					      </div>
					    </div>
					<?php } else if (get_config('theme_shiksha', 'frontpageimagecontent') === "1") { 
						$numberofsliders = get_config('theme_shiksha', 'slidercount'); ?>
						<div class="static-slider">
					      	<div class="row-fluid">
						        <div id="myCarousel" class="carousel slide" data-interval = "<?php echo $sliderinterval; ?>">
						        <?php if (!empty($numberofsliders)) { ?>
				            		<ol class="carousel-indicators">
										<?php for ($slidecount = 1; $slidecount <= $numberofsliders; $slidecount++) { ?>
								            <li data-target="#myCarousel" data-slide-to="<?php echo $slidecount - 1;?>" <?php if ($slidecount == 1) { ?>class="active"<?php } ?>></li>
								        <?php } ?>
							        </ol>
							    <?php } ?>
						          <!-- Carousel items -->
						            <div class="carousel-inner">

						            	<?php if (!empty($numberofsliders)) { 
										for ($slidecount = 1; $slidecount <= $numberofsliders; $slidecount++) { 
											$sliderimageurl = $PAGE->theme->setting_file_url('slideimage'.$slidecount, 'slideimage'.$slidecount);
											$sliderimagetext = get_config('theme_shiksha', 'slidertext'.$slidecount);
											$sliderimagelink = get_config('theme_shiksha', 'sliderurl'.$slidecount);
											$sliderbuttontext = get_config('theme_shiksha', 'sliderbuttontext'.$slidecount);
											$slideeffect = get_config('theme_shiksha', 'slidereffect'.$slidecount);
											if ($slideeffect == 1) {
												$slideeffects = 'repeat';
											} else if ($slideeffect == 2) {
												$slideeffects = 'static';
											} else if ($slideeffect == 3) { // for parallax effect
												$slideeffects = 'fixed';
											} else if ($slideeffect == 4) {
												$slideeffects = 'cover';
											}
											$slidebackgroundcolor = get_config('theme_shiksha', 'sliderbackgroundcolor'.$slidecount);
											
										?>
							            	<div class="<?php if ($slidecount == 1) { ?>active <?php } ?>item static-slider-<?php echo $slidecount; ?><?php if (!empty($slidebackgroundcolor)) { ?> slider-bg-color-<?php echo $slidecount; ?><?php } ?> slider-bg-<?php echo $slideeffects; ?>">
							            	<div class="container">
							                	<div class="banner-content">
										          <?php if (!empty($sliderimagetext)) { echo $sliderimagetext; } ?>
										          <?php if (!empty($sliderbuttontext)) { ?>
					          						<a href="<?php if(!empty($sliderimagelink)) { echo $sliderimagelink; } ?>" class="banner-button"><?php echo $sliderbuttontext;?><img src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/img/steps-arrow-tick.png" alt=""></a>
					          					  <?php } ?>
										        </div>
							                </div>
							            	</div>
							            <?php //}
							            } /*end of for */
							            }/*end of numberofsliders if*/?>

						          	</div>
						          	<!-- Carousel nav --> 
						          	<a class="carousel-control left" href="#myCarousel" data-slide="prev"><img src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/img/img-slider-left.png" alt="Move to Left"></a>
						          	<a class="carousel-control right" href="#myCarousel" data-slide="next"><img src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/img/img-slider-right.png" alt="Move to Right"></a>
						        </div>
					      	</div>
					    </div>
					<?php } ?>
			  	</section>
			<?php } ?>
		  	<div id="page-content">
		  		<!-- cat slider start -->
		  		<?php if (get_config('theme_shiksha','controlcatslidersection') == 0) { ?>
				    <div class="row-fluid">
				      	<div class="img-slider">
					        <div class="container wow fadeInUp animated" data-wow-duration="1s" data-wow-delay="0s" data-wow-offset="10" >
											<div id="hoverarrow_cont1">
												<div id="hoverarrow1"></div>
											</div>
					          	<ul class="bxslider catslider">
					          		<?php for ($slidecounts = 1; $slidecounts <= get_config('theme_shiksha', 'catslidercount'); $slidecounts = $slidecounts + 1) { ?>
						            <li><a href="<?php echo $catslideimagearray[$slidecounts]['url']; ?>"><img src="<?php echo $catslideimagearray[$slidecounts]['image']; ?>" alt=""/> <br><br><br><?php echo $catslideimagearray[$slidecounts]['text']; ?></a></li>
						            <?php } ?>
					          	</ul>                                                                                                                            
					        </div>
				      	</div>
				    </div>
				    <!-- cat slider end -->
				<?php } ?>
				<?php if (get_config('theme_shiksha','controlfeaturedcontentsection') == 0) { ?>
				    <!-- courseprogram start-->
			        <div class="row-fluid">
				      	<section class="img-desc">
					        <div class="container">
					          <div class="img-desc-heading wow fadeInUp animated" data-wow-duration="1s" data-wow-delay="0s" data-wow-offset="10">
					            <?php if (get_config('theme_shiksha', 'courseprogrammesubheading')) { ?><h3><?php echo get_config('theme_shiksha', 'courseprogrammesubheading'); ?></h3><?php } ?>
					            <?php if (get_config('theme_shiksha', 'courseprogrammeheading')) { ?><h2><?php echo get_config('theme_shiksha', 'courseprogrammeheading'); ?></h2><?php } ?>
					            <?php if (get_config('theme_shiksha', 'courseprogrammecontent')) { ?><p><?php echo get_config('theme_shiksha', 'courseprogrammecontent'); ?></p><?php } ?>
					          </div>
					          <div class="img-area">
					            <ul>
					              <?php $incrementdelaywow = 1;?>
					              <?php for($courseprogrammecounts = 1; $courseprogrammecounts <= get_config('theme_shiksha', 'courseprogrammecount'); $courseprogrammecounts = $courseprogrammecounts + 1) { ?>
						              <?php if (!empty($courseprogrammearray[$courseprogrammecounts]['text'])) { ?>
						              <li class="wow fadeInUp animated" data-wow-duration="1s" data-wow-delay="<?php echo $incrementdelaywow?>s" data-wow-offset="10">
						              	<a href="<?php echo $courseprogrammearray[$courseprogrammecounts]['url']; ?>">
						              		<img src="<?php echo $courseprogrammearray[$courseprogrammecounts]['image']; ?>" alt="">
						                	<div class="img-area-content">
						                  		<h4><?php echo $courseprogrammearray[$courseprogrammecounts]['text']; ?></h4>
						                  		<h6><?php echo $courseprogrammearray[$courseprogrammecounts]['subtext']; ?></h6>
						                	</div>
						                </a>
						              </li>
						              <?php } ?>
					              <?php $incrementdelaywow = $incrementdelaywow + 0.25; } ?>
					            </ul>
					          </div>
					        </div>
				      	</section>
				    </div>
			    	<!-- course program end -->
			    <?php } ?>
			    <!--parallax start-->
			    <?php if (get_config('theme_shiksha','controlparallaxsection') == 0) {?>
				    <div class="row-fluid">
				      	<section class="parallax-content">
					        <div class="container">
					          	<?php if (get_config('theme_shiksha', 'parallaxsectioncontentposition') === '2') { ?>
					          	  <?php $checkparallaxsectioncontent = get_config('theme_shiksha', 'parallaxsectioncontent');
					          	  		if (!empty($checkparallaxsectioncontent)) { ?>
								          	<div class="span6 wow bounceInRight animated" data-wow-duration="1s" data-wow-delay="0s" data-wow-offset="10">
									            <?php echo get_config('theme_shiksha', 'parallaxsectioncontent');?>
								          	</div>
							      <?php } 
							      ?>
							      <?php $checkparallaxsectionaccodioncount = get_config('theme_shiksha', 'parallaxsectionaccodioncount');
							      		if (!empty($checkparallaxsectionaccodioncount)) { ?>
							        	<div class="span6 wow bounceInLeft animated" data-wow-duration="1s" data-wow-delay="0s" data-wow-offset="10">
								            <div class="accordion">
								              <?php for($parallaxaccordioncounts = 1; $parallaxaccordioncounts <= get_config('theme_shiksha', 'parallaxsectionaccodioncount'); $parallaxaccordioncounts = $parallaxaccordioncounts + 1) { ?>
									              
									              	<?php if (!empty($parallaxaccordionarray[$parallaxaccordioncounts]['heading'])) { ?>
										                <div class="accordion-heading"> 
										                	<a class="accordion-toggle">
										                		<img src="<?php echo $parallaxaccordionarray[$parallaxaccordioncounts]['icon']; ?>" alt=""><?php echo $parallaxaccordionarray[$parallaxaccordioncounts]['heading']; ?>
										                	</a> 
										                </div>
									                <?php } ?>
									                <?php if (!empty($parallaxaccordionarray[$parallaxaccordioncounts]['content'])) { ?>
										                <div class="accordion-body">
										                  <div class="accordion-inner">
										                    <?php echo $parallaxaccordionarray[$parallaxaccordioncounts]['content']; ?>
										                  </div>
										                </div>
									                <?php } ?>
								              <?php } ?>
								            </div>
								        </div>
						          <?php } ?>
						        <?php } else if (get_config('theme_shiksha', 'parallaxsectioncontentposition') === '1') { ?>
						        	<?php $checkparallaxsectionaccodioncount = get_config('theme_shiksha', 'parallaxsectionaccodioncount');
							      		if (!empty($checkparallaxsectionaccodioncount)) { ?>
							        	<div class="span6 wow bounceInLeft animated" data-wow-duration="1s" data-wow-delay="0s" data-wow-offset="10">
								            <div class="accordion">
								              <?php for($parallaxaccordioncounts = 1; $parallaxaccordioncounts <= get_config('theme_shiksha', 'parallaxsectionaccodioncount'); $parallaxaccordioncounts = $parallaxaccordioncounts + 1) { ?>
									              <!-- <div class="accordion-group"> -->
									              	<?php if (!empty($parallaxaccordionarray[$parallaxaccordioncounts]['heading'])) { ?>
										                <div class="accordion-heading"> 
										                	<a class="accordion-toggle">
										                		<img src="<?php echo $parallaxaccordionarray[$parallaxaccordioncounts]['icon']; ?>" alt=""><?php echo $parallaxaccordionarray[$parallaxaccordioncounts]['heading']; ?>
										                	</a> 
										                </div>
									                <?php } ?>
									                <?php if (!empty($parallaxaccordionarray[$parallaxaccordioncounts]['content'])) { ?>
										                <div class="accordion-body">
										                  <div class="accordion-inner">
										                    <?php echo $parallaxaccordionarray[$parallaxaccordioncounts]['content']; ?>
										                  </div>
										                </div>
									                <?php } ?>
									              <!-- </div> -->
								              <?php } ?>
								            </div>
								        </div>
						          	<?php } ?>
						          	<?php $checkparallaxsectioncontent = get_config('theme_shiksha', 'parallaxsectioncontent');
					          	  		if (!empty($checkparallaxsectioncontent)) { ?>
							          	<div class="span6 wow bounceInRight animated" data-wow-duration="1s" data-wow-delay="0s" data-wow-offset="10">
								            <?php echo get_config('theme_shiksha', 'parallaxsectioncontent');?>
							          	</div>
							        <?php } ?>
						        <?php } ?>
					        </div>
				      	</section>
				    </div>
			    <!-- parallax end -->
			    <?php } ?>
			    <?php if (get_config('theme_shiksha','controlfeaturedcoursesection') == 0) {?>
				    <?php if (!empty($coursedetailsarray)) { ?>
					    <div class="row-fluid">
					      	<section  class="featured-courses">
						        <div class="container">
						          	<div class="courses">
							            <div class="featured-courses-heading wow fadeInUp animated" data-wow-duration="1s" data-wow-delay="0s" data-wow-offset="10">
							              	<?php if (get_config('theme_shiksha', 'featuredcourseheading')) { ?><h2><?php echo get_config('theme_shiksha', 'featuredcourseheading'); ?></h2><?php } ?>
							              	<?php if (get_config('theme_shiksha', 'featuredcoursesubheading')) { ?><h4><?php echo get_config('theme_shiksha', 'featuredcoursesubheading'); ?></h4><?php } ?>
							              	<h5><a href="<?php echo $CFG->wwwroot.'/course';?>"><?php echo get_string('viewallcourses');?><i class="fa fa-long-arrow-right"></i></a></h5>
							              	<div class="clearfix"></div>
							            </div>
						            	<div class="course-items">
						            		<?php $incrementdelaywow = 1;?>
						            		<?php foreach($coursedetailsarray as $coursedetailsarrayvalue) { ?>
								              	<div class="course-item wow fadeInUp animated" data-wow-duration="1s" data-wow-delay="<?php echo $incrementdelaywow?>s" data-wow-offset="10">
									                <h5><a href="<?php echo $coursedetailsarrayvalue['courseid'];?>"><?php echo $coursedetailsarrayvalue['coursename'];?></a></h5>
									                <?php if (!empty($coursedetailsarrayvalue['teachername'])) { ?><h6><?php echo get_string('defaultcourseteacher');?> : 
									                	<a href=<?php echo $CFG->wwwroot."/user/view.php?id=".$coursedetailsarrayvalue['teacherid']; ?> >
									                		<?php echo $coursedetailsarrayvalue['teachername'];?></a></h6><?php } ?>
									                <?php if (!empty($coursedetailsarrayvalue['summary'])) { ?><p><?php echo $coursedetailsarrayvalue['summary'];?></p><?php } ?>
									                <?php if (!empty($coursedetailsarrayvalue['categoryname'])) { ?><h4><?php echo $coursedetailsarrayvalue['categoryname'];?></h4><?php } ?>
								              	</div>
							              	<?php $incrementdelaywow = $incrementdelaywow + 0.25;
							              	} ?>
						            	</div>
		          					</div>
		        				</div>
		      				</section>
		    			</div>
		    		<?php } ?>
		    	<?php } ?>
		    	
		    	<?php if (get_config('theme_shiksha','controlsuccessstorysection') == 0) {?>
	    			<?php $hassuccessstoryheading = get_config('theme_shiksha', 'successstoryheading'); 
	    				if (!empty($hassuccessstoryheading)) { ?>
	    				<input type="hidden" name="color" class = "color" value="<?php echo get_config('theme_shiksha', 'color'); ?>">
		    			<div class="row-fluid">
					      	<section  class="number-section">
						        <div class="container">
						          <div class="number-section-heading wow fadeInUp animated" data-wow-duration="1s" data-wow-delay="0s" data-wow-offset="10">
						            <?php $hassuccessstoryheading = get_config('theme_shiksha', 'successstoryheading');
						            if (!empty($hassuccessstoryheading)) { ?>
						            	<h3><?php echo get_config('theme_shiksha', 'successstoryheading'); ?></h3>
						            <?php } 
						            $hassuccessstorysubheading = get_config('theme_shiksha', 'successstorysubheading');
						            if (!empty($hassuccessstorysubheading)) { ?>
						            	<h2><?php echo get_config('theme_shiksha', 'successstorysubheading'); ?></h2>
						            <?php } ?>
						          </div>
						          <div class="number-section-content">
						            <ul>
						              <?php $hassuccessstorynumber1 = get_config('theme_shiksha', 'successstorynumber1'); if (!empty($hassuccessstorynumber1)) { ?>
							              <li>
							                <div class="chart wow" data-percent="<?php echo get_config('theme_shiksha', 'successstorynumber1'); ?>"><span class="percent"></span> </div>
							                <?php $hassuccessstorytext1 = get_config('theme_shiksha', 'successstorytext1'); if (!empty($hassuccessstorytext1)) { ?>
							                	<h3><?php echo get_config('theme_shiksha', 'successstorytext1'); ?></h3>
							                <?php } ?>
							              </li>
							          <?php } $hassuccessstorynumber2 = get_config('theme_shiksha', 'successstorynumber2'); if (!empty($hassuccessstorynumber2)) { ?>
							              <li>
							                <div class="chart wow" data-percent="<?php echo get_config('theme_shiksha', 'successstorynumber2'); ?>"><span class="percent"></span> </div>
							                <?php $hassuccessstorytext2 = get_config('theme_shiksha', 'successstorytext2'); if (!empty($hassuccessstorytext2)) { ?>
							                	<h3><?php echo get_config('theme_shiksha', 'successstorytext2'); ?></h3>
							                <?php } ?>
							              </li>
							          <?php } $hassuccessstorynumber3 = get_config('theme_shiksha', 'successstorynumber3'); if (!empty($hassuccessstorynumber3)) { ?>
							              <li>
							                <div class="chart wow" data-percent="<?php echo get_config('theme_shiksha', 'successstorynumber3'); ?>"><span class="percent"></span></div>
							                <?php $hassuccessstorytext3 = get_config('theme_shiksha', 'successstorytext3'); if (!empty($hassuccessstorytext3)) { ?>
							                	<h3><?php echo get_config('theme_shiksha', 'successstorytext3'); ?></h3>
							                <?php } ?>
							              </li>
							          <?php } $hassuccessstorynumber4 = get_config('theme_shiksha', 'successstorynumber4'); if (!empty($hassuccessstorynumber4)) { ?>
							              <li>
							                <div class="chart wow" data-percent="<?php echo get_config('theme_shiksha', 'successstorynumber4'); ?>"><span class="percent"></span> </div>
							                <?php $hassuccessstorytext4 = get_config('theme_shiksha', 'successstorytext4'); if (!empty($hassuccessstorytext4)) { ?>
							                	<h3><?php echo get_config('theme_shiksha', 'successstorytext4'); ?></h3>
							                <?php } ?>
							              </li>
							          <?php } ?>
						            </ul>
						          </div>
						        </div>
					      	</section>
					    </div>
					<?php } ?>
				<?php } ?>
			    <div class="row-fluid">
			      	<section class="contacts">
			      		<?php if (get_config('theme_shiksha','controlmapsection') == 0) {?>
					        <div class="<?php echo $span; ?> home-map bounceInRight wow animated"  data-wow-duration="1.5s" data-wow-delay=".55s" data-wow-offset="10">
					          <div class="map-wr">
					            <?php echo $map; ?>
					          </div>
					        </div>
					    <?php } ?>
					    <?php if (get_config('theme_shiksha','controlcontactsection') == 0) {?>
					        <?php $hasemailcontactheading = get_config('theme_shiksha', 'emailcontactheading'); if (!empty($hasemailcontactheading)) { ?>
						        <div class="<?php echo $span; ?> bounceInLeft wow animated"  data-wow-duration="1.5s" data-wow-delay=".55s" data-wow-offset="10">
						          <div class="offset1 home-contact-form">
						            <h4><?php echo get_config('theme_shiksha', 'emailcontactheading');?></h4>
						            <p><?php echo get_config('theme_shiksha', 'emailcontactsubheading');?></p>
						            <!-- <form class="form-horizontal"> -->
						              <div class="control-group">
						              	<input type="hidden" value="<?php echo get_string('emptynameemail', 'theme_shiksha');?>" class="emptynameemail">
						              	<input type="hidden" value="<?php echo get_string('msgsent', 'theme_shiksha');?>" class="msgsent">
						              	<input type="hidden" value="<?php echo $CFG->wwwroot?>/theme/shiksha/mail.php" class = "hiddenform">
						                <input type="text" name = "name" id="name" class="span6 msgname" placeholder="Name">
						                <input type="email" name ="email" id="email" class="span6 home-email-pad" placeholder="Email">
						                <div class="clearfix"></div>
						                <textarea name ="message" class="span12 msg" rows="3" placeholder="Message"></textarea>
						              </div>
						              <div id = "msgresponse"></div>
						              <button type="submit" class="btn pull-right send">Send</button>
						            <!-- </form> -->
						          </div>
						        </div>
						    <?php } ?>
						<?php } ?>
			      	</section>
			    </div>
			    <?php if (get_config('theme_shiksha','controlnewssection') == 0) { ?>
				    <?php if(!empty($frontpageblockheading)) { ?>
					    <div class="row-fluid">
					      	<section class="news-update wow fadeInUp animated" data-wow-duration="1s" data-wow-delay="0s" data-wow-offset="10">
						        <div class="container">
						          <div class="span3 newsheading">
						            <h3><?php if(!empty($frontpageblockheading)) { echo $frontpageblockheading; } ?></h3>
						            <p><a href="<?php if(!empty($frontpageblocklink)) { echo $frontpageblocklink; } ?>"><?php if(!empty($frontpageblock)) { echo $frontpageblock; } ?><i class="fa fa-long-arrow-right"></i></a></p>
						          </div>
						          <?php if(!empty($frontpageblocklinksection1)) { ?>
						          <div class="span3 newsitem">
						            <p><a href="<?php if(!empty($frontpageblocklinksection1)) { echo $frontpageblocklinksection1; } ?>"><?php if(!empty($frontpageblocksection1)) { echo $frontpageblocksection1; } ?></a></p>
						            <h5><?php if(!empty($frontpageblockdescriptionsection1)) { echo $frontpageblockdescriptionsection1; } ?></h5>
						          </div>
						          <?php } if(!empty($frontpageblocklinksection2)) { ?>
						          <div class="span3 newsitem">
						            <p><a href="<?php if(!empty($frontpageblocklinksection2)) { echo $frontpageblocklinksection2; } ?>"><?php if(!empty($frontpageblocksection2)) { echo $frontpageblocksection2; } ?></a></p>
						            <h5><?php if(!empty($frontpageblockdescriptionsection2)) { echo $frontpageblockdescriptionsection2; } ?></h5>
						          </div>
						          <?php } if(!empty($frontpageblocklinksection3)) { ?>
						          <div class="span3 newsitem">
						            <p><a href="<?php if(!empty($frontpageblocklinksection3)) { echo $frontpageblocklinksection3; } ?>"><?php if(!empty($frontpageblocksection3)) { echo $frontpageblocksection3; } ?></a></p>
						            <h5><?php if(!empty($frontpageblockdescriptionsection3)) { echo $frontpageblockdescriptionsection3; } ?></h5>
						          </div>
						          <?php } ?>
						        </div>
					      	</section>
					    </div>
					<?php } ?>
				<?php } ?>
		   </div>
		</div>
		<!-- END of header -->
		<?php
			echo $OUTPUT->main_content();
			include('footer.php');
		?>
		<script type="text/javascript">

			$(document).ready(function(){
/* 				var app_arrowHover = {
					noItems: 5
					,oneLinkPadding: parseInt($("ul.bxslider li").css("padding-left").split("px")[0])
					,oneLinkWidth: $("ul.bxslider li").width()
					,oneLinkFullWidth: this.oneLinkWidth + (this.oneLinkPadding * 2)
					,fullContainerWidth: (this.oneLinkFullWidth * this.noItems)
					,getLinkPosition: function(noPosition) {
						return ((this.oneLinkFullWidth * noPosition) - (this.oneLinkWidth / 2));
					}
					,init: function(){
						$("ul.bxslider li").on("mouseover",function(){
						var actualLinkIndex = (parseInt($("ul.bxslider li").index(this)) + 1);
							//alert(this.find("img").attr("src"));
						});
						$("ul.bxslider li").each(function() {
							//alert();
						});
					}
				}

				app_arrowHover.init(); */

			});
		</script>
	</body>
</html>

