<?php
// Get the HTML for the settings bits.
$html = theme_shiksha_get_html_for_settings($OUTPUT, $PAGE);
GLOBAL $USER, $PAGE, $CFG, $DB;
$haslogo = $PAGE->theme->setting_file_url('logo', 'logo');
if(!empty($haslogo)) {
  $haslogo = $PAGE->theme->setting_file_url('logo', 'logo');
} else {
  $haslogo = $CFG->wwwroot.'/theme/shiksha/css/img/logo.jpg';
}
$hasiconlogo = $PAGE->theme->setting_file_url('icon', 'icon');
if(!empty($hasiconlogo)) {
  $hasiconlogo = $PAGE->theme->setting_file_url('icon', 'icon');
} else {
  $hasiconlogo = $CFG->wwwroot.'/theme/shiksha/css/img/logo-icon.jpg';
}
/*-------------------social contacts section start---------------------*/
$socialicon1link = get_config('theme_shiksha', 'socialiconlink1');
$socialicon2link = get_config('theme_shiksha', 'socialiconlink2');
$socialicon3link = get_config('theme_shiksha', 'socialiconlink3');
$socialicon4link = get_config('theme_shiksha', 'socialiconlink4');
$socialicon1 = $PAGE->theme->setting_file_url('socialicon1', 'socialicon1');
if(!empty($socialicon1)) {
  $socialicon1 = $PAGE->theme->setting_file_url('socialicon1', 'socialicon1');
} else {
  $socialicon1 = $CFG->wwwroot."/theme/shiksha/css/img/icon-facebook.png";
}
$socialicon2 = $PAGE->theme->setting_file_url('socialicon2', 'socialicon2');
if(!empty($socialicon2)) {
  $socialicon2 = $PAGE->theme->setting_file_url('socialicon2', 'socialicon2');
} else {
  $socialicon2 = $CFG->wwwroot."/theme/shiksha/css/img/icon-twitter.png";
}
$hassocialicon3 = $PAGE->theme->setting_file_url('socialicon3', 'socialicon3');
if(!empty($hassocialicon3)) {
  $socialicon3 = $PAGE->theme->setting_file_url('socialicon3', 'socialicon3');
} else {
  $socialicon3 = $CFG->wwwroot."/theme/shiksha/css/img/icon-linkedin.png";
}
$hassocialicon4 = $PAGE->theme->setting_file_url('socialicon4', 'socialicon4');
if(!empty($hassocialicon4)) {
  $socialicon4 = $PAGE->theme->setting_file_url('socialicon4', 'socialicon4');
} else {
  $socialicon4 = $CFG->wwwroot."/theme/shiksha/css/img/icon-instagram.png";
}
/*--------------------------social contacts section end----------------------------*/
echo $OUTPUT->doctype();

$isregistration = $DB->get_record('config', array('name'=>'registerauth'));
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/bootstrap.css">
    <link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/bootstrap-responsive.css">
    <link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/jquery.bxslider.css">
    <link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/font-awesome.css">
    <link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/style.css">
  
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/jquery-2.1.4.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/bootstrap.min.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/login.js"></script>
    <?php echo $OUTPUT->standard_head_html() ?>
</head>
<body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>
<?php include $CFG->dirroot . '/theme/shiksha/analyticstracking.php'; ?>
<header role="banner" class="navbar">
  <div class="container">
    <div class="page-top-bar">
        <div class="container">
          <?php if (!empty($socialicon1link) || !empty($socialicon2link) || !empty($socialicon3link) || !empty($socialicon4link)) { ?>
            <div class="span3 top-social-icons">
              <?php if (!empty($socialicon1link)) { ?><a href="<?php echo $socialicon1link; ?>"><img src="<?php echo $socialicon1; ?>" alt=""></a><?php } ?>
              <?php if (!empty($socialicon2link)) { ?><a href="<?php echo $socialicon2link; ?>"><img src="<?php echo $socialicon2; ?>" alt=""></a><?php } ?>
              <?php if (!empty($socialicon3link)) { ?><a href="<?php echo $socialicon3link; ?>"><img src="<?php echo $socialicon3; ?>" alt=""></a><?php } ?>
              <?php if (!empty($socialicon4link)) { ?><a href="<?php echo $socialicon4link; ?>"><img src="<?php echo $socialicon4; ?>" alt=""></a><?php } ?>
            </div>
          <?php } else { ?>
            <div class="span3 top-social-icons"></div>
          <?php } ?>
        </div>
    </div>
    <!-- Site Logo
    ================================================== -->
    <div class="container">
      <?php if (get_config('theme_shiksha', 'logoorsitename') === "logo") { ?>
        <div class="logo-wr">
          <a class="sitelogo-img" href="<?php echo $CFG->wwwroot; ?>"><img src="<?php echo $haslogo; ?>" alt=""></a>
        </div>
      <?php } else if (get_config('theme_shiksha', 'logoorsitename') === "sitename") { ?>
        <div class="logo-wr">
          <a class="sitelogo-text" href="<?php echo $CFG->wwwroot; ?>"><?php echo $SITE->fullname; ?></a>
          </div>
      <?php } else if (get_config('theme_shiksha', 'logoorsitename') === "iconsitename") { ?>
        <div class="logo-wr">
          <a class="sitelogo-icon-name" href="<?php echo $CFG->wwwroot; ?>"><img src="<?php echo $hasiconlogo; ?>" alt=""><?php echo $SITE->fullname; ?></a>
          </div>
      <?php } ?>
    </div>
    <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-inverse navbar-static-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <div class="nav-collapse collapse">
            <?php echo $OUTPUT->custom_menu();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>