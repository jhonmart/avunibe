<?php
// Get the HTML for the settings bits.
$html = theme_shiksha_get_html_for_settings($OUTPUT, $PAGE);
// Set default (LTR) layout mark-up for a two column page (side-pre-only).
$regionmain = 'span9 pull-right'; //UIAV de span8 a  span9
$sidepre = 'span3 desktop-first-column left-menu-close'; //UIAV de span4 a  span3

// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmain = 'span9'; //UIAV de span8 a  span9
    $sidepre = 'span3 pull-right left-menu-close'; //UIAV de span4 a  span3
}
require('header.php');?>
<div id="page" class="container-fluid">
    <?php if ($CFG->version >= 2015051100) {
        echo $OUTPUT->full_header();
    } else { ?>
    <header id="page-header" class="clearfix">
        <div id="page-navbar" class="clearfix">
            <div class = "container">
                <div class="row">
                    <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                    <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
                </div>
            </div>
        </div>
        <div id="course-header">
            <?php echo $OUTPUT->course_header(); ?>
        </div>
    </header>
    <?php } ?>
    <div id="page-content" class="row-fluid page-inner">
        <div class="container">
            <section id="region-main" class="<?php echo $regionmain; ?>">
                <?php
                echo $OUTPUT->course_content_header();
                echo $OUTPUT->main_content();
                echo $OUTPUT->course_content_footer();
                ?>
            </section>
            <?php
            echo $OUTPUT->blocks('side-pre', $sidepre);
            ?>
        </div>
    </div>
</div>

    <?php
        include('footer.php');
        echo $OUTPUT->standard_end_of_body_html()
    ?>

<!--//// UIAV - INICIO - Manejo del plugin Expandir "columns2.php" ////-->
<script type="text/javascript">
    $(document).ready(function(){
        $("#region-main").prepend("<div id='fullwidth1x'><div id='fullwidth_cmd1x'><img src='https://aulavirtual.unibe.edu.do/theme/shiksha/pix/icon_expand1x.png' /></div><div class='clear1x'></div></div>");
        $("#fullwidth_cmd1x").on("click",function() {
            if ($("#region-main").hasClass("span9")) {
                $("#block-region-side-pre").hide();
                $("#region-main").attr("class","span12 pull-right");
                $("#fullwidth_cmd1x img, #fullwidth_cmd2x img").attr("src","https://aulavirtual.unibe.edu.do/theme/shiksha/pix/icon_contract1x.png");
            } else {
                $("#block-region-side-pre").show();
                $("#region-main").attr("class","span9 pull-right");
                $("#fullwidth_cmd1x img, #fullwidth_cmd2x img").attr("src","https://aulavirtual.unibe.edu.do/theme/shiksha/pix/icon_expand1x.png");
            }
        });

        //// UIAV - Barra de navegacion con EXPANDIR ////
        $("#page-header .breadcrumb-button").prepend("<div id='fullwidth_cmd2x' class='hide'><img src='https://aulavirtual.unibe.edu.do/theme/shiksha/pix/icon_expand1x.png' /></div>");
        $("#fullwidth_cmd2x").on("click",function() {
            if ($("#region-main").hasClass("span9")) {
                $("#block-region-side-pre").hide();
                $("#region-main").attr("class","span12 pull-right");
                $("#fullwidth_cmd2x img, #fullwidth_cmd1x img").attr("src","https://aulavirtual.unibe.edu.do/theme/shiksha/pix/icon_contract1x.png");
            } else {
                $("#block-region-side-pre").show();
                $("#region-main").attr("class","span9 pull-right");
                $("#fullwidth_cmd2x img, #fullwidth_cmd1x img").attr("src","https://aulavirtual.unibe.edu.do/theme/shiksha/pix/icon_expand1x.png");
            }
        });

        /* //// UIAV - INICIO //// - Cursos en 3 columnas bonitosas */
        $(".course_list .coursebox").prepend("<div class='courseicon1x'></div>");
//        $(".dashboard1x .page-header-headings").append("<h3>Mis Asignaturas</h3>");
        $(".course_list .coursebox .activity_info").hide();
        $(".course_list .coursebox .course_title .title a").on("mouseenter",function(){
            $(this).parent().parent().parent().addClass("shadow1x");
        });
        $(".course_list .coursebox .course_title .title a").on("mouseleave",function(){
            $(this).parent().parent().parent().removeClass("shadow1x");
        });
        /* //// UIAV - FIN //// - Cursos en 3 columnas bonitosas  */
    });
</script>
<!--//// UIAV - FIN ////-->

</body>
</html>
