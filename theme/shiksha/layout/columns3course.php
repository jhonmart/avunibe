<?php
// Get the HTML for the settings bits.
$html = theme_shiksha_get_html_for_settings($OUTPUT, $PAGE);
GLOBAL $DB, $CFG, $cm;
$courseimage = '';
$colorscheme = get_config('theme_shiksha', 'colorscheme');
$categoryid = optional_param('categoryid', array(), PARAM_INT);

// Set default (LTR) layout mark-up for a three column page.
$regionmainbox = 'span9';
$regionmain = 'span8 pull-right';
$sidepre = 'span4 desktop-first-column';
$sidepost = 'span3 pull-right';
// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmainbox = 'span9 pull-right';
    $regionmain = 'span8';
    $sidepre = 'span4 pull-right';
    $sidepost = 'span3 desktop-first-column';
}
if ($categoryid == 1) {
    $course = $DB->get_records_sql('SELECT course.*, course_categories.id as catid, course_categories.name as catname FROM {course} AS course INNER JOIN {course_categories} AS course_categories ON course.category = course_categories.id WHERE course.id != ? and course.visible = ?',array(1, 1));
} else if ($categoryid){
	$course = $DB->get_records_sql('SELECT course.*, course_categories.id as catid, course_categories.name as catname FROM {course} AS course INNER JOIN {course_categories} AS course_categories ON course.category = course_categories.id WHERE course.id != ? and course.visible = ? and course.category = ?',array(1, 1, $categoryid));
} else {
	$course = $DB->get_records_sql('SELECT course.*, course_categories.id as catid, course_categories.name as catname FROM {course} AS course INNER JOIN {course_categories} AS course_categories ON course.category = course_categories.id WHERE course.id != ? and course.visible = ?',array(1, 1));
}
$coursedetailsarray = array();
foreach ($course as $key => $coursevalue) {     
    $coursedetailsarray[$key]["courseid"] = $CFG->wwwroot."/course/view.php?id=".$coursevalue->id;
    $coursedetailsarray[$key]["enroledusers"] = $CFG->wwwroot."/enrol/users.php?id=".$coursevalue->id;
    $coursedetailsarray[$key]["coursename"] = $coursevalue->fullname;
    $summarystring = strlen($coursevalue->summary) > 71 ? substr($coursevalue->summary, 0, 71)." [ .... ]" : $coursevalue->summary;
    $coursedetailsarray[$key]["coursesummary"] = $summarystring;
    $courseteacher = $DB->get_record_sql('SELECT u.*
    FROM {course} c
    JOIN {context} ct ON c.id = ct.instanceid
    JOIN {role_assignments} ra ON ra.contextid = ct.id
    JOIN {user} u ON u.id = ra.userid
    JOIN {role} r ON r.id = ra.roleid Where c.id = ? and r.shortname = ?', array($coursevalue->id, 'editingteacher'));
    if(!empty($courseteacher)) {
        $coursedetailsarray[$key]["teacherid"] = $courseteacher->id;
        $coursedetailsarray[$key]["teachername"] = $courseteacher->firstname." ".$courseteacher->lastname;
    } else {
        $coursedetailsarray[$key]["teachername"] = "Not assigned";
    }
    $coursecontext = context_course::instance($coursevalue->id);
    $isfile = $DB->get_records_sql("Select * from {files} where contextid = ? and filename != ?", array($coursecontext->id, "."));
    if($isfile) {
        foreach ($isfile as $key1 => $isfilevalue) {
            $courseimage =  $CFG->wwwroot . "/pluginfile.php/" . $isfilevalue->contextid ."/". $isfilevalue->component . "/" . $isfilevalue->filearea . "/" . $isfilevalue->filename;   
        }
    }   
    if(!empty($courseimage)) {
        $coursedetailsarray[$key]["courseimage"] = $courseimage;
    } else {
        $coursedetailsarray[$key]["courseimage"] = $CFG->wwwroot."/theme/shiksha/data/nopic.jpg";
    }
    $coursedetailsarray[$key]["catid"] = $coursevalue->catid;
    $coursedetailsarray[$key]["categoryname"] = $coursevalue->catname;
    $coursedetailsarray[$key]["categoryid"] = $CFG->wwwroot."/course/index.php?categoryid=".$coursevalue->catid;
    $courseimage = '';
    
}       
$categorycontext = context_course::instance(1);

?>
<?php require('columns3courseheader.php'); ?>
<div id="page" class="row-fluid">
    <?php if ($CFG->version >= 2015051100) {
        echo $OUTPUT->full_header();
    } else { ?>
    <header id="page-header" class="clearfix">
        <div id="page-navbar" class="clearfix">
            <div class = "container">
                <div class="row">
                    <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                    <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
                </div>
            </div>
        </div>
        <div id="course-header">
            <?php echo $OUTPUT->course_header(); ?>
        </div>
    </header>
    <?php } ?>
    <div id="page-content" class="page-inner">
        <div class="container">
            <div id="region-main-box" class="<?php echo $regionmainbox; ?>">
                <div class="row-fluid">
                    <section id="region-main" class="<?php echo $regionmain; ?>">
                        <div class="custommain">
                            <div class="search-wr">
                                <form class="adminsearchform" method="get" action="<?php echo $CFG->wwwroot; ?>/course/search.php">
                                    <div>
                                        <label for="coursesearchbox" class="accesshide">Search in settings</label>
                                        <input id="coursesearchbox" type="text" name="search" size="30">
                                        <input type="submit" value="Search">
                                    </div>
                                </form>
                            </div>
                            <div class="course-items">
                                <?php foreach($coursedetailsarray as $coursedetailsarrayvalue) { ?>
                                    <div class="course-item">
                                      <a href="<?php echo $coursedetailsarrayvalue['courseid'];?>"><img src="<?php echo $coursedetailsarrayvalue['courseimage'];?>" alt="" class="coursepicture"></a>
                                      <div class="info">
                                        <h3 class="coursename"> <a href="<?php echo $coursedetailsarrayvalue['courseid'];?>"><?php echo $coursedetailsarrayvalue['coursename'];?></a> </h3>
                                        <?php if($coursedetailsarrayvalue['teachername'] != '') { ?><h4><?php echo get_string('defaultcourseteacher');?> 
                                        <span>
                                        <?php if( !empty($coursedetailsarrayvalue['teacherid']) ) { ?>
                                                <a href=<?php echo $CFG->wwwroot."/user/view.php?id=".$coursedetailsarrayvalue['teacherid']."&course=".$coursedetailsarrayvalue['catid'];?> >
                                        <?php } else { ?>
                                                <a href="#">
                                        <?php } ?>
                                                    <?php echo $coursedetailsarrayvalue['teachername'];?>
                                                </a>
                                        </span></h4><?php } ?>
                                      </div>
                                      <div class="content">
                                        <div class="summary">
                                          <p><?php echo $coursedetailsarrayvalue['coursesummary'];?></p>
                                        </div>
                                        <div class="action"><a href="<?php echo $coursedetailsarrayvalue['courseid'];?>"></a><a href="<?php echo $coursedetailsarrayvalue['categoryid'];?>"><?php echo $coursedetailsarrayvalue['categoryname'];?></a></div>
                                      </div>
                                    </div>
                                <?php } ?>
                            </div><!-- END of .course-items -->
                        </div>
                        <?php if (has_capability('moodle/course:create', $categorycontext)) { ?>
                        <div class="center">
                         <div class="btn-show-more-wr">
                                 <a class="btn-show-more" href="<?php echo $CFG->wwwroot;?>/course/edit.php?category=1&returnto=category"><span><?php echo get_string('addnewcourse'); ?></span></a>
                          </div>
                        </div>
                        <?php } ?>
                        <?php           
                        echo $OUTPUT->main_content();
                        ?>
                    </section>
                    <?php echo $OUTPUT->blocks('side-pre', $sidepre); ?>
                </div>
            </div>
            <?php echo $OUTPUT->blocks('side-post', $sidepost); ?>
        </div>
    </div>
</div>  
    <?php 
        include('footer.php');
        echo $OUTPUT->standard_end_of_body_html() 
    ?>

</body>
</html>
