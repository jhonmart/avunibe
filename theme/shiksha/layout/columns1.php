<?php
// Get the HTML for the settings bits.
$html = theme_shiksha_get_html_for_settings($OUTPUT, $PAGE);
require('header.php');?>
    <div id="page-content" class="row-fluid page-inner">
        <div class="container">
            <section id="region-main" class="span12">
                <?php
                echo $OUTPUT->course_content_header();
                echo $OUTPUT->main_content();
                echo $OUTPUT->course_content_footer();
                ?>
            </section>
        </div>
    </div>
    <?php
        include('footer.php');
        echo $OUTPUT->standard_end_of_body_html()
    ?>
</body>
</html>
