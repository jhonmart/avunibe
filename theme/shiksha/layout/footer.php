<?php $hasfooterlogo = $PAGE->theme->setting_file_url('footerlogo', 'footerlogo'); if(!empty($hasfooterlogo)) {
  $hasfooterlogo = $PAGE->theme->setting_file_url('footerlogo', 'footerlogo');
} else {
  $hasfooterlogo = $CFG->wwwroot.'/theme/shiksha/css/img/logo_footer_av.jpg';
} 
$quicklinksheading = get_config('theme_shiksha', 'quicklinksheading');
$hassomeinformation = get_config('theme_shiksha', 'someinformation');
$controlquicklinksection = get_config('theme_shiksha', 'controlquicklinksection');
$controlsmallfootersection = get_config('theme_shiksha', 'controlsmallfootersection');
?>
<footer id="footer">
  <?php if ( $controlquicklinksection == 0 ) { ?>
  <div class="row-fluid footer-up">
    <div class="container">
      <div class="span5"><a href="javascript:void(0);"><img src="<?php echo $hasfooterlogo; ?>" alt=""></a>
        <div class="contacts-desc">
          <?php if (!empty($hassomeinformation)) { ?>
            <div class="position" >
              <?php echo $hassomeinformation; ?>
            </div>
          <?php } ?>
          <div class="address"><?php echo get_config('theme_shiksha', 'address'); ?></div>
          <p><span><?php echo get_config('theme_shiksha', 'phoneheading'); ?></span><?php echo get_config('theme_shiksha', 'phone'); ?></p>
          <p><span><?php echo get_config('theme_shiksha', 'faxheading'); ?></span><?php echo get_config('theme_shiksha', 'fax'); ?></p>
          <p><span><?php echo get_config('theme_shiksha', 'emailheading'); ?></span> <a href="mailto:virtual@unibe.edu.do"><?php echo get_config('theme_shiksha', 'email'); ?></a></p>
        </div>
      </div>
      <div class="span7 quick-link">
        <?php if (!empty($quicklinksheading)) { ?>
          <h4 class="quick-link-head"><?php echo $quicklinksheading; ?></h4>
        <?php } ?>
        <?php for($quicklinkcols = 1; $quicklinkcols <= get_config('theme_shiksha', 'quicklinkscolumns'); $quicklinkcols = $quicklinkcols + 1) { ?>
          <ul class="col-1">
          <li><?php echo get_config('theme_shiksha', 'columnheading'.$quicklinkcols);?></li>
            <?php for($quicklinkrows = 1; $quicklinkrows <= get_config('theme_shiksha', 'quicklinksrows'.$quicklinkcols); $quicklinkrows = $quicklinkrows + 1) { ?>
              <li><a href="<?php echo get_config('theme_shiksha', 'link'.$quicklinkcols.'_'.$quicklinkrows); ?>"><?php echo get_config('theme_shiksha', 'text'.$quicklinkcols.'_'.$quicklinkrows); ?></a></li>
            <?php } ?>
          </ul>
        <?php } ?>
        
      </div>
    </div>
  </div>
  <?php } ?>
  <?php if ( $controlsmallfootersection == 0 ) { ?>
  <div class="footer-down">
    <div class="container">
      <div class="row-fluid">
        <div class="foot-custom-links"> <!--////UIAV de span8 a span6-->
          <?php if (!empty($html->leftfootnotesection1)) { ?>
            <a href="<?php echo $html->leftfootnotesectionlink1; ?>"><?php echo $html->leftfootnotesection1; ?></a> <span>|</span>
          <?php } if (!empty($html->leftfootnotesection2)) { ?>
            <a href="<?php echo $html->leftfootnotesectionlink2; ?>"><?php echo $html->leftfootnotesection2; ?></a> <span>|</span>
          <?php } if (!empty($html->leftfootnotesection3)) { ?>
            <a href="<?php echo $html->leftfootnotesectionlink3; ?>"><?php echo $html->leftfootnotesection3; ?></a> <span>|</span>
          <?php } if (!empty($html->leftfootnotesection4)) { ?>
            <a href="<?php echo $html->leftfootnotesectionlink4; ?>"><?php echo $html->leftfootnotesection4; ?></a>
          <?php } ?>
        </div>
        <?php if (!empty($html->footnote)) { ?>
          <div class="copyright"><?php echo $html->footnote; ?></div> <!--////UIAV de span4 a span6-->
        <?php } ?>
      </div>
    </div>
  </div>
  <?php } ?>

    <!--  CARGA DE LOS SCRIPTS CUSTOMIZADOS  -->
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/customs/cursos_destacados.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/customs/course.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/customs/search_course.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/customs/search_main.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/customs/my.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/customs/custom_main.js"></script>

    <script>
        var custom = new custom_main();
        custom._init_();
    </script>

</footer>
<!-- //// [Solo Logeado] - JAVASCRIPT CUSTOMIZADO UIAV //// -->
<?php
if (isloggedin()) {
?>
<script type="text/javascript">
	$(document).ready(function() {
		$("#sticky-wrapper ul.nav").append("<li><a href='<?php echo $CFG->wwwroot."/my/"; ?>'>MIS CURSOS</a></li>");

    //// UIAV - Agregar .clear1x a lista de cursos ////
    if ($(".course_list").length > 0) {
      $(".course_list").append("<div class='clear1x'></div>");
    }

	});
</script>
<?php
}
?>
<!-- //// [Solo Logeado] - JAVASCRIPT CUSTOMIZADO UIAV //// -->
<script type="text/javascript">
	$(document).ready(function() {
    if ($("li.langmenu a").length > 0) {
      $("li.langmenu a").each(function() {
        var thisx = $(this);
        if (thisx.text().indexOf("Español") == -1) {
          thisx.text("English");
        } else {
          thisx.text("Español");
        }
      });
    }

    if ($("#page-course-view-topics h3.sectionname").length > 0) {
      $("#page-course-view-topics h3.sectionname").prepend("<div class='topic_preblue_contcont1x'><div class='topic_warrow_cont1x'><div class='topic_warrow1x'></div></div></div>");
    }

    $("body").append("<a href='#' id='back-to-top' title='Back to top'>&uarr;</a>");

    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    // Objetos de seleccion de opciones con buscador rapido
//    $('select').chosen();

  });
</script>

<style type="text/css">
#page a:focus, #page .crate-account a, #page .parallax-content h3, #page .parallax-content .accordion-heading a:hover, #page .contacts-desc .position h4, #page .footer-down a, #page .img-slider .bxslider li a:hover, #page .dropdown-menu > li > a:hover, #page .dropdown-menu > li > a:focus, #page .dropdown-submenu:hover > a, #page .dropdown-submenu:focus > a, .percent, a, a:hover, a:focus, .crate-account a, .parallax-content h3, .parallax-content .accordion-heading a:hover, .contacts-desc .position h4, .footer-down a, .img-slider .bxslider li a:hover, .dropdown-menu > li > a:hover, .navbar .nav a:hover, .dropdown-menu > li > a:focus, .dropdown-submenu:hover > a, .dropdown-submenu:focus > a, .usermenu.dropdown .dropdown-menu > li > a:hover, #page-content #block-region-side-pre .footer a, #page-content #block-region-side-post .footer a {
    color: #e67e22 !important;
}
</style>

<!--<div style="z-index:99999;position:absolute;left:0px;top:0px;background-color:white;padding:10px;color:black;">.</div>-->