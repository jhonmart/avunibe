<?php
// Get the HTML for the settings bits.
$html = theme_shiksha_get_html_for_settings($OUTPUT, $PAGE);
GLOBAL $USER, $PAGE, $CFG, $DB;
$haslogo = $PAGE->theme->setting_file_url('logo', 'logo');
if(!empty($haslogo)) {
  $haslogo = $PAGE->theme->setting_file_url('logo', 'logo');
} else {
  $haslogo = $CFG->wwwroot.'/theme/shiksha/css/img/logo.jpg';
}
$hasicon = $PAGE->theme->setting_file_url('icon', 'icon');
if(!empty($hasicon)) {
  $hasiconlogo = $PAGE->theme->setting_file_url('icon', 'icon');
} else {
  $hasiconlogo = $CFG->wwwroot.'/theme/shiksha/css/img/logo-icon.jpg';
}
/*-------------------social contacts section start---------------------*/
$socialicon1link = get_config('theme_shiksha', 'socialiconlink1');
$socialicon2link = get_config('theme_shiksha', 'socialiconlink2');
$socialicon3link = get_config('theme_shiksha', 'socialiconlink3');
$socialicon4link = get_config('theme_shiksha', 'socialiconlink4');

$hassocialicon1 = $PAGE->theme->setting_file_url('socialicon1', 'socialicon1');
if(!empty($hassocialicon1)) {
  $socialicon1 = $PAGE->theme->setting_file_url('socialicon1', 'socialicon1');
} else {
  $socialicon1 = $CFG->wwwroot."/theme/shiksha/css/img/icon-facebook.png";
}
$hassocialicon2 = $PAGE->theme->setting_file_url('socialicon2', 'socialicon2');
if(!empty($hassocialicon2)) {
  $socialicon2 = $PAGE->theme->setting_file_url('socialicon2', 'socialicon2');
} else {
  $socialicon2 = $CFG->wwwroot."/theme/shiksha/css/img/icon-twitter.png";
}
$hassocialicon3 = $PAGE->theme->setting_file_url('socialicon3', 'socialicon3');
if(!empty($hassocialicon3)) {
  $socialicon3 = $PAGE->theme->setting_file_url('socialicon3', 'socialicon3');
} else {
  $socialicon3 = $CFG->wwwroot."/theme/shiksha/css/img/icon-linkedin.png";
}
$hassocialicon4 = $PAGE->theme->setting_file_url('socialicon4', 'socialicon4');
if(!empty($hassocialicon4)) {
  $socialicon4 = $PAGE->theme->setting_file_url('socialicon4', 'socialicon4');
} else {
  $socialicon4 = $CFG->wwwroot."/theme/shiksha/css/img/icon-instagram.png";
}
if ($CFG->version >= 2015051100) {
  $hasstringbadge = get_string('badges', 'badges');
} else {
  $hasstringbadge = get_string('mybadges', 'badges');
}
/*--------------------------social contacts section end----------------------------*/
echo $OUTPUT->doctype();

$isregistration = $DB->get_record('config', array('name'=>'registerauth'));
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/bootstrap.css">
    <link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/bootstrap-responsive.css">
    <link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/jquery.bxslider.css">
    <link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/font-awesome.css">
    <link type="text/css" rel="Stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/shiksha/css/style.css?id=3131">
  
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/jquery-2.1.4.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/bootstrap.min.js"></script>
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/jquery.sticky.js"></script> 
    <script src="<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/engine.js"></script>

    <style type="text/css">
        *[role="main"] {
            display: none;
        }
    </style>
    <?php echo $OUTPUT->standard_head_html() ?>
</head>
<body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>
<?php include $CFG->dirroot . '/theme/shiksha/analyticstracking.php'; ?>
<header role="banner" class="navbar">
  <div class="container">
    <div id="top_contents1" class="page-top-bar"> <!--////AVUI2 - 24/05/2017 - ADD id="top_contents1" -->
        <div class="container">
          <?php if (!empty($socialicon1link) || !empty($socialicon2link) || !empty($socialicon3link) || !empty($socialicon4link)) { ?>
            <div class="span3 top-social-icons">
              <?php if (!empty($socialicon1link)) { ?><a href="<?php echo $socialicon1link; ?>"><img src="<?php echo $socialicon1; ?>" alt=""></a><?php } ?>
              <?php if (!empty($socialicon2link)) { ?><a href="<?php echo $socialicon2link; ?>"><img src="<?php echo $socialicon2; ?>" alt=""></a><?php } ?>
              <?php if (!empty($socialicon3link)) { ?><a href="<?php echo $socialicon3link; ?>"><img src="<?php echo $socialicon3; ?>" alt=""></a><?php } ?>
              <?php if (!empty($socialicon4link)) { ?><a href="<?php echo $socialicon4link; ?>"><img src="<?php echo $socialicon4; ?>" alt=""></a><?php } ?>
            </div>
          <?php } else { ?>
            <div class="span3 top-social-icons"></div>
          <?php } ?>
          <div class="span9 logining-wr">
            <?php if (!isloggedin()) { ?>
              <?php if($isregistration->value == 'email') { ?>
                  <div class="crate-account"><a href="<?php echo $CFG->wwwroot; ?>/login/signup.php?"><?php echo get_string('startsignup'); ?></a></div>
                <?php } ?>
                  <form action="<?php echo $CFG->wwwroot; ?>/login/index.php?authldap_skipntlmsso=1" method="post">
                    <input type="text" placeholder="Username:" name="username">
                    <input type="password" placeholder="Password:" name="password">
                    <input type="submit" value="<?php echo get_string('login');?>">
                  </form>
                <?php } else { ?>
                  <div class="usermenu pull-right"> <!--////AVUI2 - 24/05/2017 - Remove class "dropdown"-->
                    <div class="actionmenu nowrap-items">
					<!--////AVUI - SAVED ////
                      <ul class="nav-inner">
                        <li class="opendropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="userbutton"><span class="avatars"><span class="avatar current" ><?php echo $OUTPUT->user_profile_picture(); ?></span></span>
                        <!--<span class="welcome">Welcome,</span> UIAV
                        <span class="usertext"><?php echo $USER->firstname .' '. $USER->lastname;?></span><i class="fa fa-chevron-down"></i> </span>
						<!--<b class="caret"></b>
						</a>
                          <ul class="dropdown-menu align-tr-br" role="menu" aria-labelledby="dropdownMenu">
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/my" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('myhome'); ?></span></a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/user/profile.php?id=<?php echo $USER->id;?>" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('profile'); ?></span></a></li>
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/badges/view.php?type=1"><span class="menu-action-text"><?php echo $hasstringbadge;?></span></a></li>
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/message/index.php" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('messages', 'chat'); ?></span></a></li>
                            
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/login/logout.php" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('logout');?></span></a></li>
                          </ul>
                        </li>
                      </ul>
					<!--////AVUI - SAVED ////-->
					
					<!--////AVUI - ADDED ////-->
                      <ul class="nav-inner">
                        
                        <li class="">
                            <span class="userbutton">
                              <span class="avatars">

                                <span class="avatar current" >
                                  <?php echo $OUTPUT->user_profile_picture(); ?>
                                </span>

                              </span>
                                <!--<span class="welcome">Welcome,</span> UIAV -->
                              <span class="usertext">
                                <?php echo $USER->firstname .' '. $USER->lastname;?>
                              </span>

                              <i class="fa fa-chevron-down"></i>
                            </span><!--<b class="caret"></b>-->
                        </li>

                        <li style="padding-top:5px;margin-left:12px;">
                          <a class="cmd_top1x" href="<?php echo $CFG->wwwroot; ?>/login/logout.php" class="icon menu-action">
                            <?php echo get_string('logout');?>
                          </a>
                        </li>

                        <!--///////////////////////////////////////-->
                        <li class="opendropdown dropdown">
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <span class="hbim1x">
                              <img src="<?php echo $CFG->wwwroot."/theme/shiksha/pix/"; ?>hb_mi.png" />
                            </span>
                          </a>

                          <ul class="dropdown-menu align-tr-br" role="menu" aria-labelledby="dropdownMenu">
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/my" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('myhome'); ?></span></a></li>
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/user/preferences.php" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('preferences'); ?></span></a></li>
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/user/profile.php?id=<?php echo $USER->id;?>" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('profile'); ?></span></a></li>
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/badges/view.php?type=1"><span class="menu-action-text"><?php echo $hasstringbadge;?></span></a></li>
                            <li><a tabindex="-1" href="<?php echo $CFG->wwwroot; ?>/message/index.php" class="icon menu-action"><span class="menu-action-text"><?php echo get_string('messages', 'chat'); ?></span></a></li>
                          </ul>

                        </li>
                        <!--///////////////////////////////////////-->

                        <div style="clear:both;"></div>
                      </ul>
					<!--////AVUI - ADDED ////-->
					
                    </div>
                  </div>
                <?php } ?>
          </div>
        </div>
    </div>
    <!-- Site Logo
    ================================================== -->
    <div id="top_logo1" class="container"> <!--////AVUI2 - 24/05/2017 - ADD id="top_logo1" -->
      <?php if (get_config('theme_shiksha', 'logoorsitename') === "logo") { ?>
        <div class="logo-wr">
          <a class="sitelogo-img" href="<?php echo $CFG->wwwroot; ?>"><img src="<?php echo $haslogo; ?>" alt=""></a>
        </div>
      <?php } else if (get_config('theme_shiksha', 'logoorsitename') === "sitename") { ?>
        <div class="logo-wr">
          <a class="sitelogo-text" href="<?php echo $CFG->wwwroot; ?>"><?php echo $SITE->fullname; ?></a>
          </div>
      <?php } else if (get_config('theme_shiksha', 'logoorsitename') === "iconsitename") { ?>
        <div class="logo-wr">
          <a class="sitelogo-icon-name" href="<?php echo $CFG->wwwroot; ?>"><img src="<?php echo $hasiconlogo; ?>" alt=""><?php echo $SITE->fullname; ?></a>
          </div>
      <?php } ?>
    </div>
    <!-- Navbar
    ================================================== -->
    <div id="top_menu1" class="navbar navbar-inverse navbar-static-top"> <!--////AVUI2 - 24/05/2017 - ADD id="top_menu1" -->
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <div class="nav-collapse collapse">
            <?php echo $OUTPUT->custom_menu();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>