<?php
// Get the HTML for the settings bits.
$html = theme_shiksha_get_html_for_settings($OUTPUT, $PAGE);
GLOBAL $DB;

	require('headerlogin.php'); ?>
	<input type="hidden" id="logininfo" value ="<?php echo get_string('username'); ?>" name = "<?php echo get_string('password'); ?>"/> <!-- username and password string lang support -->
	<div id="page" class="container-fluid login-page">
		<div id="page-content" class="row-fluid">
			<section id="region-main" class="container login-regin-main" style="position:relative;">
            <div class="student-login" style=""> </div>
				<?php 
					echo $OUTPUT->course_content_header();
					echo $OUTPUT->main_content(); 
					echo $OUTPUT->course_content_footer();
				?>
				<?php if($isregistration->value == 'email') { ?><div class="signup-link"><?php echo get_string('firsttime'); ?><a href="<?php echo $CFG->wwwroot; ?>/login/signup.php?"><?php echo ' '.get_string('startsignup'); ?></a></div><?php } ?>
				<?php echo $OUTPUT->lang_menu(); ?>
			</section>
		</div>
	</div>
	
	<?php  include('footer.php'); echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
