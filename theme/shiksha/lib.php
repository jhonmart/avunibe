<?php
function theme_shiksha_get_setting($setting, $format = false) {
    global $CFG;
    require_once($CFG->dirroot . '/lib/weblib.php');
    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('shiksha');
    }
    if (empty($theme->settings->$setting)) {
        return false;
    } else if (!$format) {
        return $theme->settings->$setting;
    } else if ($format === 'format_text') {
        return format_text($theme->settings->$setting, FORMAT_PLAIN);
    } else if ($format === 'format_html') {
        return format_text($theme->settings->$setting, FORMAT_HTML, array('trusted' => true, 'noclean' => true));
    } else {
        return format_string($theme->settings->$setting);
    }
}
/**
 * Parses CSS before it is cached.
 *
 * This function can make alterations and replace patterns within the CSS.
 *
 * @param string $css The CSS
 * @param theme_config $theme The theme config object.
 * @return string The parsed CSS The parsed CSS.
 */
function theme_shiksha_process_css($css, $theme) {

    if (!empty($theme->settings->fontnamebody)) {
        $font = $theme->settings->fontnamebody;
    } else {
        $font = 'robotomedium';
    }
    $logobackgroundimage = $theme->setting_file_url('logobackgroundimage', 'logobackgroundimage');
    $bannerimage = $theme->setting_file_url('bannerimage', 'bannerimage');
    $css = theme_shiksha_set_logobackgroundimage($css, $logobackgroundimage);
    $css = theme_shiksha_set_bannerimage($css, $bannerimage);
    
    $headingfont = theme_shiksha_get_setting('fontnameheading');
    $bodyfont = theme_shiksha_get_setting('fontnamebody');
    
    
    $css = theme_shiksha_set_headingfont($css, $headingfont);
    $css = theme_shiksha_set_bodyfont($css, $bodyfont);
    $css = theme_shiksha_set_fontfiles($css, 'heading', $headingfont);
    $css = theme_shiksha_set_fontfiles($css, 'body', $bodyfont);
    
    $sliderimage1 = $theme->setting_file_url('slideimage1', 'slideimage1');
    $css = theme_shiksha_set_sliderimage1($css, $sliderimage1);

    $sliderimage2 = $theme->setting_file_url('slideimage2', 'slideimage2');
    $css = theme_shiksha_set_sliderimage2($css, $sliderimage2);

    $sliderimage3 = $theme->setting_file_url('slideimage3', 'slideimage3');
    $css = theme_shiksha_set_sliderimage3($css, $sliderimage3);

    $sliderimage4 = $theme->setting_file_url('slideimage4', 'slideimage4');
    $css = theme_shiksha_set_sliderimage4($css, $sliderimage4);

    $sliderimage5 = $theme->setting_file_url('slideimage5', 'slideimage5');
    $css = theme_shiksha_set_sliderimage5($css, $sliderimage5);
    
    $parallaxsectionbackgroundimage = $theme->setting_file_url('parallaxsectionbackgroundimage', 'parallaxsectionbackgroundimage');
    $css = theme_shiksha_set_parallaxsectionbackgroundimage($css, $parallaxsectionbackgroundimage);
    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_shiksha_set_customcss($css, $customcss);

    $themecolor = theme_shiksha_get_setting('color');
    $css = theme_shiksha_set_color($css, $themecolor);

    $sliderbgcolor1 = theme_shiksha_get_setting('sliderbackgroundcolor1');
    $css = theme_shiksha_set_sliderbgcolor1($css, $sliderbgcolor1);

    $sliderbgcolor2 = theme_shiksha_get_setting('sliderbackgroundcolor2');
    $css = theme_shiksha_set_sliderbgcolor2($css, $sliderbgcolor2);

    $sliderbgcolor3 = theme_shiksha_get_setting('sliderbackgroundcolor3');
    $css = theme_shiksha_set_sliderbgcolor3($css, $sliderbgcolor3);

    $sliderbgcolor4 = theme_shiksha_get_setting('sliderbackgroundcolor4');
    $css = theme_shiksha_set_sliderbgcolor4($css, $sliderbgcolor4);

    $sliderbgcolor5 = theme_shiksha_get_setting('sliderbackgroundcolor5');
    $css = theme_shiksha_set_sliderbgcolor5($css, $sliderbgcolor5);

    $bannerbgcolor = theme_shiksha_get_setting('staticbackgroundcolor');
    $css = theme_shiksha_set_bannerbgcolor($css, $bannerbgcolor);
    return $css;
}

function theme_shiksha_set_sliderimage1($css, $themesliderimage) {
    GLOBAL $CFG;
    $tag = '[[setting:slideimage1]]';
    $replacement = $themesliderimage;
    if (is_null($replacement)) {
        $replacement = $CFG->wwwroot.'/theme/shiksha/css/img/bg-banner.jpg';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_sliderimage2($css, $themesliderimage) {
    GLOBAL $CFG;
    $tag = '[[setting:slideimage2]]';
    $replacement = $themesliderimage;
    if (is_null($replacement)) {
        $replacement = $CFG->wwwroot.'/theme/shiksha/css/img/bg-banner-2.jpg';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_sliderimage3($css, $themesliderimage) {
    GLOBAL $CFG;
    $tag = '[[setting:slideimage3]]';
    $replacement = $themesliderimage;
    if (is_null($replacement)) {
        $replacement = $CFG->wwwroot.'/theme/shiksha/css/img/bg-banner-3.jpg';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_sliderimage4($css, $themesliderimage) {
    GLOBAL $CFG;
    $tag = '[[setting:slideimage4]]';
    $replacement = $themesliderimage;
    if (is_null($replacement)) {
        $replacement = $CFG->wwwroot.'/theme/shiksha/css/img/bg-banner.jpg';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_sliderimage5($css, $themesliderimage) {
    GLOBAL $CFG;
    $tag = '[[setting:slideimage5]]';
    $replacement = $themesliderimage;
    if (is_null($replacement)) {
        $replacement = $CFG->wwwroot.'/theme/shiksha/css/img/bg-banner-2.jpg';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_parallaxsectionbackgroundimage($css, $parallaxsectionbackgroundimage) {
    GLOBAL $CFG;
    $tag = '[[setting:parallaxsectionbackgroundimage]]';
    $replacement = $parallaxsectionbackgroundimage;
    if (is_null($replacement)) {
        $replacement = $CFG->wwwroot.'/theme/shiksha/css/img/bg-parallax.jpg';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_shiksha_set_bannerimage($css, $themebannerimage) {
    GLOBAL $CFG;
    $tag = '[[setting:bannerimage]]';
    $replacement = $themebannerimage;
    if (is_null($replacement)) {
        $replacement = $CFG->wwwroot.'/theme/shiksha/css/img/bg-banner.jpg';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_shiksha_set_logobackgroundimage($css, $themelogobackgroundimage) {
    GLOBAL $CFG;
    $colorscheme = get_config('theme_shiksha', 'colorscheme');
    $tag = '[[setting:logobackgroundimage]]';
    $replacement = $themelogobackgroundimage;
    if (is_null($replacement)) {
        $replacement = $CFG->wwwroot.'/theme/shiksha/css/img/bg-logo.jpg';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_shiksha_set_headingfont($css, $headingfont) {
    $tag = '[[setting:headingfont]]';
    $replacement = $headingfont;
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_shiksha_set_bodyfont($css, $bodyfont) {
    $tag = '[[setting:bodyfont]]';
    $replacement = $bodyfont;
    $css = str_replace($tag, $replacement, $css);
    return $css;
}


/**
 * Adds the font to CSS.
 *
 * @param string $css The CSS.
 * @param string $font The font name.
 * @return string The parsed CSS
 */

function theme_shiksha_set_fontfiles($css, $type, $fontname) {
    $tag = '[[setting:fontfiles' . $type . ']]';
    $replacement = '';
    if (theme_shiksha_get_setting('fontselect') === '2') {
        static $theme;
        if (empty($theme)) {
            $theme = theme_config::load('shiksha');  // $theme needs to be us for child themes.
        }

        $fontfiles = array();
        $fontfileeot = $theme->setting_file_url('fontfileeot' . $type, 'fontfileeot' . $type);
        if (!empty($fontfileeot)) {
            $fontfiles[] = "url('" . $fontfileeot . "?#iefix') format('embedded-opentype')";
        }
        $fontfilewoff = $theme->setting_file_url('fontfilewoff' . $type, 'fontfilewoff' . $type);
        if (!empty($fontfilewoff)) {
            $fontfiles[] = "url('" . $fontfilewoff . "') format('woff')";
        }
        $fontfilewofftwo = $theme->setting_file_url('fontfilewofftwo' . $type, 'fontfilewofftwo' . $type);
        if (!empty($fontfilewofftwo)) {
            $fontfiles[] = "url('" . $fontfilewofftwo . "') format('woff2')";
        }
        $fontfileotf = $theme->setting_file_url('fontfileotf' . $type, 'fontfileotf' . $type);
        if (!empty($fontfileotf)) {
            $fontfiles[] = "url('" . $fontfileotf . "') format('opentype')";
        }
        $fontfilettf = $theme->setting_file_url('fontfilettf' . $type, 'fontfilettf' . $type);
        if (!empty($fontfilettf)) {
            $fontfiles[] = "url('" . $fontfilettf . "') format('truetype')";
        }
        $fontfilesvg = $theme->setting_file_url('fontfilesvg' . $type, 'fontfilesvg' . $type);
        if (!empty($fontfilesvg)) {
            $fontfiles[] = "url('" . $fontfilesvg . "') format('svg')";
        }

        $replacement = '@font-face {' . PHP_EOL . 'font-family: "' . $fontname . '";' . PHP_EOL;
        $replacement .=!empty($fontfileeot) ? "src: url('" . $fontfileeot . "');" . PHP_EOL : '';
        if (!empty($fontfiles)) {
            $replacement .= "src: ";
            $replacement .= implode("," . PHP_EOL . " ", $fontfiles);
            $replacement .= ";";
        }
        $replacement .= '' . PHP_EOL . "}";
    }

    $css = str_replace($tag, $replacement, $css);
    return $css;
}
/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_shiksha_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('shiksha');
    }
    if ($context->contextlevel == CONTEXT_SYSTEM) {
        if ($filearea === 'logo') {
            return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
        } else if ($filearea === 'style') {
            theme_shiksha_serve_css($args[1]);
        } else if ($filearea === 'pagebackground') {
            return $theme->setting_file_serve('pagebackground', $args, $forcedownload, $options);
        } else if ($filearea === 'icon') {
            return $theme->setting_file_serve('icon', $args, $forcedownload, $options);
        } else if ($filearea === 'socialicon1') {
            return $theme->setting_file_serve('socialicon1', $args, $forcedownload, $options);
        } else if ($filearea === 'socialicon2') {
            return $theme->setting_file_serve('socialicon2', $args, $forcedownload, $options);
        } else if ($filearea === 'socialicon3') {
            return $theme->setting_file_serve('socialicon3', $args, $forcedownload, $options);
        } else if ($filearea === 'socialicon4') {
            return $theme->setting_file_serve('socialicon4', $args, $forcedownload, $options);
        } else if ($filearea === 'addressfontawesomeicon') {
            return $theme->setting_file_serve('addressfontawesomeicon', $args, $forcedownload, $options);
        } else if ($filearea === 'phonefontawesomeicon') {
            return $theme->setting_file_serve('phonefontawesomeicon', $args, $forcedownload, $options);
        } else if ($filearea === 'emailfontawesomeicon') {
            return $theme->setting_file_serve('emailfontawesomeicon', $args, $forcedownload, $options);
        } else if ($filearea === 'bannerimage') {
            return $theme->setting_file_serve('bannerimage', $args, $forcedownload, $options);
        } else if ($filearea === 'logobackgroundimage') {
            return $theme->setting_file_serve('logobackgroundimage', $args, $forcedownload, $options);
        } else if (preg_match("/^fontfile(eot|otf|svg|ttf|woff|woff2)(heading|body)$/", $filearea)) { // http://www.regexr.com/.
            return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
        } else if (preg_match("/^(marketing|slide)[1-9][0-9]*image$/", $filearea)) {
            return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
        } else if($filearea === 'courseprogrammeimage1') {
            return $theme->setting_file_serve('courseprogrammeimage1', $args, $forcedownload, $options);
        } else if($filearea === 'courseprogrammeimage2') {
            return $theme->setting_file_serve('courseprogrammeimage2', $args, $forcedownload, $options);
        } else if($filearea === 'courseprogrammeimage3') {
            return $theme->setting_file_serve('courseprogrammeimage3', $args, $forcedownload, $options);
        } else if($filearea === 'courseprogrammeimage4') {
            return $theme->setting_file_serve('courseprogrammeimage4', $args, $forcedownload, $options);
        } else if($filearea === 'slideimage1') {
            return $theme->setting_file_serve('slideimage1', $args, $forcedownload, $options);
        } else if($filearea === 'slideimage2') {
            return $theme->setting_file_serve('slideimage2', $args, $forcedownload, $options);
        } else if($filearea === 'slideimage3') {
            return $theme->setting_file_serve('slideimage3', $args, $forcedownload, $options);
        } else if($filearea === 'slideimage4') {
            return $theme->setting_file_serve('slideimage4', $args, $forcedownload, $options);
        } else if($filearea === 'slideimage5') {
            return $theme->setting_file_serve('slideimage5', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage1') {
            return $theme->setting_file_serve('catslideimage1', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage2') {
            return $theme->setting_file_serve('catslideimage2', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage3') {
            return $theme->setting_file_serve('catslideimage3', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage4') {
            return $theme->setting_file_serve('catslideimage4', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage5') {
            return $theme->setting_file_serve('catslideimage5', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage6') {
            return $theme->setting_file_serve('catslideimage6', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage7') {
            return $theme->setting_file_serve('catslideimage7', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage8') {
            return $theme->setting_file_serve('catslideimage8', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage9') {
            return $theme->setting_file_serve('catslideimage9', $args, $forcedownload, $options);
        } else if($filearea === 'catslideimage10') {
            return $theme->setting_file_serve('catslideimage10', $args, $forcedownload, $options);
        } else if($filearea === 'faviconurl') {
            return $theme->setting_file_serve('faviconurl', $args, $forcedownload, $options);
        } else if($filearea === 'parallaxsectionbackgroundimage') {
            return $theme->setting_file_serve('parallaxsectionbackgroundimage', $args, $forcedownload, $options);
        } else if($filearea === 'parallaxaccordionicon1') {
            return $theme->setting_file_serve('parallaxaccordionicon1', $args, $forcedownload, $options);
        } else if($filearea === 'parallaxaccordionicon2') {
            return $theme->setting_file_serve('parallaxaccordionicon2', $args, $forcedownload, $options);
        } else if($filearea === 'parallaxaccordionicon3') {
            return $theme->setting_file_serve('parallaxaccordionicon3', $args, $forcedownload, $options);
        } else if($filearea === 'parallaxaccordionicon4') {
            return $theme->setting_file_serve('parallaxaccordionicon4', $args, $forcedownload, $options);
        } else if($filearea === 'parallaxaccordionicon5') {
            return $theme->setting_file_serve('parallaxaccordionicon5', $args, $forcedownload, $options);
        } else if($filearea === 'footerlogo') {
            return $theme->setting_file_serve('footerlogo', $args, $forcedownload, $options);
        } else {
            send_file_not_found();
        }
    } else {
        send_file_not_found();
    }

}

/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_shiksha_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}
/**
 * Adds any custom color to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $themecolor The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_shiksha_set_color($css, $themecolor) {
    $tag = '[[setting:color]]';
    $replacement = $themecolor;
    if (is_null($replacement)) {
        $replacement = '#e67e22';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
/**
 * Adds any custom slider bgcolor to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $themecolor The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_shiksha_set_sliderbgcolor1($css, $sliderbgcolor1) {
    $tag = '[[setting:sliderbgcolor1]]';
    $replacement = $sliderbgcolor1;
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_sliderbgcolor2($css, $sliderbgcolor2) {
    $tag = '[[setting:sliderbgcolor2]]';
    $replacement = $sliderbgcolor2;
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_sliderbgcolor3($css, $sliderbgcolor3) {
    $tag = '[[setting:sliderbgcolor3]]';
    $replacement = $sliderbgcolor3;
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_sliderbgcolor4($css, $sliderbgcolor4) {
    $tag = '[[setting:sliderbgcolor4]]';
    $replacement = $sliderbgcolor4;
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_sliderbgcolor5($css, $sliderbgcolor5) {
    $tag = '[[setting:sliderbgcolor5]]';
    $replacement = $sliderbgcolor5;
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_shiksha_set_bannerbgcolor($css, $bannerbgcolor) {
    $tag = '[[setting:bannerbgcolor]]';
    $replacement = $bannerbgcolor;
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * Do not add shiksha specific logic in here, child themes should be able to
 * rely on that function just by declaring settings with similar names.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 *      - navbarclass A CSS class to use on the navbar. By default ''.
 *      - heading HTML to use for the heading. A logo if one is selected or the default heading.
 *      - footnote HTML to use as a footnote. By default ''.
 */
function theme_shiksha_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {
        $return->heading = html_writer::tag('div', '', array('class' => 'logo'));
    } else {
        $return->heading = $output->page_heading();
    }

    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = '<div class="footnote text-center">'.format_text($page->theme->settings->footnote).'</div>';
    }

    if (!empty($page->theme->settings->leftfootnote)) {
        $return->leftfootnote = format_text($page->theme->settings->leftfootnote);
    }

    if (!empty($page->theme->settings->leftfootnotesection1)) {
        $return->leftfootnotesection1 = $page->theme->settings->leftfootnotesection1;
    }

    if (!empty($page->theme->settings->leftfootnotesectionlink1)) {
        $return->leftfootnotesectionlink1 = $page->theme->settings->leftfootnotesectionlink1;
    }

    
    if (!empty($page->theme->settings->leftfootnotesection2)) {
        $return->leftfootnotesection2 = $page->theme->settings->leftfootnotesection2;
    }

    if (!empty($page->theme->settings->leftfootnotesectionlink2)) {
        $return->leftfootnotesectionlink2 = $page->theme->settings->leftfootnotesectionlink2;
    }

    if (!empty($page->theme->settings->leftfootnotesection3)) {
        $return->leftfootnotesection3 = $page->theme->settings->leftfootnotesection3;
    }

    if (!empty($page->theme->settings->leftfootnotesectionlink3)) {
        $return->leftfootnotesectionlink3 = $page->theme->settings->leftfootnotesectionlink3;
    }

    if (!empty($page->theme->settings->leftfootnotesection4)) {
        $return->leftfootnotesection4 = $page->theme->settings->leftfootnotesection4;
    }

    if (!empty($page->theme->settings->leftfootnotesectionlink4)) {
        $return->leftfootnotesectionlink4 = $page->theme->settings->leftfootnotesectionlink4;
    }

    if (!empty($page->theme->settings->leftfootnotesection5)) {
        $return->leftfootnotesection5 = $page->theme->settings->leftfootnotesection5;
    }

    if (!empty($page->theme->settings->leftfootnotesectionlink5)) {
        $return->leftfootnotesectionlink5 = $page->theme->settings->leftfootnotesectionlink5;
    }

    if (!empty($page->theme->settings->leftfootnotesection6)) {
        $return->leftfootnotesection6 = $page->theme->settings->leftfootnotesection6;
    }

    if (!empty($page->theme->settings->leftfootnotesectionlink6)) {
        $return->leftfootnotesectionlink6 = $page->theme->settings->leftfootnotesectionlink6;
    }
    return $return;
}

/**
 * All theme functions should start with theme_shiksha_
 * @deprecated since 2.5.1
 */
function shiksha_process_css() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_shiksha_
 * @deprecated since 2.5.1
 */
function shiksha_set_logo() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_shiksha_
 * @deprecated since 2.5.1
 */
function shiksha_set_customcss() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}


