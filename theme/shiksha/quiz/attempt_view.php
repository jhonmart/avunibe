<!--<div class="background_for_open_modal" id="modal_quiz_mode_security">-->
<!--    <div class="auto_modal_quiz" role="dialog">-->
<!--        <div class="modal-dialog">-->
<!--            <div class="modal-content">-->
<!--                <div class="modal-header">-->
<!--                    <h4 class="modal-title">--><?php //echo get_string('mode_full_screen'); ?><!--</h4>-->
<!--                </div>-->
<!--                <div class="modal-body">-->
<!--                    <p>--><?php //echo get_string('mode_full_screen_desc'); ?><!--</p>-->
<!--                </div>-->
<!--                <div class="modal-footer">-->
<!--                    <button type="button" class="btn btn-success btn-md" data-dismiss="modal" onclick="quiz.action(true); ">--><?php //echo get_string('start'); ?><!--</button>-->
<!--                    <button type="button" class="btn btn-warning btn-md" onclick="quiz.action(false)">--><?php //echo get_string('finalize'); ?><!--</button>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<script>
    function dynamicallyLoadScript(url) {
        var script = document.createElement("script");
        script.src = url;

        document.head.appendChild(script);
    }

    dynamicallyLoadScript('<?php echo $CFG->wwwroot; ?>/theme/shiksha/js/quiz_control.js');
</script>